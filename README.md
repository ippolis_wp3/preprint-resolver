# PreprintResolver

IPPOLIS WP3 PreprintResolver project.

## Demo

A public test instance is running at [preprintresolver.eu](https://preprintresolver.eu/).

## Deployment

When running this in the context of the other IPPOLIS WP3 tools (i.e. not standalone), you have to make sure to

- override the backend URL in ./services/arxiv-frontend-web/.env.local, e.g. `PUBLIC_BACKEND="http://localhost:8080/backend"`
- activate the Spring profile `eureka`, e.g. `export spring_profiles_active=eureka` if you are not using docker-compose

### h2-database-server

Run `./scripts/update_external_resources.sh`.

### jcr-impact-factor-analyzer

For a tmux deployment running as non-root or with a `TMPDIR` which has the `noexec` flag set, you have to manually run

`install.packages("JCRImpactFactor", repos = "http://cran.us.r-project.org")`

in R before running the service.

## Citation

```bibtex
@InProceedings{2309.01373,
	 title={PreprintResolver: Improving Citation Quality by Resolving Published Versions of ArXiv Preprints Using Literature Databases},
	 author={Bloch, L. and Rückert, J. and Friedrich, C. M.},
	 booktitle={Proceedings of the 27th International Conference on Theory and Practice of Digital Libraries ({TPDL})},
	 year={2023},
	 pages={47 - 61},
	 doi={10.1007/978-3-031-43849-3_5}
}
```
