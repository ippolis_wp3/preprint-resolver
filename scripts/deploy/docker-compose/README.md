# docker-compose

## Development

Run `compose_up.sh .dev` to bring the services up, `compose_down.sh .dev` to bring them down.

Use `compose_script.sh .dev build` to run custom compose commands (`build` in this example).
