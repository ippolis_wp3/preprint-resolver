package de.ippolis.wp3.arxiv_resolver_service.model;

import java.util.List;

public class TitleAuthorIdRequestDTO {

  private String title;
  private String id;
  private List<String> authors;
  private BibTexPagePreprocessing bibTexPagePreprocessing;
  private boolean useLocalData;
  private boolean useAPI;

  public TitleAuthorIdRequestDTO() {}

  public TitleAuthorIdRequestDTO(
      String title,
      String id,
      List<String> authors,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      boolean useLocalData,
      boolean useAPI) {
    this.title = title;
    this.id = id;
    this.authors = authors;
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
    this.useLocalData = useLocalData;
    this.useAPI = useAPI;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public List<String> getAuthors() {
    return authors;
  }

  public void setAuthors(List<String> authors) {
    this.authors = authors;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }

  public boolean isUseLocalData() {
    return useLocalData;
  }

  public void setUseLocalData(boolean useLocalData) {
    this.useLocalData = useLocalData;
  }

  public boolean isUseAPI() {
    return useAPI;
  }

  public void setUseAPI(boolean useAPI) {
    this.useAPI = useAPI;
  }
}
