package de.ippolis.wp3.arxiv_resolver_service.model;

import org.springframework.stereotype.Component;

@Component
public class ArxivHelper {


  public String removeArxivPrefix(String id) {
    if (id.toLowerCase().contains("arxiv:")) {
      int index = id.toLowerCase().indexOf("arxiv:");
      id = id.substring(index + 6);
    }
    if (id.toLowerCase().contains("abs/")) {
      int index = id.toLowerCase().indexOf("abs/");
      id = id.substring(index + 4);
    }
    if (id.toLowerCase().contains("pdf/")) {
      int index = id.toLowerCase().indexOf("pdf/");
      id = id.substring(index + 4);
    }
    if (id.toLowerCase().endsWith(".pdf")) {
      id = id.substring(0,id.length() - 4);
    }
    return id;
  }


}
