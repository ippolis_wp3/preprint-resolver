package de.ippolis.wp3.arxiv_resolver_service.service;

import com.rometools.rome.feed.atom.Feed;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.feed.AtomFeedHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/** Provides REST functionality. */
@Service
public class RawRestServiceArxiv {

  private final RestTemplate rawRestTemplate;

  @Autowired
  public RawRestServiceArxiv(RestTemplate rawRestTemplate) {
    this.rawRestTemplate = rawRestTemplate;
    rawRestTemplate.getMessageConverters().add(new AtomFeedHttpMessageConverter());
  }

  /**
   * Performs a JSON GET request to the given URL trying to deserialize the result into the given
   * response type.
   */
  public <T> T performFeedGetRequest(String requestUri, Class<T> responseType) {
    return rawRestTemplate.getForObject(requestUri, responseType);
  }

  /**
   * Performs a JSON POST request to the given URL with the given parameters, converting the result
   * into a {@link JSONObject}.
   */
  public Feed performFeedGetRequest(String requestUri) {

    return this.performFeedGetRequest(requestUri, Feed.class);
  }
}
