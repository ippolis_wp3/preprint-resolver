package de.ippolis.wp3.arxiv_resolver_service.model;

import com.rometools.rome.feed.atom.Category;
import com.rometools.rome.feed.atom.Content;
import com.rometools.rome.feed.atom.Entry;
import com.rometools.rome.feed.atom.Link;
import com.rometools.rome.feed.synd.SyndPerson;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.jdom2.Element;

public class ArxivReference {
  private ArrayList<BibTexAuthor> authors;
  private String summary;
  private String title;
  private String published;
  private String updated;
  private ArrayList<String> alternateLinks;
  private String doi;
  private ArrayList<String> categories;
  private ArrayList<String> contents;
  private ArrayList<String> contributors;
  private ArrayList<String> otherLinks;
  private String id;
  private ArrayList<String> additionalInformation;

  public ArxivReference() {}

  public ArxivReference(Entry entry) {
    List<SyndPerson> authorsExtracted = entry.getAuthors();
    ArrayList<BibTexAuthor> authors = new ArrayList<>();
    for (SyndPerson author : authorsExtracted) {
      BibTexAuthor bibTexAuthor = new BibTexAuthor(author.getName());
      authors.add(bibTexAuthor);
    }
    this.setAuthors(authors);
    this.setSummary(entry.getSummary().getValue());
    this.setTitle(entry.getTitle());
    this.setPublished(entry.getPublished().toString());
    this.setUpdated(entry.getUpdated().toString());
    List<Link> alternateLinks = entry.getAlternateLinks();
    ArrayList<String> links = new ArrayList<>();
    for (Link link : alternateLinks) {
      links.add(link.getHref());
    }
    this.setAlternateLinks(links);
    List<Category> categories = entry.getCategories();
    ArrayList<String> cat = new ArrayList<>();
    for (Category category : categories) {
      cat.add(category.getTerm());
    }
    this.setCategories(cat);
    List<Content> contents = entry.getContents();
    ArrayList<String> cont = new ArrayList<>();
    for (Content content : contents) {
      cont.add(content.getValue());
    }
    this.setContents(cont);
    List<SyndPerson> contributors = entry.getContributors();
    ArrayList<String> contributorList = new ArrayList<>();
    for (SyndPerson person : contributors) {
      contributorList.add(person.getName());
    }
    this.setContributors(contributorList);
    ArrayList<String> otherLinks = new ArrayList<>();
    List<Link> otherLinksList = entry.getOtherLinks();
    for (Link link : otherLinksList) {
      if (link.getHref().contains("doi.org/")) {
        String href = link.getHref();
        int index = href.lastIndexOf("doi.org/");
        String doiString = href.substring(index + 8);
        setDoi(doiString);
      }
      otherLinks.add(link.getHref());
    }
    this.setOtherLinks(otherLinks);
    this.setId(entry.getId());
    List<Element> foreignMarkup = entry.getForeignMarkup();
    ArrayList<String> additionalInfo = new ArrayList<>();
    for (Element el : foreignMarkup) {
      if (!el.getValue().equals("")) {
        additionalInfo.add(el.getValue());
      }
    }
    setAdditionalInformation(additionalInfo);
  }

  public ArrayList<String> getAdditionalInformation() {
    return additionalInformation;
  }

  public void setAdditionalInformation(ArrayList<String> additionalInformation) {
    this.additionalInformation = additionalInformation;
  }

  public ArrayList<BibTexAuthor> getAuthors() {
    return authors;
  }

  public void setAuthors(ArrayList<BibTexAuthor> authors) {
    this.authors = authors;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public String getDoi() {
    return doi;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getPublished() {
    return published;
  }

  public void setPublished(String published) {
    this.published = published;
  }

  public String getUpdated() {
    return updated;
  }

  public void setUpdated(String updated) {
    this.updated = updated;
  }

  public ArrayList<String> getAlternateLinks() {
    return alternateLinks;
  }

  public void setAlternateLinks(ArrayList<String> alternateLinks) {
    this.alternateLinks = alternateLinks;
  }

  public ArrayList<String> getCategories() {
    return categories;
  }

  public void setCategories(ArrayList<String> categories) {
    this.categories = categories;
  }

  public ArrayList<String> getContents() {
    return contents;
  }

  public void setContents(ArrayList<String> contents) {
    this.contents = contents;
  }

  public ArrayList<String> getContributors() {
    return contributors;
  }

  public void setContributors(ArrayList<String> contributors) {
    this.contributors = contributors;
  }

  public ArrayList<String> getOtherLinks() {
    return otherLinks;
  }

  public void setOtherLinks(ArrayList<String> otherLinks) {
    this.otherLinks = otherLinks;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  private ArrayList<String> getListOfAuthorNames(boolean abbreviateNames) {
    ArrayList<String> authorNames = new ArrayList<>();
    for (BibTexAuthor author : this.authors) {
      authorNames.add(author.toBibTexAuthorString(abbreviateNames));
    }
    return authorNames;
  }

  public Map<String, Object> toMap(boolean abbreviateNames) {
    Map<String, Object> map = new LinkedHashMap<>();
    map.put("authors", getListOfAuthorNames(abbreviateNames));
    map.put("summary", summary);
    map.put("title", title);
    map.put("published", published);
    map.put("updated", updated);
    map.put("alternateLinks", alternateLinks);
    map.put("categories", categories);
    map.put("contents", contents);
    map.put("contributors", contributors);
    map.put("otherLinks", otherLinks);
    map.put("id", id);
    map.put("additionalInfo", additionalInformation);
    map.put("doi", doi);
    return map;
  }
}
