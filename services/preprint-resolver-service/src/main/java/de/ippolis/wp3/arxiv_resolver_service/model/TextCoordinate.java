package de.ippolis.wp3.arxiv_resolver_service.model;

public class TextCoordinate {
  private final int beginIndex;
  private final int endIndex;

  public TextCoordinate(int beginIndex, int endIndex) {
    this.beginIndex = beginIndex;
    this.endIndex = endIndex;
  }

  public int getBeginIndex() {
    return beginIndex;
  }

  public int getEndIndex() {
    return endIndex;
  }
}
