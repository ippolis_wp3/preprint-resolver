package de.ippolis.wp3.arxiv_resolver_service.controller;

import de.ippolis.wp3.arxiv_resolver_service.model.ArxivDoiTitleAuthorRequestDTO;
import de.ippolis.wp3.arxiv_resolver_service.model.ArxivHelper;
import de.ippolis.wp3.arxiv_resolver_service.model.BibTexPagePreprocessing;
import de.ippolis.wp3.arxiv_resolver_service.model.json.ErrorJsonResponse;
import de.ippolis.wp3.arxiv_resolver_service.model.json.JsonResponse;
import de.ippolis.wp3.arxiv_resolver_service.model.json.SuccessJsonResponse;
import de.ippolis.wp3.arxiv_resolver_service.service.ArxivResolverService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.lang.LocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArxivResolverController {

  private final ArxivResolverService arxivResolverService;
  private final ArxivHelper arxivHelper;
  private final Logger logger = LoggerFactory.getLogger(ArxivResolverController.class);
  private final MessageSource messageSource;

  @Autowired
  public ArxivResolverController(
      ArxivResolverService arxivResolverService, ArxivHelper arxivHelper,
      MessageSource messageSource) {
    this.arxivResolverService = arxivResolverService;
    this.arxivHelper = arxivHelper;
    this.messageSource = messageSource;
  }

  @CrossOrigin(origins = "*")
  @GetMapping("/arxiv/resolve")
  public JsonResponse resolve(@RequestParam("arxivId") String arxivId, @RequestParam Optional<String> locale) {
    final Locale resolvedLocale = LocaleUtils.toLocale(locale.orElse("en_US"));
    arxivId = arxivId.strip();
    arxivId = arxivHelper.removeArxivPrefix(arxivId);
    final Map<String, Object> resultsArxiv =
        arxivResolverService.getInformationFromArxivByID(arxivId, true);

    if (resultsArxiv == null) {
      return new ErrorJsonResponse(this.messageSource.getMessage("arxiv.resolve.error.invalid_id", null, resolvedLocale));
    }

    List<Map<String, Object>> resultsOpenAlex = new LinkedList<>();
    List<Map<String, Object>> resultsSemanticScholar = new LinkedList<>();
    List<Map<String, Object>> resultsDBLP = new LinkedList<>();
    List<Map<String, Object>> resultsCrossRef = new LinkedList<>();
    String doi = "";
    if (resultsArxiv.containsKey("doi")) {
      Object doiFoundObject = resultsArxiv.get("doi");
      if (doiFoundObject != null) {
        doi = doiFoundObject.toString();
      }
    }

    if (resultsArxiv.containsKey("title")) {
      Object titleObject = resultsArxiv.get("title");
      if (titleObject instanceof String) {
        String title = titleObject.toString();
        if (resultsArxiv.containsKey("authors")) {
          Object authorsObject = resultsArxiv.get("authors");
          if (authorsObject instanceof ArrayList<?>) {
            List<String> authors = (ArrayList<String>) authorsObject;
            resultsSemanticScholar =
                this.arxivResolverService
                    .resolveArxivByIdUsingSemanticScholar(
                        arxivId,
                        arxivId,
                        doi,
                        title,
                        authors,
                        true,
                        true,
                        BibTexPagePreprocessing.SINGLE_MINUS_WITH_SPACES)
                    .get("resultsMap");

            resultsDBLP =
                this.arxivResolverService
                    .resolveArxivByIdUsingDBLP(
                        arxivId,
                        arxivId,
                        doi,
                        title,
                        authors,
                        true,
                        true,
                        BibTexPagePreprocessing.SINGLE_MINUS_WITH_SPACES)
                    .get("resultsMap");
            resultsCrossRef =
                this.arxivResolverService
                    .resolveArxivByTitleAndAuthorsCrossRef(
                        arxivId,
                        arxivId,
                        title,
                        authors,
                        doi,
                        BibTexPagePreprocessing.SINGLE_MINUS_WITH_SPACES,
                        true,
                        true)
                    .get("resultsMap");
            resultsOpenAlex =
                this.arxivResolverService
                    .resolveArxivByTitleAuthorsAndIdOpenAlex(
                        arxivId,
                        title,
                        authors,
                        doi,
                        BibTexPagePreprocessing.SINGLE_MINUS_WITH_SPACES,
                        true,
                        true)
                    .get("resultsMap");
          }
        }
      }
    }

    final Map<String, Object> allResults = new HashMap<>();
    allResults.put("arxiv", resultsArxiv);
    allResults.put("semantic_scholar", resultsSemanticScholar);
    allResults.put("dblp", resultsDBLP);
    allResults.put("openalex", resultsOpenAlex);
    allResults.put("cross_ref", resultsCrossRef);

    return new SuccessJsonResponse(allResults);
  }

  @GetMapping("/arxiv/getInformationByID")
  private JsonResponse getInformationFromArxivByID(String arxivId) {
    Map<String, Object> results = arxivResolverService.getInformationFromArxivByID(arxivId, true);
    return new SuccessJsonResponse(results);
  }

  @GetMapping("/arxiv/getVersionHistoryByID")
  private JsonResponse getVersionHistoryFromArxivByID(@RequestParam("arxivId") String arxivId) {
    List<Map<String, Object>> results =
        arxivResolverService.getVersionHistoryFromArxivByID(arxivId);
    return new SuccessJsonResponse(results);
  }

  @GetMapping("/arxiv/resolveSemanticScholar")
  public JsonResponse resolveSemanticScholar(
      @RequestParam("arxivId") String arxivId,
      @RequestParam("useLocalData") boolean useLocalData,
      @RequestParam("useAPI") boolean useAPI) {
    arxivId = arxivId.strip();
    arxivId = arxivHelper.removeArxivPrefix(arxivId);
    final Map<String, Object> resultsArxiv =
        arxivResolverService.getInformationFromArxivByID(arxivId, true);
    Map<String, List<Map<String, Object>>> resultSemanticScholar = null;
    if (resultsArxiv == null) {
      return new ErrorJsonResponse("__Invalid arXiv-ID");
    }

    String doi = "";
    if (resultsArxiv.containsKey("doi")) {
      Object doiFoundObject = resultsArxiv.get("doi");
      if (doiFoundObject != null) {
        doi = doiFoundObject.toString();
      }
    }
    // TODO handle error case separately
    if (resultsArxiv.containsKey("title")) {
      Object titleObject = resultsArxiv.get("title");
      if (titleObject instanceof String) {
        String title = titleObject.toString();
        if (resultsArxiv.containsKey("authors")) {
          Object authorsObject = resultsArxiv.get("authors");
          if (authorsObject instanceof ArrayList<?>) {
            List<String> authors = (ArrayList<String>) authorsObject;
            resultSemanticScholar =
                this.arxivResolverService.resolveArxivByIdUsingSemanticScholar(
                    arxivId,
                    arxivId,
                    doi,
                    title,
                    authors,
                    useLocalData,
                    useAPI,
                    BibTexPagePreprocessing.SINGLE_MINUS_WITH_SPACES);
          }
        }
      }
    }
    return new SuccessJsonResponse(resultSemanticScholar);
  }

  @GetMapping("/arxiv/resolveOpenalex")
  public JsonResponse resolveOpenalex(
      @RequestParam("arxivId") String arxivId,
      @RequestParam("useLocalData") boolean useLocalData,
      @RequestParam("useAPI") boolean useAPI) {
    arxivId = arxivId.strip();
    arxivId = arxivHelper.removeArxivPrefix(arxivId);
    final Map<String, Object> resultsArxiv =
        arxivResolverService.getInformationFromArxivByID(arxivId, true);

    if (resultsArxiv == null) {
      return new ErrorJsonResponse("__Invalid arXiv-ID");
    }

    String doi = "";
    if (resultsArxiv.containsKey("doi")) {
      Object doiFoundObject = resultsArxiv.get("doi");
      if (doiFoundObject != null) {
        doi = doiFoundObject.toString();
      }
    }

    Map<String, List<Map<String, Object>>> resultsOpenAlex = null;
    if (resultsArxiv.containsKey("title")) {
      Object titleObject = resultsArxiv.get("title");
      if (titleObject instanceof String) {
        String title = titleObject.toString();
        if (resultsArxiv.containsKey("authors")) {
          Object authorsObject = resultsArxiv.get("authors");
          if (authorsObject instanceof ArrayList<?>) {
            ArrayList<String> authors = (ArrayList<String>) authorsObject;
            resultsOpenAlex =
                this.arxivResolverService.resolveArxivByTitleAuthorsAndIdOpenAlex(
                    arxivId,
                    title,
                    authors,
                    doi,
                    BibTexPagePreprocessing.SINGLE_MINUS_WITH_SPACES,
                    useLocalData,
                    useAPI);
          }
        }
      }
    }

    return new SuccessJsonResponse(resultsOpenAlex);
  }

  @GetMapping("/arxiv/resolveCrossCite")
  public JsonResponse resolveCrossCite(
      @RequestParam("arxivId") String arxivId,
      @RequestParam("useLocalData") boolean useLocalData,
      @RequestParam("useAPI") boolean useAPI) {
    arxivId = arxivId.strip();
    arxivId = arxivHelper.removeArxivPrefix(arxivId);
    final Map<String, Object> resultsArxiv =
        arxivResolverService.getInformationFromArxivByID(arxivId, true);

    if (resultsArxiv == null) {
      return new ErrorJsonResponse("__Invalid arXiv-ID");
    }

    String doi = "";
    if (resultsArxiv.containsKey("doi")) {
      Object doiFoundObject = resultsArxiv.get("doi");
      if (doiFoundObject != null) {
        doi = doiFoundObject.toString();
      }
    }

    Map<String, List<Map<String, Object>>> resultsCrossCite = null;
    if (resultsArxiv.containsKey("title")) {
      Object titleObject = resultsArxiv.get("title");
      if (titleObject instanceof String) {
        String title = titleObject.toString();
        if (resultsArxiv.containsKey("authors")) {
          Object authorsObject = resultsArxiv.get("authors");
          if (authorsObject instanceof ArrayList<?>) {
            ArrayList<String> authors = (ArrayList<String>) authorsObject;
            resultsCrossCite =
                this.arxivResolverService.resolveArxivByTitleAndAuthorsCrossRef(
                    arxivId,
                    arxivId,
                    title,
                    authors,
                    doi,
                    BibTexPagePreprocessing.SINGLE_MINUS_WITH_SPACES,
                    useLocalData,
                    useAPI);
          }
        }
      }
    }

    return new SuccessJsonResponse(resultsCrossCite);
  }

  @GetMapping("/arxiv/resolveDBLP")
  public JsonResponse resolveDBLP(
      @RequestParam("arxivId") String arxivId,
      @RequestParam("useLocalData") boolean useLocalData,
      @RequestParam("useAPI") boolean useAPI) {
    arxivId = arxivId.strip();
    arxivId = arxivHelper.removeArxivPrefix(arxivId);
    final Map<String, Object> resultsArxiv =
        arxivResolverService.getInformationFromArxivByID(arxivId, true);

    if (resultsArxiv == null) {
      return new ErrorJsonResponse("__Invalid arXiv-ID");
    }

    String doi = "";
    if (resultsArxiv.containsKey("doi")) {
      Object doiFoundObject = resultsArxiv.get("doi");
      if (doiFoundObject != null) {
        doi = doiFoundObject.toString();
      }
    }
    // TODO handle error case separately
    if (resultsArxiv.containsKey("title")) {
      Object titleObject = resultsArxiv.get("title");
      if (titleObject instanceof String) {
        String title = titleObject.toString();
        if (resultsArxiv.containsKey("authors")) {
          Object authorsObject = resultsArxiv.get("authors");
          if (authorsObject instanceof ArrayList<?>) {
            List<String> authors = (ArrayList<String>) authorsObject;
            Map<String, List<Map<String, Object>>> resultDBLP =
                this.arxivResolverService.resolveArxivByIdUsingDBLP(
                    arxivId,
                    arxivId,
                    doi,
                    title,
                    authors,
                    useLocalData,
                    useAPI,
                    BibTexPagePreprocessing.SINGLE_MINUS_WITH_SPACES);

            /*    if (resultDBLP.size() == 0) {
                  messageSource.getMessage(
                      "analysis.arxivchecker.DBLP.notfound", new Object[] {}, messageSource.getLocale());
                }
            */
            return new SuccessJsonResponse(resultDBLP);
          }
        }
      }
    }
    return new SuccessJsonResponse(null);
  }

  @PostMapping(
      value = "/arxiv/correctArxivByDBLPToString",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse solveArxivByArxivID(@RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      String id = requestDTO.getId();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();

      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      String result =
          this.arxivResolverService.correctArxivDBLP(
              id, title, authors, doi, arxivId, bibTexPagePreprocessing, useLocalData, useAPI);
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/arxiv/correctArxivBySemanticScholarToString",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse solveArxivByArxivIDSemanticScholar(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      String id = requestDTO.getId();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      String result =
          this.arxivResolverService.correctArxivSemanticScholar(
              id, title, authors, doi, arxivId, useLocalData, useAPI, bibTexPagePreprocessing);
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/arxiv/correctArxivByOpenAlexToString",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse solveArxivByArxivIDOpenAlex(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      String id = requestDTO.getId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      String result =
          this.arxivResolverService.correctArxivOpenAlex(
              id, title, authors, bibTexPagePreprocessing, doi, arxivId, useLocalData, useAPI);
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/arxiv/correctArxivByCrossCiteToString",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse solveArxivByArxivIDCrossCite(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      String id = requestDTO.getId();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      String result =
          this.arxivResolverService.correctArxivCrossCite(
              id, title, authors, doi, arxivId, useLocalData, useAPI, bibTexPagePreprocessing);
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }
}
