package de.ippolis.wp3.arxiv_resolver_service.model.json;

/**
 * JSON response representing a failure (e.g., invalid parameters)
 * <p>
 * Implementation of the JSend specification. See https://github.com/omniti-labs/jsend for details.
 */
public class FailJsonResponse extends DataJsonResponse {

  public FailJsonResponse(Object data) {
    super(JsonResponseStatus.FAIL, data);
  }
}
