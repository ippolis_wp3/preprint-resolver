package de.ippolis.wp3.arxiv_resolver_service.model;

public enum BibTexPagePreprocessing {
  DOUBLE_MINUS_WITH_SPACES,
  DOUBLE_MINUS_WITHOUT_SPACES,
  SINGLE_MINUS_WITH_SPACES,
  SINGLE_MINUS_WITHOUT_SPACES,
  NO_PREPROCESSING
}
