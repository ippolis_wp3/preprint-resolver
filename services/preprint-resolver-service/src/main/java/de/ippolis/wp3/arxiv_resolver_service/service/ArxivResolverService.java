package de.ippolis.wp3.arxiv_resolver_service.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rometools.rome.feed.atom.Entry;
import com.rometools.rome.feed.atom.Feed;
import de.ippolis.wp3.arxiv_resolver_service.model.ArxivReference;
import de.ippolis.wp3.arxiv_resolver_service.model.BibTexPagePreprocessing;
import java.util.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArxivResolverService {

  private final RawRestServiceArxiv restServiceArxiv;
  private final RestService restService;
  private final Logger logger = LoggerFactory.getLogger(ArxivResolverService.class);

  @Autowired
  public ArxivResolverService(RawRestServiceArxiv restServiceArxiv, RestService restService) {
    this.restServiceArxiv = restServiceArxiv;
    this.restService = restService;
  }

  /**
   * corrects arxiv reference using semanticscholar bibliography database
   *
   * @param keyValue bibtex entry identifier
   * @param titleString title of arxiv reference
   * @param authorsString list of surnames
   * @return corrected bibtex entry of arxiv reference, or empty string if reference was not found
   *     in semanticscholar
   */
  public String correctArxivSemanticScholar(
      String keyValue,
      String titleString,
      List<String> authorsString,
      String doi,
      String arxivId,
      boolean useLocalData,
      boolean useAPI,
      BibTexPagePreprocessing bibTexPagePreprocessing) {

    JSONObject requestParameters = new JSONObject();
    requestParameters.put("id", keyValue);
    requestParameters.put("doi", doi);
    requestParameters.put("arxivId", arxivId);
    requestParameters.put("title", titleString);
    requestParameters.put("authors", authorsString);
    requestParameters.put("useLocalData", useLocalData);
    requestParameters.put("useAPI", useAPI);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);

    final JSONObject response =
        this.restService.performJsonPostRequest(
            "/semantic-scholar/resolveArxiv", requestParameters);

    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        JSONObject data = response.getJSONObject("data");
        if (data.keySet().contains("result")) {
          return data.get("result").toString();
        }
      }
    }
    return "";
  }

  /**
   * corrects arxiv reference using dblp bibliography database
   *
   * @param keyValue bibtex entry identifier
   * @param titleString title of arxiv reference
   * @param authorsString list of surnames
   * @return corrected bibtex entry of arxiv reference, or empty string if reference was not found
   *     in dblp
   */
  public String correctArxivDBLP(
      String keyValue,
      String titleString,
      List<String> authorsString,
      String doi,
      String arxivId,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      boolean useLocalData,
      boolean useAPI) {
    JSONObject requestParameters = new JSONObject();
    requestParameters.put("title", titleString);
    requestParameters.put("authors", authorsString);
    requestParameters.put("doi", doi);
    requestParameters.put("id", keyValue);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);
    requestParameters.put("useLocalData", useLocalData);
    requestParameters.put("useAPI", useAPI);
    requestParameters.put("arxivId", arxivId);
    final JSONObject response =
        this.restService.performJsonPostRequest(
            "/dblp/resolveArxivToLatexString", requestParameters);

    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        JSONObject data = response.getJSONObject("data");
        if (data.keySet().contains("result")) {
          return data.get("result").toString();
        }
      }
    }
    return "";
  }

  /**
   * check if there is a published version in semantic scholar to resolve a given arxiv id
   *
   * @param arxivId given arxiv id to solve
   * @param bibTexPagePreprocessing page formatting
   * @return Map containing published version of arxiv reference if found in semanticscholar, null
   *     otherwise
   */
  public Map<String, List<Map<String, Object>>> resolveArxivByIdUsingSemanticScholar(
      String id,
      String arxivId,
      String doi,
      String title,
      List<String> authors,
      boolean useLocalData,
      boolean useAPI,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    JSONObject requestParameters = new JSONObject();
    requestParameters.put("arxivId", arxivId);
    requestParameters.put("doi", doi);
    requestParameters.put("id", id);
    requestParameters.put("useLocalData", useLocalData);
    requestParameters.put("useAPI", useAPI);
    requestParameters.put("title", title);
    requestParameters.put("authors", authors);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);
    final JSONObject response =
        this.restService.performJsonPostRequest(
            "/semantic-scholar/resolveArxivFrontend", requestParameters);
    Map<String, List<Map<String, Object>>> results = new LinkedHashMap();
    List<Map<String, Object>> list = new LinkedList<>();
    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        JSONObject data = response.getJSONObject("data");
        if (data.keySet().contains("resultsMap")) {
          try {
            HashMap<String, Object>[] result =
                new ObjectMapper().readValue(data.get("resultsMap").toString(), HashMap[].class);
            list.addAll(Arrays.asList(result));
          } catch (Exception e) {
            logger.debug(e.getMessage());
          }
        }
        List<Map<String, Object>> filterLogs = new LinkedList<>();
        if (data.keySet().contains("filterLogs")) {
          try {
            HashMap<String, Object>[] result =
                new ObjectMapper().readValue(data.get("filterLogs").toString(), HashMap[].class);
            filterLogs.addAll(Arrays.asList(result));
          } catch (Exception e) {
            logger.debug(e.getMessage());
          }
        }
        results.put("resultsMap", list);
        results.put("filterLogs", filterLogs);
        return results;
      }
    } else {
      logger.debug(response.getString("message"));
      return new LinkedHashMap<>();
    }
    return new LinkedHashMap<>();
  }

  /**
   * check if there is a published version in dblp to resolve a given arxiv id
   *
   * @param arxivId given arxiv id to solve
   * @param bibTexPagePreprocessing page formatting
   * @return Map containing published version of arxiv reference if found in dblp, null otherwise
   */
  public Map<String, List<Map<String, Object>>> resolveArxivByIdUsingDBLP(
      String id,
      String arxivId,
      String doi,
      String title,
      List<String> authors,
      boolean useLocalData,
      boolean useAPI,
      BibTexPagePreprocessing bibTexPagePreprocessing) {

    JSONObject requestParameters = new JSONObject();
    requestParameters.put("arxivId", arxivId);
    requestParameters.put("title", title);
    requestParameters.put("id", id);
    requestParameters.put("doi", doi);
    requestParameters.put("authors", authors);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);
    requestParameters.put("useLocalData", useLocalData);
    requestParameters.put("useAPI", useAPI);
    final JSONObject response =
        this.restService.performJsonPostRequest("/dblp/resolveArxivToMap", requestParameters);
    Map<String, List<Map<String, Object>>> results = new LinkedHashMap<>();
    List<Map<String, Object>> list = new LinkedList<>();
    List<Map<String, Object>> filterLogs = new LinkedList<>();
    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        JSONObject data = response.getJSONObject("data");
        if (data.keySet().contains("resultsMap")) {
          try {
            HashMap<String, Object>[] result =
                new ObjectMapper().readValue(data.get("resultsMap").toString(), HashMap[].class);
            list.addAll(Arrays.asList(result));
          } catch (Exception e) {
            logger.debug(e.getMessage());
          }
        }
        if (data.keySet().contains("filterLogs")) {
          try {
            HashMap<String, Object>[] result =
                new ObjectMapper().readValue(data.get("filterLogs").toString(), HashMap[].class);
            filterLogs.addAll(Arrays.asList(result));
          } catch (Exception e) {
            logger.debug(e.getMessage());
          }
        }
        results.put("resultsMap", list);
        results.put("filterLogs", filterLogs);

        return results;
      }
    } else {
      logger.debug(response.getString("message"));
      return new LinkedHashMap<>();
    }
    return new LinkedHashMap<>();
  }

  /**
   * extract arxiv information for arxiv id
   *
   * @param id arxiv id
   * @param abbreviateNames name formatting
   * @return Map containing information, if valid arxiv id, null otherwise
   */
  public Map<String, Object> getInformationFromArxivByID(String id, boolean abbreviateNames) {
    if (id.equals("")) {
      return null;
    }
    id = id.toLowerCase();
    String basicUrl = "https://export.arxiv.org/api/query?id_list=";
    String url = basicUrl + id;
    try {
      final Feed response = restServiceArxiv.performFeedGetRequest(url);
      List<Entry> entries = response.getEntries();
      for (Entry entry : entries) {
        ArxivReference ref = new ArxivReference(entry);
        if (ref.getId().toLowerCase().contains(id.toLowerCase())) {
          return ref.toMap(abbreviateNames);
        }
      }

      return (null);
    } catch (Exception e) {
      logger.debug(e.getMessage());
      return null;
    }
  }

  /**
   * check if there is a published version in openalex to resolve a given arxiv id
   *
   * @param arxivId given arxiv id to solve
   * @param bibTexPagePreprocessing page formatting
   * @return Map containing published version of arxiv reference if found in openalex, null
   *     otherwise
   */
  public Map<String, List<Map<String, Object>>> resolveArxivByTitleAuthorsAndIdOpenAlex(
      String arxivId,
      String title,
      List<String> authors,
      String doi,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      boolean useLocalData,
      boolean useAPI) {

    JSONObject requestParameters = new JSONObject();
    requestParameters.put("arxivId", arxivId);
    requestParameters.put("title", title);
    requestParameters.put("authors", authors);
    requestParameters.put("doi", doi);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);
    requestParameters.put("useLocalData", useLocalData);
    requestParameters.put("useAPI", useAPI);

    final JSONObject response =
        this.restService.performJsonPostRequest(
            "/open-alex/resolveArxivFrontend", requestParameters);
    Map<String, List<Map<String, Object>>> results = new LinkedHashMap();
    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        List<Map<String, Object>> list = new LinkedList<>();
        List<Map<String, Object>> filterLogs = new LinkedList<>();
        JSONObject data = response.getJSONObject("data");
        if (data.keySet().contains("resultsMap")) {
          try {
            HashMap<String, Object>[] result =
                new ObjectMapper().readValue(data.get("resultsMap").toString(), HashMap[].class);
            list.addAll(Arrays.asList(result));
          } catch (Exception e) {
            logger.debug(e.getMessage());
          }
        }
        if (data.keySet().contains("filterLogs")) {
          try {
            HashMap<String, Object>[] result =
                new ObjectMapper().readValue(data.get("filterLogs").toString(), HashMap[].class);
            filterLogs.addAll(Arrays.asList(result));
          } catch (Exception e) {
            logger.debug(e.getMessage());
          }
        }
        results.put("resultsMap", list);
        results.put("filterLogs", filterLogs);
        return results;
      }
    } else {
      logger.debug(response.getString("message"));
      return new LinkedHashMap<>();
    }
    return new LinkedHashMap<>();
  }

  /**
   * check if there is a published version in cross cite to resolve a given arxiv id
   *
   * @param arxivId given arxiv id to solve
   * @param bibTexPagePreprocessing page formatting
   * @return Map containing published version of arxiv reference if found in cross cite, null
   *     otherwise
   */
  public Map<String, List<Map<String, Object>>> resolveArxivByTitleAndAuthorsCrossRef(
      String id,
      String arxivId,
      String title,
      List<String> authors,
      String doi,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      boolean useLocalData,
      boolean useAPI) {
    JSONObject requestParameters = new JSONObject();
    requestParameters.put("arxivId", arxivId);
    requestParameters.put("id", id);
    requestParameters.put("useLocalData", useLocalData);
    requestParameters.put("useAPI", useAPI);
    requestParameters.put("title", title);
    requestParameters.put("authors", authors);
    requestParameters.put("doi", doi);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);

    final JSONObject response =
        this.restService.performJsonPostRequest(
            "/cross-cite/resolveArxivFrontend", requestParameters);
    Map<String, List<Map<String, Object>>> results = new LinkedHashMap<>();
    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        JSONObject data = response.getJSONObject("data");
        List<Map<String, Object>> list = new LinkedList<>();
        List<Map<String, Object>> filterLogs = new LinkedList<>();
        if (data.keySet().contains("resultsMap")) {
          try {
            HashMap<String, Object>[] result =
                new ObjectMapper().readValue(data.get("resultsMap").toString(), HashMap[].class);
            list.addAll(Arrays.asList(result));
          } catch (Exception e) {
            logger.debug(e.getMessage());
          }
        }
        if (data.keySet().contains("filterLogs")) {
          try {
            HashMap<String, Object>[] result =
                new ObjectMapper().readValue(data.get("filterLogs").toString(), HashMap[].class);
            filterLogs.addAll(Arrays.asList(result));
          } catch (Exception e) {
            logger.debug(e.getMessage());
          }
        }
        results.put("resultsMap", list);
        results.put("filterLogs", filterLogs);
        return results;
      }
    } else {
      logger.debug(response.getString("message"));
      return new LinkedHashMap<>();
    }
    return new LinkedHashMap<>();
  }

  /**
   * extract arxiv version history of given arxiv id
   *
   * @param id arxiv id
   * @return list of maps containing arxiv versions if arxiv id is valid, null otherwise
   */
  public List<Map<String, Object>> getVersionHistoryFromArxivByID(String id) {
    ArrayList<Map<String, Object>> resultsList = new ArrayList<>();
    if (id.equals("")) {
      return resultsList;
    }
    String basicUrl = "https://export.arxiv.org/api/query?id_list=";
    String url = basicUrl + id;
    try {
      final Feed response = restServiceArxiv.performFeedGetRequest(url);
      List<Entry> entries = response.getEntries();
      for (Entry entry : entries) {
        String idExtracted = entry.getId();
        String version = "";
        if (idExtracted.contains("v")) {
          int index = idExtracted.lastIndexOf("v");
          version = idExtracted.substring(index + 1);
        }
        try {
          int versionNumber = Integer.valueOf(version);
          for (int i = 1; i <= versionNumber; i++) {
            String idNew = id;
            if (idNew.contains("v")) {
              int indexNew = idNew.lastIndexOf("v");
              idNew = idNew.substring(indexNew + 1);
              idNew = idNew + "v" + i;
            } else {
              idNew = idNew + "v" + i;
            }
            Map<String, Object> versionMap = getInformationFromArxivByID(idNew, true);
            resultsList.add(versionMap);
          }
        } catch (NumberFormatException e) {
          logger.debug(e.getMessage());
          return resultsList;
        }
      }

    } catch (Exception e) {
      logger.debug(e.getMessage());
      return resultsList;
    }
    return resultsList;
  }

  public String correctArxivOpenAlex(
      String id,
      String title,
      List<String> authors,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      String doi,
      String arxivId,
      boolean useLocalData,
      boolean useAPI) {
    JSONObject requestParameters = new JSONObject();
    requestParameters.put("id", id);
    requestParameters.put("title", title);
    requestParameters.put("authors", authors);
    requestParameters.put("doi", doi);
    requestParameters.put("arxivId", arxivId);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);
    requestParameters.put("useLocalData", useLocalData);
    requestParameters.put("useAPI", useAPI);
    final JSONObject response =
        this.restService.performJsonPostRequest(
            "/open-alex/resolveArxivToString", requestParameters);

    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        JSONObject data = response.getJSONObject("data");
        if (data.keySet().contains("result")) {
          return data.get("result").toString();
        }
      }
    } else {
      logger.debug(response.getString("message"));
      return "";
    }
    return "";
  }

  public String correctArxivCrossCite(
      String id,
      String title,
      List<String> authors,
      String doi,
      String arxivId,
      boolean useLocalData,
      boolean useAPI,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    JSONObject requestParameters = new JSONObject();
    requestParameters.put("id", id);
    requestParameters.put("title", title);
    requestParameters.put("authors", authors);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);
    requestParameters.put("doi", doi);
    requestParameters.put("arxivId", arxivId);
    requestParameters.put("useLocalData", useLocalData);
    requestParameters.put("useAPI", useAPI);
    final JSONObject response =
        this.restService.performJsonPostRequest(
            "/cross-cite/resolveArxivToString", requestParameters);

    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        JSONObject data = response.getJSONObject("data");
        if (data.keySet().contains("result")) {
          return data.get("result").toString();
        }
      }
    }
    return "";
  }
}
