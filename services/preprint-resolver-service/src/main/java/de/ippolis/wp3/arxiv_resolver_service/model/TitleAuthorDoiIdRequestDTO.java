package de.ippolis.wp3.arxiv_resolver_service.model;

import java.util.List;

public class TitleAuthorDoiIdRequestDTO {

  private String title;
  private String id;
  private List<String> authors;
  private BibTexPagePreprocessing bibTexPagePreprocessing;
  private String doi;
  private boolean useLocalData;
  private boolean useAPI;

  public TitleAuthorDoiIdRequestDTO() {}

  public TitleAuthorDoiIdRequestDTO(
      String title,
      String id,
      List<String> authors,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      String doi,
      boolean useLocalData,
      boolean useAPI) {
    this.title = title;
    this.id = id;
    this.authors = authors;
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
    this.doi = doi;
    this.useLocalData = useLocalData;
    this.useAPI = useAPI;
  }

  public String getDoi() {
    return doi;
  }

  public boolean isUseLocalData() {
    return useLocalData;
  }

  public void setUseLocalData(boolean useLocalData) {
    this.useLocalData = useLocalData;
  }

  public boolean isUseAPI() {
    return useAPI;
  }

  public void setUseAPI(boolean useAPI) {
    this.useAPI = useAPI;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public List<String> getAuthors() {
    return authors;
  }

  public void setAuthors(List<String> authors) {
    this.authors = authors;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }
}
