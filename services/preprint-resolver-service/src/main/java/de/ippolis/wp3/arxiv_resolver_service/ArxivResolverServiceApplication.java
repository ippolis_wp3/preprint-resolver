package de.ippolis.wp3.arxiv_resolver_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * This is the application entry point. Change the class name as necessary.
 * <p>
 * Usually there is no reason to add any other code in this file.
 **/
@SpringBootApplication
@EnableCaching
public class ArxivResolverServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(ArxivResolverServiceApplication.class, args);
  }

  @Bean("rawRestTemplate")
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }
}
