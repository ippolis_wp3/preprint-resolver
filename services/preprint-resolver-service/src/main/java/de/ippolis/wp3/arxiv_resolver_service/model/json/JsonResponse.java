package de.ippolis.wp3.arxiv_resolver_service.model.json;

/**
 * Tagging interface for JSON responses providing a response status
 *
 * TODO make common interface for all responses: add getData, getMessage, isFail, isSuccess etc.,
 *  then use factory to create success/error/fail responses
 */
public interface JsonResponse {

  JsonResponseStatus getStatus();

  void setStatus(JsonResponseStatus status);
}
