import { sveltekit } from '@sveltejs/kit/vite';
import path from "path";

/** @type {import('vite').UserConfig} */
const config = {
  plugins: [sveltekit()],
  resolve: {
    alias: {
      // these are the aliases and paths to them
      $assets: path.resolve("./static"),
      $lib: path.resolve("./src/lib"),
      $src: path.resolve("./src")
    },
  },
  server: {
    fs: {
      allow: ['static'],
    },
    port: 3000,
  },
  ssr: {
    noExternal: '@popperjs/core',
  },
};

export default config;
