rm -rf static/vendor

# jQuery
mkdir -p static/vendor/jquery/dist
cp node_modules/jquery/dist/jquery.min.* static/vendor/jquery/dist/

# jQuery json-viewer
mkdir -p static/vendor/jquery.json-viewer/json-viewer
cp node_modules/jquery.json-viewer/json-viewer/* static/vendor/jquery.json-viewer/json-viewer

# Bootstrap
mkdir -p  static/vendor/bootstrap/dist/js
mkdir -p  static/vendor/bootstrap/dist/css
cp node_modules/bootstrap/dist/js/bootstrap.bundle.min.js* static/vendor/bootstrap/dist/js/
cp node_modules/bootstrap/dist/css/bootstrap.min.css* static/vendor/bootstrap/dist/css/

# Bootstrap Icons
mkdir -p  static/vendor/bootstrap-icons
cp -r node_modules/bootstrap-icons/font static/vendor/bootstrap-icons/

# Bootstrap Table
mkdir -p static/vendor/bootstrap-table/dist/locale
mkdir -p static/vendor/bootstrap-table/dist/themes/bootstrap-table
cp node_modules/bootstrap-table/dist/bootstrap-table.min.js static/vendor/bootstrap-table/dist/
cp node_modules/bootstrap-table/dist/locale/bootstrap-table-de-DE.min.js static/vendor/bootstrap-table/dist/locale/
cp node_modules/bootstrap-table/dist/locale/bootstrap-table-en-US.min.js static/vendor/bootstrap-table/dist/locale/
cp node_modules/bootstrap-table/dist/themes/bootstrap-table/bootstrap-table.min.* static/vendor/bootstrap-table/dist/themes/bootstrap-table/
cp -r node_modules/bootstrap-table/dist/themes/bootstrap-table/fonts static/vendor/bootstrap-table/dist/themes/bootstrap-table/
