import { browser } from "$app/environment";

function addLocalStorageSync(store, localStorageKey, defaultValue) {
  if (!browser) return null;

  if (localStorage.getItem(localStorageKey) !== null) {
    let value = getLocalStorageItem(localStorageKey);
    store.set(value);
  }

  store.subscribe((value) => {
    setLocalStorageItem(localStorageKey, value);
  });
}

function setLocalStorageItem(key, value) {
  if (!browser) return null;

  localStorage.setItem(key, JSON.stringify(value));
}

function getLocalStorageItem(key, defaultValue = null) {
  if (!browser) return null;

  if (localStorage.getItem(key) === null) {
    return defaultValue;
  }

  try {
    return JSON.parse(localStorage.getItem(key));
  } catch (e) {
    return defaultValue;
  }

}

export { addLocalStorageSync, getLocalStorageItem, setLocalStorageItem };
