import { _ } from "svelte-i18n";

let $_;

_.subscribe((value) => ($_ = value));

let JsonResponseStatus = {
  SUCCESS: "SUCCESS",
  FAIL: "FAIL",
  ERROR: "ERROR",
}

class JsonResponse {
  constructor(status) {
    this.status = status;
  }
}

class DataJsonResponse extends JsonResponse {
  constructor(status, data) {
    super(status);

    this.data = data;
  }
}

class SuccessJsonResponse extends DataJsonResponse {
  constructor(data) {
    super(JsonResponseStatus.SUCCESS, data);
  }
}

class FailJsonResponse extends DataJsonResponse {
  constructor(data) {
    super(JsonResponseStatus.FAIL, data);
  }
}

class ErrorJsonResponse extends JsonResponse {
  constructor(message) {
    super(JsonResponseStatus.ERROR);
    this.message = message;
  }
}

async function parseResponse(rawResponse) {
  const response = await rawResponse.json();
  let jsonResponse;

  // TODO check if rawResponse.ok is true, if not craft ErrorJsonResponse using .text()

  switch (response.status) {
    case JsonResponseStatus.SUCCESS:
      jsonResponse = new SuccessJsonResponse(response.data);
      break;
    case JsonResponseStatus.FAIL:
      jsonResponse = new FailJsonResponse(response.data);
      break;
    case JsonResponseStatus.ERROR:
      jsonResponse = new ErrorJsonResponse(response.message);
      break;
    default:
      jsonResponse = new ErrorJsonResponse($_("utils.json.response.invalid")); // TODO i18n
  }

  return jsonResponse;
}

export { parseResponse, JsonResponseStatus, JsonResponse, DataJsonResponse, SuccessJsonResponse, FailJsonResponse, ErrorJsonResponse };
