import { _, locale } from "svelte-i18n";

import { JsonResponseStatus, parseResponse } from "./json";
import { backend } from "$lib/stores/backend";

let backendValue;
let localeValue;
let $_;

locale.subscribe((value) => (localeValue = value));
backend.subscribe((value) => (backendValue = value));
_.subscribe((value) => ($_ = value));

function buildRequestUrl(path, parameters = {}, includeLocale = true) {
  let url = backendValue + path;

  let numParameters = 0;

  if (includeLocale) {
    parameters["locale"] = localeValue;
  }

  for (const [paramName, paramValue] of Object.entries(parameters)) {
    url += numParameters > 0 ? "&" : "?";
    url += paramName;

    if (typeof paramValue !== "undefined") {
      url += `=${paramValue}`;
    }

    numParameters += 1;
  }

  return url;
}

async function fetchJsonGet(path, parameters = {}, includeLocale = true) {
  try {
    return parseResponse(await fetch(buildRequestUrl(path, parameters, includeLocale), {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    }));
  } catch (error) {
    return {
      status: JsonResponseStatus.ERROR,
      message: $_("utils.request.error.fetch"),
    };
  }
}

async function fetchJsonPost(path, requestBody, parameters = {},
    includeLocale = true) {

  try {
    return parseResponse(await fetch(
      buildRequestUrl(path, parameters, includeLocale), {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(requestBody),
      }
    ));
  } catch (error) {
    return {
      status: JsonResponseStatus.ERROR,
      message: $_("utils.request.error.fetch"),
    };
  }
}

// TODO figure out a way to do this cleaner
function createRequestTracker() {
  return {
    inProgress: false,
  };
}

export { buildRequestUrl, createRequestTracker, fetchJsonGet, fetchJsonPost };
