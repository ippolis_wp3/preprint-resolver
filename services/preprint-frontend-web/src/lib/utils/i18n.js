import { register, init, locale } from "svelte-i18n";
import { addLocalStorageSync, getLocalStorageItem } from "$lib/utils/storage";

function initializeLocales() {
  register("en_US", () => import("$src/i18n/en_US.json"));
  register("de_DE", () => import("$src/i18n/de_DE.json"));

  init({
    fallbackLocale: "en_US",
    initialLocale: getLocalStorageItem("store.locale", "de_DE"),
  });

  locale.set(getLocalStorageItem("store.locale", "en_US"));
  addLocalStorageSync(locale, "store.locale", "en_US");
}

export { initializeLocales };
