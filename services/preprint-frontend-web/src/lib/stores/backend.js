import { writable } from "svelte/store";

import {
  PUBLIC_BACKEND,
} from "$env/static/public";

export const backend = writable(PUBLIC_BACKEND);

export const serviceInformation = writable([]);
