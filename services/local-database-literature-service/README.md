# local-database-literature-service

Service which queries local databases for literature information.

## Setup

The ElasticSearch configuration file must be placed at `./src/main/resources/secret/elasticSearchConfig.txt`.
