package de.ippolis.wp3.local_database_literature_service.dblp.controller;

import de.ippolis.wp3.local_database_literature_service.dblp.service.DBLPLocalDataBaseService;
import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.local_database_literature_service.utils.model.Reference;
import de.ippolis.wp3.local_database_literature_service.utils.model.dto.ArxivIdIdBibTexPagePreprocessingRequestDTO;
import de.ippolis.wp3.local_database_literature_service.utils.model.dto.DoiIdBibTexPagePreprocessingRequestDTO;
import de.ippolis.wp3.local_database_literature_service.utils.model.dto.TitleIdBibTexPagePreprocessingRequestDTO;
import de.ippolis.wp3.local_database_literature_service.utils.model.json.ErrorJsonResponse;
import de.ippolis.wp3.local_database_literature_service.utils.model.json.JsonResponse;
import de.ippolis.wp3.local_database_literature_service.utils.model.json.SuccessJsonResponse;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DBLPController {

  private final DBLPLocalDataBaseService dblpLocalDataBaseService;
  Logger logger = LoggerFactory.getLogger(DBLPController.class);

  @Autowired
  public DBLPController(DBLPLocalDataBaseService dblpLocalDataBaseService) {
    this.dblpLocalDataBaseService = dblpLocalDataBaseService;
  }

  @PostMapping(
      value = "/dblp/getLocalReferencesByDOI",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getReferencesCrossCiteByDOI(
      @RequestBody DoiIdBibTexPagePreprocessingRequestDTO requestDTO) {
    try {
      String id = requestDTO.getId();
      String doi = requestDTO.getDoi();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<Reference> results =
          dblpLocalDataBaseService.getByDOI(doi, id, bibTexPagePreprocessing);
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/dblp/getLocalReferencesByTitle",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getReferencesCrossCiteByTitle(
      @RequestBody TitleIdBibTexPagePreprocessingRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      String id = requestDTO.getId();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<Reference> results =
          dblpLocalDataBaseService.getByTitle(title, id, bibTexPagePreprocessing);
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/dblp/getLocalReferencesByArxivId",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getReferencesCrossCiteByArxivId(
      @RequestBody ArxivIdIdBibTexPagePreprocessingRequestDTO requestDTO) {
    try {
      String arxivId = requestDTO.getArxivId();
      String id = requestDTO.getId();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<Reference> results =
          dblpLocalDataBaseService.getByArxivId(arxivId, id, bibTexPagePreprocessing);
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }
}
