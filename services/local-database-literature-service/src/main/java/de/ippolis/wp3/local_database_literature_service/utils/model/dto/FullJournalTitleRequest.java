package de.ippolis.wp3.local_database_literature_service.utils.model.dto;

public class FullJournalTitleRequest {

  private String abbreviation;

  public FullJournalTitleRequest() {
  }

  public FullJournalTitleRequest(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public String getAbbreviation() {
    return abbreviation;
  }

  public void setAbbreviation(String abbreviation) {
    this.abbreviation = abbreviation;
  }
}
