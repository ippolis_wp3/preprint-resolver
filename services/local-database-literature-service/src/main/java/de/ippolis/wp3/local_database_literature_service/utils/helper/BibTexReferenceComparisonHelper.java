package de.ippolis.wp3.local_database_literature_service.utils.helper;

import de.ippolis.wp3.local_database_literature_service.utils.model.Reference;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class BibTexReferenceComparisonHelper {

  private final MessageSource messageSource;

  @Autowired
  public BibTexReferenceComparisonHelper(MessageSource messageSource) {
    this.messageSource = messageSource;
  }

  public String preProcessName(String name) {
    name = name.toLowerCase();
    name = name.replace("\\\"u", "ü");
    name = name.replace("\\\"a", "ä");
    name = name.replace("\\\"o", "ö");
    name = name.replace("\\'i", "í");
    name = name.replace("\\'{i}", "í");
    name = name.replace("\\'{e}", "é");
    name = name.replace("\\'e", "é");
    name = name.replace("{", "");
    name = name.replace("}", "");
    return (name);
  }

  private String preProcessAuthorNamesForComparison(String name) {
    name = name.toLowerCase();
    name = name.strip();
    name = name.replace("\\\"u", "u");
    name = name.replace("\\\"a", "a");
    name = name.replace("\\\"o", "o");
    name = name.replace("\\'i", "i");
    name = name.replace("\\'{i}", "i");
    name = name.replace("í", "i");
    name = name.replace("\\'{e}", "e");
    name = name.replace("\\'e", "e");
    name = name.replace("á", "a");
    name = name.replace("ñ", "n");
    name = name.replace("à", "a");
    name = name.replace("ă", "a");
    name = name.replace("ü", "u");
    name = name.replace("ä", "a");
    name = name.replace("ö", "o");
    name = name.replace("oe", "o");
    name = name.replace("ae", "a");
    name = name.replace("ue", "u");
    name = name.replace("é", "e");
    name = name.replace("è", "e");
    name = name.replace("ó", "o");
    name = name.replace("ø", "o");
    name = name.replace("ž", "z");
    name = name.replace("ø", "o");
    name = name.replace("&apos;", "'");
    name = name.replace("{", "");
    name = name.replace("}", "");
    name = name.replace("â", "a");
    name = name.replace("å", "a");
    name = name.replace(".", "");
    name = name.replace("ç", "c");
    name = name.replace("ń", "n");
    name = name.replace("ê", "e");
    name = name.replace("ı", "i");
    name = name.replace("’", "'");
    name = name.replace("ú", "u");
    name = name.replace("ý", "y");
    name = name.replace("ß", "ss");
    name = name.replace("š", "s");
    name = name.replace("ć", "c");
    name = name.replace("ğ", "g");
    name = name.replace("ş", "s");
    name = name.replace("ã", "a");
    name = name.replace("ś", "s");
    name = name.replace("ń", "n");
    name = name.replace("ő", "o");
    name = name.replace("ű", "u");
    name = name.replace("š", "s");
    name = name.replace("ň", "n");
    name = name.replace("ï", "i");
    name = name.replace("ł", "l");
    name = name.replace("ą", "a");
    name = name.replace("ë", "e");
    name = name.replace("č", "c");
    name = name.replace("ř", "r");
    return name;
  }

  public boolean checkAuthorsMatch(List<String> authors, List<String> authorNames) {
    if (authors.size() <= 0) {
      return false;
    }
    int authorsTotal = Math.max(authors.size(), authorNames.size());
    int authorsFound = 0;
    for (String author : authors) {
      for (String authorName : authorNames) {
        if (preProcessAuthorNamesForComparison(authorName)
            .contains(preProcessAuthorNamesForComparison(author))) {
          authorsFound++;
          break;
        }
      }
    }
    return (((double) authorsFound / (double) authorsTotal) > 0.7);
  }

  private String preprocessTitle(String title) {
    if (title.endsWith(".")) {
      title = title.substring(0, title.length() - 1);
    }
    if (title.endsWith(" ")) {
      title = title.substring(0, title.length() - 1);
    }
    title = title.replace("{", "");
    title = title.replace("}", "");
    title = title.replace("&apos;", "'");
    title = title.replace("&amp;", "&");
    return title;
  }

  public boolean checkTitlesMatch(String title, String comparison) {
    title = preprocessTitle(title);
    comparison = preprocessTitle(comparison);
    int levenshteinDistance =
        StringUtils.getLevenshteinDistance(title.toLowerCase(), comparison.toLowerCase());
    int lettersFound = title.length();
    int letters = comparison.length();
    int maxLetters = Math.max(letters, lettersFound);
    double ratio = (double) levenshteinDistance / (double) maxLetters;
    return (ratio < 0.05);
  }

  private String getValueFromMap(Map<String, String> map, String key) {
    String value = "";
    if (map.containsKey(key)) {
      value = map.get(key);
    }
    return value;
  }

  private String preprocessBraces(String text) {
    text = text.replace("{", "\\{");
    text = text.replace("}", "\\}");
    text = text.replace("_", "\\_");
    text = text.replace("\\\\{", "\\{");
    text = text.replace("\\\\}", "\\}");
    text = text.replace("\\\\_", "\\_");
    return text;
  }

  private String preprocessBibTexField(String text) {
    text = text.toLowerCase();
    // text = text.replace("{", "");
    // text = text.replace("}", "");
    text = text.replace("  ", " ");
    text = text.replace("--", "-");
    text = text.replace("\\_", "_");
    text = text.strip();
    return text;
  }

  public String preprocessDOI(String doi) {
    doi = doi.toLowerCase();
    if (doi.endsWith(".")) {
      doi = doi.substring(0, doi.length() - 1);
    }
    if (doi.contains("doi.org/")) {
      int lastIndex = doi.indexOf("doi.org/");
      doi = doi.substring(lastIndex + 8);
    }
    return doi.strip();
  }

  public List<Reference> filterMatches(
      List<Reference> possibleMatches, List<String> authors, String title) {
    List<Reference> filteredMatches = new LinkedList<>();
    for (Reference possibleMatch : possibleMatches) {
      if (checkTitlesMatch(title, possibleMatch.getTitleCorrect())) {
        ArrayList<String> surnames = possibleMatch.getSurnames();
        if (checkAuthorsMatch(surnames, authors)) {
          filteredMatches.add(possibleMatch);
        }
      }
    }
    return filteredMatches;
  }
}
