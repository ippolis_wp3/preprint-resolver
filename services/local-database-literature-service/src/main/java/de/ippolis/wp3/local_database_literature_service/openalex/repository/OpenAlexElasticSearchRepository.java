package de.ippolis.wp3.local_database_literature_service.openalex.repository;

import de.ippolis.wp3.local_database_literature_service.openalex.model.OpenAlexReferenceDocument;
import java.util.List;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface OpenAlexElasticSearchRepository
    extends ElasticsearchRepository<OpenAlexReferenceDocument, String> {

  List<OpenAlexReferenceDocument> findByDoi(String doi);

  List<OpenAlexReferenceDocument> findByTitle(String title);

  List<OpenAlexReferenceDocument> findByArxiv(String arxivId);
}
