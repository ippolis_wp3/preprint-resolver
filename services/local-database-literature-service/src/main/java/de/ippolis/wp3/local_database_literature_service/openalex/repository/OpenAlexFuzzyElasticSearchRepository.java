package de.ippolis.wp3.local_database_literature_service.openalex.repository;

import de.ippolis.wp3.local_database_literature_service.openalex.model.OpenAlexReferenceDocument;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.data.elasticsearch.core.query.StringQuery;
import org.springframework.stereotype.Repository;

@Repository
public class OpenAlexFuzzyElasticSearchRepository {

  private final ElasticsearchOperations elasticsearchOperations;
  private final ReactiveElasticsearchClient elasticsearchClient;

  @Autowired
  public OpenAlexFuzzyElasticSearchRepository(ElasticsearchOperations elasticsearchOperations,
      ReactiveElasticsearchClient elasticsearchClient) {
    this.elasticsearchOperations = elasticsearchOperations;
    this.elasticsearchClient = elasticsearchClient;
  }

  public List<OpenAlexReferenceDocument> findByTitleFuzzy(String title) {
    Query query = new StringQuery(
        "{ \"match\": { \"title\": { \"query\": \"" + title
            + "\", \"fuzziness\": 0, \"fuzzy_transpositions\":true, \"prefix_length\":10, \"max_expansions\": 5} } } ");

    List<OpenAlexReferenceDocument> results = new LinkedList<>();
    // SearchRequest searchRequest = SearchRequest.Builder.build();
    //Mono<ResponseBody<OpenAlexReferenceDocument>> res = elasticsearchClient.search(searchRequest,OpenAlexReferenceDocument.class);
    SearchHits search = elasticsearchOperations.search(query,
        OpenAlexReferenceDocument.class);
    List<SearchHit> searchHits = search.getSearchHits();

    for (SearchHit<OpenAlexReferenceDocument> hit : searchHits) {
      OpenAlexReferenceDocument yourObject = hit.getContent();
      results.add(yourObject);
    }
    return results;
  }
}
