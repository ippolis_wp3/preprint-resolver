package de.ippolis.wp3.local_database_literature_service.abbreviations.repository;

import de.ippolis.wp3.local_database_literature_service.abbreviations.model.AbbreviationDocument;
import java.util.List;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AbbreviationRepository extends
    ElasticsearchRepository<AbbreviationDocument, String> {

  List<AbbreviationDocument> findByTitle(String title);

  List<AbbreviationDocument> findByMedabbreviation(String medabbreviation);

  List<AbbreviationDocument> findByIsoabbreviation(String isoabbreviation);

}
