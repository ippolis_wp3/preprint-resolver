package de.ippolis.wp3.local_database_literature_service.utils.model;

public enum ReferenceSource {
  SEMANTIC_SCHOLAR,
  CROSS_CITE,
  DBLP,
  OPENALEX,
}
