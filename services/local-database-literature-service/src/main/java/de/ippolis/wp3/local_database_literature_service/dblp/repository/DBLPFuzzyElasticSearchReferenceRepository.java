package de.ippolis.wp3.local_database_literature_service.dblp.repository;

import de.ippolis.wp3.local_database_literature_service.dblp.model.DBLPReferenceDocument;
import java.util.LinkedList;
import java.util.List;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.data.elasticsearch.core.query.StringQuery;
import org.springframework.stereotype.Repository;

@Repository
public class DBLPFuzzyElasticSearchReferenceRepository {

  private final ElasticsearchOperations elasticsearchOperations;

  public DBLPFuzzyElasticSearchReferenceRepository(
      ElasticsearchOperations elasticsearchOperations) {
    this.elasticsearchOperations = elasticsearchOperations;
  }

  public List<DBLPReferenceDocument> findByTitleFuzzy(String title) {
    Query query = new StringQuery(
        "{ \"match\": { \"title\": { \"query\": \"" + title
            + "\", \"fuzziness\": 0, \"fuzzy_transpositions\":true, \"prefix_length\":10, \"max_expansions\": 5} } } ");
    List<DBLPReferenceDocument> results = new LinkedList<>();

    SearchHits search = elasticsearchOperations.search(query,
        DBLPReferenceDocument.class);
    List<SearchHit> searchHits = search.getSearchHits();

    for (SearchHit<DBLPReferenceDocument> hit : searchHits) {
      DBLPReferenceDocument yourObject = hit.getContent();
      results.add(yourObject);
    }
    return results;
  }
}
