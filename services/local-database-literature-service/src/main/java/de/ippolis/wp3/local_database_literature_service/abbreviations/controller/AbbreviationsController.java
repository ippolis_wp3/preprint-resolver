package de.ippolis.wp3.local_database_literature_service.abbreviations.controller;

import de.ippolis.wp3.local_database_literature_service.abbreviations.service.AbbreviationsService;
import de.ippolis.wp3.local_database_literature_service.cross_cite.controller.CrossCiteController;
import de.ippolis.wp3.local_database_literature_service.utils.model.dto.AbbreviationRequest;
import de.ippolis.wp3.local_database_literature_service.utils.model.dto.FullJournalTitleRequest;
import de.ippolis.wp3.local_database_literature_service.utils.model.json.ErrorJsonResponse;
import de.ippolis.wp3.local_database_literature_service.utils.model.json.JsonResponse;
import de.ippolis.wp3.local_database_literature_service.utils.model.json.SuccessJsonResponse;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AbbreviationsController {

  private final AbbreviationsService abbreviationsService;
  Logger logger = LoggerFactory.getLogger(CrossCiteController.class);

  @Autowired
  public AbbreviationsController(
      AbbreviationsService abbreviationsService) {
    this.abbreviationsService = abbreviationsService;
  }

  @PostMapping(
      value = "/abbreviations/getAbbreviation",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getAbbreviation(
      @RequestBody AbbreviationRequest journalName) {
    try {
      Map<String, String> result =
          abbreviationsService.getAbbreviation(
              journalName.getJournalTitle());
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/abbreviations/getJournalTitle",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getJournalTitle(
      @RequestBody FullJournalTitleRequest fullJournalTitleRequest) {
    try {
      Map<String, String> result =
          abbreviationsService.getJournalTitle(
              fullJournalTitleRequest.getAbbreviation());
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }
}
