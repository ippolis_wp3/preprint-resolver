package de.ippolis.wp3.local_database_literature_service.abbreviations.repository;

import de.ippolis.wp3.local_database_literature_service.abbreviations.model.AbbreviationDocument;
import de.ippolis.wp3.local_database_literature_service.cross_cite.model.CrossCiteReferenceDocument;
import java.util.LinkedList;
import java.util.List;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.data.elasticsearch.core.query.StringQuery;
import org.springframework.stereotype.Repository;

@Repository
public class AbbreviationsFuzzyElasticSearchRepository {

  private final ElasticsearchOperations elasticsearchOperations;

  public AbbreviationsFuzzyElasticSearchRepository(
      ElasticsearchOperations elasticsearchOperations) {
    this.elasticsearchOperations = elasticsearchOperations;
  }

  public List<AbbreviationDocument> findByTitleFuzzy(String title) {
    Query query = new StringQuery(
        "{ \"match\": { \"title\": { \"query\": \"" + title
            + "\", \"fuzziness\": 0, \"fuzzy_transpositions\":true, \"prefix_length\":10, \"max_expansions\": 5} } } ");
    List<AbbreviationDocument> results = new LinkedList<>();

    SearchHits search = elasticsearchOperations.search(query,
        CrossCiteReferenceDocument.class);
    List<SearchHit> searchHits = search.getSearchHits();

    for (SearchHit<AbbreviationDocument> hit : searchHits) {
      AbbreviationDocument yourObject = hit.getContent();
      results.add(yourObject);
    }
    return results;
  }

  public List<AbbreviationDocument> findByIsoabbreviationFuzzy(String title) {
    Query query = new StringQuery(
        "{ \"match\": { \"isoabbreviation\": { \"query\": \"" + title
            + "\", \"fuzziness\": 0, \"fuzzy_transpositions\":true, \"prefix_length\":10, \"max_expansions\": 5} } } ");
    List<AbbreviationDocument> results = new LinkedList<>();

    SearchHits search = elasticsearchOperations.search(query,
        CrossCiteReferenceDocument.class);
    List<SearchHit> searchHits = search.getSearchHits();

    for (SearchHit<AbbreviationDocument> hit : searchHits) {
      AbbreviationDocument yourObject = hit.getContent();
      results.add(yourObject);
    }
    return results;
  }
}
