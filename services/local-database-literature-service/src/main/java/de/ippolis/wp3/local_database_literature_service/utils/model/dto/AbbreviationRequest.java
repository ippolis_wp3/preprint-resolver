package de.ippolis.wp3.local_database_literature_service.utils.model.dto;

public class AbbreviationRequest {

  private String journalTitle;

  public AbbreviationRequest() {
  }

  public AbbreviationRequest(String journalTitle) {
    this.journalTitle = journalTitle;
  }

  public String getJournalTitle() {
    return journalTitle;
  }

  public void setJournalTitle(String journalTitle) {
    this.journalTitle = journalTitle;
  }
}
