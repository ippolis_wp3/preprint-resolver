package de.ippolis.wp3.local_database_literature_service.utils.model;

public class StringInsert {

  private int index;
  private String toInsert;

  public StringInsert(int index, String toInsert) {
    this.index = index;
    this.toInsert = toInsert;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public String getToInsert() {
    return toInsert;
  }

  public void setToInsert(String toInsert) {
    this.toInsert = toInsert;
  }
}
