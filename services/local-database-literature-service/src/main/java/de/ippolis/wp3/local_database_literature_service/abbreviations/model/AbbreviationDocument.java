package de.ippolis.wp3.local_database_literature_service.abbreviations.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "abbreviations")
public class AbbreviationDocument {

  @Id
  private String id;
  private String title;
  private String medabbreviation;
  private String isoabbreviation;
  private String issnprint;
  private String issnonline;

  public AbbreviationDocument() {
  }

  public AbbreviationDocument(String id, String title, String medabbreviation,
      String isoabbreviation, String issnprint, String issnonline) {
    this.id = id;
    this.title = title;
    this.medabbreviation = medabbreviation;
    this.isoabbreviation = isoabbreviation;
    this.issnprint = issnprint;
    this.issnonline = issnonline;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getMedabbreviation() {
    return medabbreviation;
  }

  public void setMedabbreviation(String medabbreviation) {
    this.medabbreviation = medabbreviation;
  }

  public String getIsoabbreviation() {
    return isoabbreviation;
  }

  public void setIsoabbreviation(String isoabbreviation) {
    this.isoabbreviation = isoabbreviation;
  }

  public String getIssnprint() {
    return issnprint;
  }

  public void setIssnprint(String issnprint) {
    this.issnprint = issnprint;
  }

  public String getIssnonline() {
    return issnonline;
  }

  public void setIssnonline(String issnonline) {
    this.issnonline = issnonline;
  }
}
