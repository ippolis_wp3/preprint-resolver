package de.ippolis.wp3.local_database_literature_service.abbreviations.service;

import de.ippolis.wp3.local_database_literature_service.abbreviations.model.AbbreviationDocument;
import de.ippolis.wp3.local_database_literature_service.abbreviations.repository.AbbreviationRepository;
import de.ippolis.wp3.local_database_literature_service.abbreviations.repository.AbbreviationsFuzzyElasticSearchRepository;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AbbreviationsService {

  private final AbbreviationRepository abbreviationRepository;
  private final AbbreviationsFuzzyElasticSearchRepository abbreviationsFuzzyElasticSearchRepository;

  @Autowired
  public AbbreviationsService(
      AbbreviationRepository abbreviationRepository,
      AbbreviationsFuzzyElasticSearchRepository abbreviationsFuzzyElasticSearchRepository) {
    this.abbreviationRepository = abbreviationRepository;
    this.abbreviationsFuzzyElasticSearchRepository = abbreviationsFuzzyElasticSearchRepository;
  }

  private String preprocessText(String title) {
    title = title.replaceAll("[^a-zA-Z0-9\s]", "");
    return title;
  }

  public Map<String, String> getAbbreviation(String journalName) {
    Map<String, String> resultsMap = new LinkedHashMap<>();
    journalName = preprocessText(journalName);
    if (journalName.strip().equals("")) {
      return null;
    }
    List<AbbreviationDocument> resultsByTitle =
        abbreviationRepository.findByTitle(journalName);
    int distance = journalName.length();
    for (AbbreviationDocument abbreviationDocument : resultsByTitle) {
      int distanceComputed = StringUtils.getLevenshteinDistance(journalName.toLowerCase(),
          abbreviationDocument.getTitle().toLowerCase());
      if (distanceComputed < distance) {
        distance = distanceComputed;
        if (((float) distance / (float) journalName.length()) < 0.1) {
          resultsMap.put("journalName", abbreviationDocument.getTitle());
          resultsMap.put("isoAbbreviation", abbreviationDocument.getIsoabbreviation());
        }
        if (distance == 0) {
          break;
        }
      }
    }
    if (resultsMap.size() == 0) {
      resultsByTitle =
          abbreviationsFuzzyElasticSearchRepository.findByTitleFuzzy(journalName);
      distance = journalName.length();
      for (AbbreviationDocument abbreviationDocument : resultsByTitle) {
        int distanceComputed = StringUtils.getLevenshteinDistance(journalName.toLowerCase(),
            abbreviationDocument.getTitle().toLowerCase());
        if (distanceComputed < distance) {
          distance = distanceComputed;
          if (((float) distance / (float) journalName.length()) < 0.1) {
            resultsMap.put("journalName", abbreviationDocument.getTitle());
            resultsMap.put("isoAbbreviation", abbreviationDocument.getIsoabbreviation());
          }
          if (distance == 0) {
            break;
          }
        }
      }
    }
    return resultsMap;
  }

  public Map<String, String> getJournalTitle(String abbreviation) {
    Map<String, String> resultsMap = new LinkedHashMap<>();
    abbreviation = preprocessText(abbreviation);
    if (abbreviation.strip().equals("")) {
      return null;
    }
    List<AbbreviationDocument> resultsByTitle =
        abbreviationRepository.findByIsoabbreviation(abbreviation);
    int distance = abbreviation.length();
    for (AbbreviationDocument abbreviationDocument : resultsByTitle) {
      int distanceComputed = StringUtils.getLevenshteinDistance(abbreviation.toLowerCase(),
          preprocessText(abbreviationDocument.getIsoabbreviation()).toLowerCase());
      if (distanceComputed < distance) {
        distance = distanceComputed;
        if (((float) distance / (float) abbreviation.length()) < 0.3) {
          resultsMap.put("journalName", abbreviationDocument.getTitle());
          resultsMap.put("isoAbbreviation", abbreviationDocument.getIsoabbreviation());
        }
        if (distance == 0) {
          break;
        }
      }
    }
    if (resultsMap.size() == 0) {
      resultsByTitle =
          abbreviationsFuzzyElasticSearchRepository.findByIsoabbreviationFuzzy(abbreviation);
      distance = abbreviation.length();
      for (AbbreviationDocument abbreviationDocument : resultsByTitle) {
        int distanceComputed = StringUtils.getLevenshteinDistance(abbreviation.toLowerCase(),
            preprocessText(abbreviationDocument.getIsoabbreviation()).toLowerCase());
        if (distanceComputed < distance) {
          distance = distanceComputed;
          if (((float) distance / (float) abbreviation.length()) < 0.3) {
            resultsMap.put("journalName", abbreviationDocument.getTitle());
            resultsMap.put("isoAbbreviation", abbreviationDocument.getIsoabbreviation());
          }
          if (distance == 0) {
            break;
          }
        }
      }
    }
    return resultsMap;
  }
}
