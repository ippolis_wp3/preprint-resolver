CREATE TABLE SCIMAGOJOURNALRANKING AS SELECT * FROM CSVREAD('.\\data\\scimago\\scimagojr_2020.csv',null,'charset=UTF-8 fieldSeparator=;');
ALTER TABLE SCIMAGOJOURNALRANKING ADD SCIMAGOYEAR int NOT NULL DEFAULT(2020);
CREATE TABLE JOURNALABBREVIATIONS AS SELECT * FROM CSVREAD('.\\data\\journalabbreviations\\journal_abbreviations_acs.csv','FULLNAME,ABBREVIATION','charset=UTF-8 fieldSeparator=,');
