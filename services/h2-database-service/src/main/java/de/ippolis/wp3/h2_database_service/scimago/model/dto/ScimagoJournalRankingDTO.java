package de.ippolis.wp3.h2_database_service.scimago.model.dto;

import de.ippolis.wp3.h2_database_service.scimago.model.ScimagoJournalRankingEntity;

public class ScimagoJournalRankingDTO {

  private final String journalTitle;
  private int rank;
  private String type;
  private double sjr;
  private String sjrBestQuartile;
  private int hIndex;
  private int totalDocs2020;
  private int totalDocs3years;
  private int totalRefs;
  private int totalCites3years;
  private int citableDocs3Years;
  private double citesDocs2years;
  private double refsPerDoc;
  private String categories;
  private String scimagoYear;
  private String areas;

  public ScimagoJournalRankingDTO(ScimagoJournalRankingEntity entity) {
    this.rank = Integer.parseInt(entity.getRank());
    this.journalTitle = entity.getTitle();
    this.type = entity.getType();
    if (entity.getSjr() != null) {
      this.sjr = Double.parseDouble(entity.getSjr().replace(',', '.'));
    }
    this.sjrBestQuartile = entity.getSjrBestQuartile();
    this.hIndex = Integer.parseInt(entity.gethIndex());
    this.totalDocs2020 = Integer.parseInt(entity.getTotalDocs2020());
    this.totalDocs3years = Integer.parseInt(entity.getTotalDocs3years());
    this.totalRefs = Integer.parseInt(entity.getTotalRefs());
    this.totalCites3years = Integer.parseInt(entity.getTotalCites3years());
    this.citableDocs3Years = Integer.parseInt(entity.getCitableDocs3Years());
    if (entity.getCitesDocs2years() != null) {
      this.citesDocs2years = Double.parseDouble(entity.getCitesDocs2years().replace(',', '.'));
    }
    if (entity.getRefsPerDoc() != null) {
      this.refsPerDoc = Double.parseDouble(entity.getRefsPerDoc().replace(',', '.'));
    }
    this.categories = entity.getCategories();
    this.scimagoYear = entity.getScimagoYear();
    this.areas = entity.getAreas();
  }

  public ScimagoJournalRankingDTO(String title) {
    this.journalTitle = title;
  }

  public String getAreas() {
    return areas;
  }

  public void setAreas(String areas) {
    this.areas = areas;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public String getJournalTitle() {
    return journalTitle;
  }

  public String getType() {
    return type;
  }

  public double getSjr() {
    return sjr;
  }

  public String getSjrBestQuartile() {
    return sjrBestQuartile;
  }

  public int gethIndex() {
    return hIndex;
  }

  public int getTotalDocs2020() {
    return totalDocs2020;
  }

  public int getTotalDocs3years() {
    return totalDocs3years;
  }

  public int getTotalRefs() {
    return totalRefs;
  }

  public int getTotalCites3years() {
    return totalCites3years;
  }

  public int getCitableDocs3Years() {
    return citableDocs3Years;
  }

  public double getCitesDocs2years() {
    return citesDocs2years;
  }

  public double getRefsPerDoc() {
    return refsPerDoc;
  }

  public String getCategories() {
    return categories;
  }

  public String getScimagoYear() {
    return scimagoYear;
  }
}
