package de.ippolis.wp3.h2_database_service.journal_abreviations.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity(name = "JOURNALABBREVIATIONS")
public class JournalAbbreviationEntity {

  @Id
  @Column(name = "FULLNAME")
  private String fullname;
  @Column(name = "ABBREVIATION")
  private String abbreviation;

  public JournalAbbreviationEntity() {
  }

  public JournalAbbreviationEntity(String fullname, String abbreviation) {
    this.fullname = fullname;
    this.abbreviation = abbreviation;
  }

  public String getFullname() {
    return fullname;
  }

  public void setFullname(String fullname) {
    this.fullname = fullname;
  }

  public String getAbbreviation() {
    return abbreviation;
  }

  public void setAbbreviation(String abbreviation) {
    this.abbreviation = abbreviation;
  }
}
