package de.ippolis.wp3.h2_database_service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

/**
 * This is the application entry point
 */
@SpringBootApplication
public class H2DatabaseApplication {

  @Autowired
  private DataSource dataSource;


  public static void main(String[] args) {
    SpringApplication.run(H2DatabaseApplication.class, args);
  }

  /**
   * TODO documentation
   * <p>
   * TODO can we instead check if the database already exists? Create volume in docker file
   */
  @EventListener(ApplicationReadyEvent.class)
  public void fillScimagoDatabase() throws IOException, SQLException {

    try (Connection conn = dataSource.getConnection()) {
      Statement statementCreate = conn.createStatement();
      statementCreate.execute("DELETE FROM SCIMAGOJOURNALRANKING");
      Path pathScimago = Paths.get(".", "data", "scimago");
      File dir = new File(pathScimago.toString());
      Statement statementDelete = conn.createStatement();
      statementDelete.execute("DELETE FROM SCIMAGOJOURNALRANKING");
      File[] directoryListing = dir.listFiles();
      if (directoryListing != null) {
        for (File child : directoryListing) {
          String path = child.getAbsolutePath();
          String pathRel = child.getCanonicalPath();
          String year = path.substring(path.length() - 8, path.length() - 4);
          Statement statement = conn.createStatement();
          statement.execute(
              "INSERT INTO SCIMAGOJOURNALRANKING SELECT *," + year + " FROM CSVREAD('" + pathRel
                  + "',null,'charset=UTF-8 fieldSeparator=;');");
        }
      }
    }
  }

  /**
   * TODO documentation
   * <p>
   * TODO can we instead check if the database already exists? Create volume in docker file
   */
  @EventListener(ApplicationReadyEvent.class)
  public void fillJournalAbbreviationDatabase() throws IOException {
    String tempPath = "";
    try (Connection conn = dataSource.getConnection()) {
      Statement statementCreate = conn.createStatement();
      statementCreate.execute("DELETE FROM JOURNALABBREVIATIONS");
      Path pathJournalAbbreviations = Paths.get(".", "data", "journalabbreviations");
      File dir = new File(pathJournalAbbreviations.toString());
      Statement statementDelete = conn.createStatement();
      statementDelete.execute("DELETE FROM JOURNALABBREVIATIONS");
      File[] directoryListing = dir.listFiles();
      if (directoryListing != null) {
        for (File child : directoryListing) {
          String myCsv = csvFormatting(child);
          Path tempFile = Files.createTempFile("json", ".csv");
          tempPath = tempFile.toString();
          Files.writeString(tempFile, myCsv);
          Statement statement = conn.createStatement();
          statement.execute(
              "INSERT INTO JOURNALABBREVIATIONS SELECT * FROM CSVREAD('" + tempPath
                  + "','FULLNAME,ABBREVIATION','charset=UTF-8 fieldSeparator=,');");
          Files.deleteIfExists(tempFile);
        }
      }
    } catch (SQLException e) {
      Files.deleteIfExists(Path.of(tempPath));
      e.printStackTrace();
    } catch (FileNotFoundException e) {
      Files.deleteIfExists(Path.of(tempPath));
      e.printStackTrace();
    } catch (IOException e) {
      Files.deleteIfExists(Path.of(tempPath));
      e.printStackTrace();
    }
    fillJournalAbrreviationDatabaseJson();
  }

  private String jsonToCSVFormatting(String json) {
    String csv = json.replace("{", "");
    csv = csv.replace("}", "");
    csv = csv.replace("\"default\":", "");
    csv = csv.replace("\"container-title\":", "");
    csv = csv.replace("\": \"", "\";\"");
    csv = csv.replace("\",\n", "\"$$$");
    csv = csv.replace("\n", "");
    csv = csv.replace("\t", "");
    csv = csv.replace("\"$$$", "\"\n");
    return csv;
  }

  private String csvFormatting(File child) throws FileNotFoundException {
    String myCsv = new Scanner(
        child).useDelimiter("\\Z")
        .next();
    myCsv = myCsv.replace(".", "");
    myCsv = myCsv.replace(" & ", " and ");
    myCsv = myCsv.replace("`", "'");
    return myCsv;
  }


  private void fillJournalAbrreviationDatabaseJson() throws IOException {
    String tempPath = "";
    try (Connection conn = dataSource.getConnection()) {
      Path pathJournalAbbreviationJson = Paths.get(".", "data", "journalabbreviations_json");
      File dir = new File(pathJournalAbbreviationJson.toString());
      File[] directoryListing = dir.listFiles();
      if (directoryListing != null) {
        for (File child : directoryListing) {
          String myJson = new Scanner(
              child).useDelimiter("\\Z")
              .next();
          myJson = jsonToCSVFormatting(myJson);
          Path tempFile = Files.createTempFile("json", ".csv");
          tempPath = tempFile.toString();
          Files.writeString(tempFile, myJson);
          Statement statement = conn.createStatement();
          statement.execute(
              "INSERT INTO JOURNALABBREVIATIONS SELECT * FROM CSVREAD('" + tempFile
                  + "','FULLNAME;ABBREVIATION','charset=UTF-8 fieldSeparator=;');");

          Files.deleteIfExists(tempFile);
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
      Files.deleteIfExists(Path.of(tempPath));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
