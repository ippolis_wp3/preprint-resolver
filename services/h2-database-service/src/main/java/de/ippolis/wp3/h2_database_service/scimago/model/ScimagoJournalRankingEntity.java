package de.ippolis.wp3.h2_database_service.scimago.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity(name = "SCIMAGOJOURNALRANKING")
public class ScimagoJournalRankingEntity {

  @Column(name = "RANK")
  private String rank;
  @Column(name = "SOURCEID")
  private String sourceid;
  //@Field(termVector = TermVector.YES)
  @Id
  @Column(name = "TITLE")
  private String title;
  @Column(name = "TYPE")
  private String type;
  @Column(name = "ISSN")
  private String issn;
  @Column(name = "SJR")
  private String sjr;
  @Column(name = "\"SJR Best Quartile\"")
  private String sjrBestQuartile;
  @Column(name = "\"H index\"")
  private String hIndex;
  @Column(name = "\"Total Docs. (2020)\"")
  private String totalDocs2020;
  @Column(name = "\"Total Docs. (3years)\"")
  private String totalDocs3years;
  @Column(name = "\"Total Refs.\"")
  private String totalRefs;
  @Column(name = "\"Total Cites (3years)\"")
  private String totalCites3years;
  @Column(name = "\"Citable Docs. (3years)\"")
  private String citableDocs3Years;
  @Column(name = "\"Cites / Doc. (2years)\"")
  private String citesDocs2years;
  @Column(name = "\"Ref. / Doc.\"")
  private String refsPerDoc;

  @Column(name = "COUNTRY")
  private String country;
  @Column(name = "REGION")
  private String region;
  @Column(name = "PUBLISHER")
  private String publisher;
  @Column(name = "COVERAGE")
  private String coverage;
  @Column(name = "CATEGORIES")
  private String categories;

  @Column(name = "SCIMAGOYEAR")
  private String scimagoYear;
  @Column(name = "AREAS")
  private String areas;

  public ScimagoJournalRankingEntity() {

  }

  public ScimagoJournalRankingEntity(String rank, String sourceid, String title, String type,
      String issn, String sjr, String sjrBestQuartile, String hIndex, String totalDocs2020,
      String totalDocs3years, String totalRefs, String totalCites3years, String citableDocs3Years,
      String citesDocs2years, String refsPerDoc, String country, String region, String publisher,
      String coverage, String categories, String scimagoYear, String areas) {
    this.rank = rank;
    this.sourceid = sourceid;
    this.title = title;
    this.type = type;
    this.issn = issn;
    this.sjr = sjr;
    this.sjrBestQuartile = sjrBestQuartile;
    this.hIndex = hIndex;
    this.totalDocs2020 = totalDocs2020;
    this.totalDocs3years = totalDocs3years;
    this.totalRefs = totalRefs;
    this.totalCites3years = totalCites3years;
    this.citableDocs3Years = citableDocs3Years;
    this.citesDocs2years = citesDocs2years;
    this.refsPerDoc = refsPerDoc;
    this.country = country;
    this.region = region;
    this.publisher = publisher;
    this.coverage = coverage;
    this.categories = categories;
    this.scimagoYear = scimagoYear;
    this.areas = areas;
  }

  public String getAreas() {
    return areas;
  }

  public void setAreas(String areas) {
    this.areas = areas;
  }

  public String getCategories() {
    return categories;
  }

  public void setCategories(String categories) {
    this.categories = categories;
  }

  public String getScimagoYear() {
    return scimagoYear;
  }

  public void setScimagoYear(String scimagoYear) {
    this.scimagoYear = scimagoYear;
  }

  public String getRank() {
    return rank;
  }

  public void setRank(String rank) {
    this.rank = rank;
  }

  public String getSourceid() {
    return sourceid;
  }

  public void setSourceid(String sourceid) {
    this.sourceid = sourceid;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getIssn() {
    return issn;
  }

  public void setIssn(String issn) {
    this.issn = issn;
  }

  public String getSjr() {
    return sjr;
  }

  public void setSjr(String sjr) {
    this.sjr = sjr;
  }

  public String getSjrBestQuartile() {
    return sjrBestQuartile;
  }

  public void setSjrBestQuartile(String sjrBestQuartile) {
    this.sjrBestQuartile = sjrBestQuartile;
  }

  public String gethIndex() {
    return hIndex;
  }

  public void sethIndex(String hIndex) {
    this.hIndex = hIndex;
  }

  public String getTotalDocs2020() {
    return totalDocs2020;
  }

  public void setTotalDocs2020(String totalDocs2020) {
    this.totalDocs2020 = totalDocs2020;
  }

  public String getTotalDocs3years() {
    return totalDocs3years;
  }

  public void setTotalDocs3years(String totalDocs3years) {
    this.totalDocs3years = totalDocs3years;
  }

  public String getTotalRefs() {
    return totalRefs;
  }

  public void setTotalRefs(String totalRefs) {
    this.totalRefs = totalRefs;
  }

  public String getTotalCites3years() {
    return totalCites3years;
  }

  public void setTotalCites3years(String totalCites3years) {
    this.totalCites3years = totalCites3years;
  }

  public String getCitableDocs3Years() {
    return citableDocs3Years;
  }

  public void setCitableDocs3Years(String citableDocs3Years) {
    this.citableDocs3Years = citableDocs3Years;
  }

  public String getCitesDocs2years() {
    return citesDocs2years;
  }

  public void setCitesDocs2years(String citesDocs2years) {
    this.citesDocs2years = citesDocs2years;
  }

  public String getRefsPerDoc() {
    return refsPerDoc;
  }

  public void setRefsPerDoc(String refsPerDoc) {
    this.refsPerDoc = refsPerDoc;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getPublisher() {
    return publisher;
  }

  public void setPublisher(String publisher) {
    this.publisher = publisher;
  }

  public String getCoverage() {
    return coverage;
  }

  public void setCoverage(String coverage) {
    this.coverage = coverage;
  }
}
