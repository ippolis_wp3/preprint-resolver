package de.ippolis.wp3.h2_database_service.journal_abreviations.model.repository;

import de.ippolis.wp3.h2_database_service.journal_abreviations.model.JournalAbbreviationEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JournalAbbreviationRepository extends
    JpaRepository<JournalAbbreviationEntity, Long> {

  public List<JournalAbbreviationEntity> findByFullnameOrAbbreviationIgnoreCase(String fullname,
      String abbreviation);

}
