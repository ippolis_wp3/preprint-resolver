package de.ippolis.wp3.h2_database_service.scimago.model.repository;

import de.ippolis.wp3.h2_database_service.scimago.model.ScimagoJournalRankingEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScimagoJournalRankingRepository extends
    JpaRepository<ScimagoJournalRankingEntity, Long> {

  public List<ScimagoJournalRankingEntity> findByScimagoYearAndTitleIgnoreCase(String year,
      String title);
}
