package de.ippolis.wp3.h2_database_service.journal_abreviations.controller;

import de.ippolis.wp3.h2_database_service.journal_abreviations.model.dto.RequestAbbreviationDTO;
import de.ippolis.wp3.h2_database_service.journal_abreviations.model.dto.RequestFullNameDTO;
import de.ippolis.wp3.h2_database_service.journal_abreviations.service.JournalAbbreviationService;
import de.ippolis.wp3.h2_database_service.json.JsonResponse;
import de.ippolis.wp3.h2_database_service.json.SuccessJsonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Example REST controller.
 */
@RestController
public class JournalAbbreviationController {

  @Autowired
  private final JournalAbbreviationService journalAbbreviationService;
  Logger logger = LoggerFactory.getLogger(JournalAbbreviationController.class);

  @Autowired
  public JournalAbbreviationController(JournalAbbreviationService journalAbbreviationService) {
    this.journalAbbreviationService = journalAbbreviationService;
  }

  @PostMapping(value = "/requestAbbreviation", consumes = "application/json", produces = "application/json")
  public JsonResponse requestAbbreviation(@RequestBody RequestAbbreviationDTO abbreviationDTO) {
    final SuccessJsonResponse response = new SuccessJsonResponse(
        this.journalAbbreviationService.getAbbreviation(abbreviationDTO.getFullname()));

    logger.debug("Response status and data: {}; {}", response.getStatus(), response.getData());

    return response;
  }


  @PostMapping(value = "/requestFullName", consumes = "application/json", produces = "application/json")
  public JsonResponse requestFullname(@RequestBody RequestFullNameDTO fullNameDTO) {
    final SuccessJsonResponse response = new SuccessJsonResponse(
        this.journalAbbreviationService.getFullname(fullNameDTO.getAbbreviation()));

    logger.debug("Response status and data: {}; {}", response.getStatus(), response.getData());

    return response;
  }
}
