package de.ippolis.wp3.h2_database_service.journal_abreviations.model.dto;

public class RequestFullNameDTO {

  private String abbreviation;

  public RequestFullNameDTO() {
  }

  public RequestFullNameDTO(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public String getAbbreviation() {
    return abbreviation;
  }

  public void setAbbreviation(String abbreviation) {
    this.abbreviation = abbreviation;
  }
}
