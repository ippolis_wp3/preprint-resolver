package de.ippolis.wp3.h2_database_service.scimago.service;

import de.ippolis.wp3.h2_database_service.scimago.model.dto.ScimagoJournalRankingDTO;
import de.ippolis.wp3.h2_database_service.scimago.model.ScimagoJournalRankingEntity;
import de.ippolis.wp3.h2_database_service.scimago.model.repository.ScimagoJournalRankingRepository;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScimagoJournalRankingService {

  @Autowired
  private ScimagoJournalRankingRepository scimagoJournalRankingRepository;


  public ScimagoJournalRankingDTO getJournalRanking(String journal, int year)
      throws InterruptedException {
    if (year == 0) {
      year = extractMaxYear();
    }
    journal = journal.replace("  ", " ");
    journal = journal.strip();
    List<ScimagoJournalRankingEntity> resultsList = scimagoJournalRankingRepository.findByScimagoYearAndTitleIgnoreCase(
        String.valueOf(year), journal);
    ScimagoJournalRankingDTO scimagoJournalRankingDTO;
    if (resultsList.size() > 0) {
      scimagoJournalRankingDTO = new ScimagoJournalRankingDTO(resultsList.get(0));
    } else {
      int maxYear = extractMaxYear();
      int minYear = extractMinYear();
      List<ScimagoJournalRankingEntity> resultsListNew = new ArrayList<>();
      if (year > maxYear) {
        resultsListNew = scimagoJournalRankingRepository.findByScimagoYearAndTitleIgnoreCase(
            String.valueOf(maxYear), journal);
      } else {
        if (year < minYear) {
          resultsListNew = scimagoJournalRankingRepository.findByScimagoYearAndTitleIgnoreCase(
              String.valueOf(minYear), journal);
        }
      }
      if (resultsListNew.size() > 0) {
        scimagoJournalRankingDTO = new ScimagoJournalRankingDTO(resultsListNew.get(0));
      } else {
        return null;
      }

    }

    return (scimagoJournalRankingDTO);
  }

  private int extractMaxYear() {
    Path pathScimago = Paths.get(".", "data", "scimago");
    File dir = new File(pathScimago.toString());
    File[] directoryListing = dir.listFiles();
    int yearMax = 0;
    if (directoryListing != null) {
      for (File child : directoryListing) {
        String path = child.getAbsolutePath();
        String year = path.substring(path.length() - 8, path.length() - 4);
        int yearValue;
        yearValue = Integer.parseInt(year);
        if (yearMax < yearValue) {
          yearMax = yearValue;
        }
      }
    }
    return yearMax;
  }

  private int extractMinYear() {
    int i = 0;
    Path pathScimago = Paths.get(".", "data", "scimago");
    File dir = new File(pathScimago.toString());
    File[] directoryListing = dir.listFiles();
    int yearMin = 0;
    if (directoryListing != null) {
      for (File child : directoryListing) {
        String path = child.getAbsolutePath();
        String year = path.substring(path.length() - 8, path.length() - 4);
        int yearValue = Integer.parseInt(year);
        if (i == 0) {
          yearMin = yearValue;
        } else {
          if (yearMin > yearValue) {
            yearMin = yearValue;
          }
        }
        i++;
      }
    }
    return yearMin;
  }
}
