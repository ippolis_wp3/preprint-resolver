package de.ippolis.wp3.h2_database_service.json;

/**
 * JSON response representing a successful result.
 * <p>
 * Implementation of the JSend specification. See https://github.com/omniti-labs/jsend for details.
 */
public class SuccessJsonResponse extends DataJsonResponse {

  public SuccessJsonResponse() {
    this(JsonResponseStatus.SUCCESS);
  }

  public SuccessJsonResponse(Object data) {
    super(JsonResponseStatus.SUCCESS, data);
  }
}
