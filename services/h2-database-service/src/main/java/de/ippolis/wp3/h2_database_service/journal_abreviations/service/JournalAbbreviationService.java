package de.ippolis.wp3.h2_database_service.journal_abreviations.service;

import de.ippolis.wp3.h2_database_service.journal_abreviations.model.JournalAbbreviationEntity;
import de.ippolis.wp3.h2_database_service.journal_abreviations.model.repository.JournalAbbreviationRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JournalAbbreviationService {

  @Autowired
  private JournalAbbreviationRepository repository;

  private String preprocessText(String text) {
    text = text.replace("  ", " ");
    text = text.replace("{", "");
    text = text.replace("}", "");
    text = text.replace(".", "");
    text = text.replace(" & ", " and ");
    if (text.endsWith(" ")) {
      text = text.substring(0, text.length() - 1);
    }
    return (text);
  }

  public List<String> getAbbreviation(String fullname) {
    List<String> returnList = new ArrayList<>();
    fullname = preprocessText(fullname);
    List<JournalAbbreviationEntity> resultsList = repository.findByFullnameOrAbbreviationIgnoreCase(
        fullname, fullname);
    if (resultsList.size() <= 0) {
      return returnList;
    } else {
      for (JournalAbbreviationEntity entity : resultsList) {
        if (!resultsList.contains(entity.getAbbreviation())) {
          returnList.add(entity.getAbbreviation());
        }
      }
    }
    return returnList;
  }

  public List<String> getFullname(String abbreviation) {
    ArrayList<String> returnList = new ArrayList<>();
    abbreviation = preprocessText(abbreviation);
    List<JournalAbbreviationEntity> resultsList = repository.findByFullnameOrAbbreviationIgnoreCase(
        abbreviation, abbreviation);
    if (resultsList.size() <= 0) {
      return returnList;
    } else {
      for (JournalAbbreviationEntity entity : resultsList) {
        if (!returnList.contains(entity.getFullname())) {
          returnList.add(entity.getFullname());
        }
      }
    }
    return returnList;
  }


}
