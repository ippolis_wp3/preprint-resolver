package de.ippolis.wp3.h2_database_service.scimago.model.dto;

public class RequestDTO {

  String journalName;
  int year;

  public RequestDTO() {

  }

  public RequestDTO(String journalName, int year) {
    this.journalName = journalName;
    this.year = year;
  }

  public String getJournalName() {
    return journalName;
  }

  public void setJournalName(String journalName) {
    this.journalName = journalName;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }
}
