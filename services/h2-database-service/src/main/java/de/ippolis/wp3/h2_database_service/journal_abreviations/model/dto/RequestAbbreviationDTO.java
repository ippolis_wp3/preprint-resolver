package de.ippolis.wp3.h2_database_service.journal_abreviations.model.dto;

public class RequestAbbreviationDTO {

  private String fullname;

  public RequestAbbreviationDTO() {
  }

  public RequestAbbreviationDTO(String fullname) {
    this.fullname = fullname;
  }

  public String getFullname() {
    return fullname;
  }

  public void setFullname(String fullname) {
    this.fullname = fullname;
  }
}
