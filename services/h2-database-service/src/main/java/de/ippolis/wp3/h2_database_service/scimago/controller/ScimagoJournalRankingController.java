package de.ippolis.wp3.h2_database_service.scimago.controller;

import de.ippolis.wp3.h2_database_service.json.JsonResponse;
import de.ippolis.wp3.h2_database_service.json.SuccessJsonResponse;
import de.ippolis.wp3.h2_database_service.scimago.model.dto.RequestDTO;
import de.ippolis.wp3.h2_database_service.scimago.service.ScimagoJournalRankingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Example REST controller.
 */
@RestController
public class ScimagoJournalRankingController {

  @Autowired
  private final ScimagoJournalRankingService scimagoJournalRankingService;
  Logger logger = LoggerFactory.getLogger(ScimagoJournalRankingController.class);

  @Autowired
  public ScimagoJournalRankingController(
      ScimagoJournalRankingService scimagoJournalRankingService) {
    this.scimagoJournalRankingService = scimagoJournalRankingService;
  }

  /**
   * Once running, this service will print the current time at lb://microservice-template/time (or
   * http://localhost:8080/microservice-template/time)
   * <p>
   * The Locale is automatically extracted from the URL parameter locale.
   **/
  @PostMapping(value = "/getSJR", consumes = "application/json", produces = "application/json")
  public JsonResponse getScimagoRanking(@RequestBody RequestDTO request)
      throws InterruptedException {
    String journal = request.getJournalName();
    int year = request.getYear();
    final SuccessJsonResponse response = new SuccessJsonResponse(
        this.scimagoJournalRankingService.getJournalRanking(journal, year));

    logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());

    return response;
  }
}
