#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "Executing from directory $DIR"

BASE_DIR=$(realpath "$DIR/..")

echo "Base directory $BASE_DIR"

DATA_DIR=$BASE_DIR/data

echo "Data directory $DATA_DIR"

#mkdir -p $DATA_DIR

# Delete folders in the data directory that are older than 30 days to trigger updates
find "$DATA_DIR" -mindepth 1 -type d -mtime +30 -print0 | while IFS= read -r -d '' FOLDER; do
    rm -rf "$FOLDER"
done

while IFS=',' read -r URL LOCATION
do
    DESTINATION_FILE="$DATA_DIR/$LOCATION"
    DESTINATION_DIR=$(dirname "$DESTINATION_FILE")

    mkdir -p $DESTINATION_DIR

    if [ ! -f "$DESTINATION_FILE" ]; then
        echo "Downloading $URL to $DESTINATION_FILE"
        wget -q -O $DESTINATION_FILE $URL
    else
        echo "Skipping $DESTINATION_FILE because it exists"
    fi
done < $BASE_DIR/src/main/resources/external_resources.csv
