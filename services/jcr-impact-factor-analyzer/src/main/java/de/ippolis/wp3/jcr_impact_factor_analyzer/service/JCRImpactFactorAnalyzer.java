package de.ippolis.wp3.jcr_impact_factor_analyzer.service;

import com.github.rcaller.rstuff.RCaller;
import com.github.rcaller.rstuff.RCallerOptions;
import com.github.rcaller.rstuff.RCode;
import de.ippolis.wp3.jcr_impact_factor_analyzer.dto.JCRImpactFactorDTO;
import de.ippolis.wp3.jcr_impact_factor_analyzer.dto.JCRImpactFactorEntity;
import de.ippolis.wp3.jcr_impact_factor_analyzer.repository.JCRImpactFactorRepository;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.text.similarity.JaroWinklerDistance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JCRImpactFactorAnalyzer {

  private final DataSource dataSource;
  private final JCRImpactFactorRepository jcrImpactFactorRepository;

  @Autowired
  public JCRImpactFactorAnalyzer(DataSource dataSource,
      JCRImpactFactorRepository jcrImpactFactorRepository) {
    this.dataSource = dataSource;
    this.jcrImpactFactorRepository = jcrImpactFactorRepository;
    impactFactorToCSV();
  }

  private String preprocessJournalTitle(String text) {
    String titleContent = text.replace("\\&", "&");
    titleContent = titleContent.replace("'", "");
    titleContent = titleContent.replace("{", "");
    titleContent = titleContent.replace("}", "");
    titleContent = titleContent.toLowerCase();
    return titleContent;
  }

  public boolean checkJournalTitleMatch(String title, String comparison) {
    title = preprocessJournalTitle(title);
    comparison = preprocessJournalTitle(comparison);
    int levenshteinDistance =
        StringUtils.getLevenshteinDistance(title.toLowerCase(), comparison.toLowerCase());
    int lettersFound = title.length();
    int letters = comparison.length();
    int maxLetters = Math.max(letters, lettersFound);
    double ratio = (double) levenshteinDistance / (double) maxLetters;
    return (ratio < 0.05);
  }


  private void impactFactorToCSV() {
    String tmpfileName = "";
    try {
      String tmpdir = Files.createTempDirectory("jcrImpactFactor").toFile().getAbsolutePath();
      Path filePath = Paths.get(tmpdir, "testfile.csv");
      tmpfileName = filePath.toString();
      tmpfileName = tmpfileName.replace("\\", "\\\\");
      RCode code = RCode.create();
      code.addString("input", "");
      code.addString("tmpfileName", tmpfileName);

      code.addRCode("if(!\"JCRImpactFactor\" %in% rownames(installed.packages())){");
      code.addRCode(
          "install.packages(\"JCRImpactFactor\",repos = \"http://cran.us.r-project.org\")}");
      code.addRCode(
          "resultTest <- JCRImpactFactor::find.IF.JCR(input,exact.match = FALSE)");
      code.addRCode(
          "write.table(resultTest,tmpfileName,col.names=FALSE,sep=\",\")");
      RCaller caller = RCaller.create(code, RCallerOptions.create());
      caller.runOnly();
      tmpfileName = tmpfileName.replace("\\\\", "\\");
      try (Connection conn = dataSource.getConnection()) {
        Statement statement = conn.createStatement();
        statement.execute(
            "INSERT INTO JCRTABLE SELECT * FROM CSVREAD('" + tmpfileName
                + "','id,IEJOURNALTITLE,IF2010,IF2011,IF2012,IF2013,IF2014,IF2015,IF2016,IF2017,IF2018,IF2019','charset=UTF-8 fieldSeparator=,');");
        tmpfileName = tmpfileName.replace("\\\\", "\\");
        Files.deleteIfExists(Path.of(tmpfileName));
      } catch (SQLException e) {
        e.printStackTrace();
        tmpfileName = tmpfileName.replace("\\\\", "\\");
        Files.deleteIfExists(Path.of(tmpfileName));
      }
    } catch (IOException e) {
      e.printStackTrace();
      tmpfileName = tmpfileName.replace("\\\\", "\\");
      if (!tmpfileName.equals("")) {
        try {
          Files.deleteIfExists(Path.of(tmpfileName));
        } catch (IOException ex) {
          ex.printStackTrace();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      tmpfileName = tmpfileName.replace("\\\\", "\\");
      try {
        Files.deleteIfExists(Path.of(tmpfileName));
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }

  }

  private double toDouble(String text) {
    try {
      double number = Double.parseDouble(text);
      return number;
    } catch (NumberFormatException e) {
      return -1;
    }
  }

  private double extractIFByYear(JCRImpactFactorEntity entity, int year) {
    if (year <= 2010) {
      if (entity.getIF2010() != null || entity.getIF2010().equals("")) {
        if (isDouble(entity.getIF2010())) {
          return toDouble(entity.getIF2010());
        }
      }
    } else {
      if (year == 2011) {
        if (entity.getIF2011() != null || entity.getIF2011().equals("")) {
          if (isDouble(entity.getIF2011())) {
            return toDouble(entity.getIF2011());
          }
        }
      } else {
        if (year == 2012) {
          if (entity.getIF2012() != null || entity.getIF2012().equals("")) {
            if (isDouble(entity.getIF2012())) {
              return toDouble(entity.getIF2012());
            }
          }
        } else {
          if (year == 2013) {
            if (entity.getIF2013() != null || entity.getIF2013().equals("")) {
              if (isDouble(entity.getIF2013())) {
                return toDouble(entity.getIF2013());
              }
            }
          } else {
            if (year == 2014) {
              if (entity.getIF2014() != null || entity.getIF2014().equals("")) {
                if (isDouble(entity.getIF2014())) {
                  return toDouble(entity.getIF2014());
                }
              }
            } else {
              if (year == 2015) {
                if (entity.getIF2015() != null || entity.getIF2015().equals("")) {
                  if (isDouble(entity.getIF2015())) {
                    return toDouble(entity.getIF2015());
                  }
                }
              } else {
                if (year == 2016) {
                  if (entity.getIF2016() != null || entity.getIF2016().equals("")) {
                    if (isDouble(entity.getIF2016())) {
                      return toDouble(entity.getIF2016());
                    }
                  }
                } else {
                  if (year == 2017) {
                    if (entity.getIF2017() != null || entity.getIF2017().equals("")) {
                      if (isDouble(entity.getIF2017())) {
                        return toDouble(entity.getIF2017());
                      }
                    }
                  } else {
                    if (year == 2018) {
                      if (entity.getIF2018() != null || entity.getIF2018().equals("")) {
                        if (isDouble(entity.getIF2018())) {
                          return toDouble(entity.getIF2018());
                        }
                      }
                    } else {
                      if (entity.getIF2019() != null || entity.getIF2019().equals("")) {
                        if (isDouble(entity.getIF2019())) {
                          return toDouble(entity.getIF2019());

                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return -1;
  }

  private JCRImpactFactorDTO computeImpactFactor(String journal, int year) {
    List<JCRImpactFactorEntity> resultsList = jcrImpactFactorRepository.findByJournalTitleIgnoreCase(
        journal);
    JCRImpactFactorDTO jcrImpactFactorDTO = new JCRImpactFactorDTO();
    for (JCRImpactFactorEntity entity : resultsList) {
      double impactFactor = extractIFByYear(entity, year);
      if (impactFactor != -1) {
        jcrImpactFactorDTO.setImpactFactor(impactFactor);
        if (year <= 2010) {
          jcrImpactFactorDTO.setYear(2010);
        } else {
          if (year >= 2019) {
            jcrImpactFactorDTO.setYear(2019);
          } else {
            jcrImpactFactorDTO.setYear(year);
          }
        }
        jcrImpactFactorDTO.setJournal(entity.getJournalTitle());
        return jcrImpactFactorDTO;
      } else {
        int diff = 10;
        double value = -1;
        int yearNew = year;
        for (int i = 2010; i <= 2019; i++) {
          if (diff > Math.abs(year - i)) {
            double impactFactorNew = extractIFByYear(entity, i);
            if (impactFactorNew != -1) {
              value = impactFactorNew;
              yearNew = i;
              diff = Math.abs(year - i);
            }

          }
        }
        if (value != -1) {
          jcrImpactFactorDTO.setImpactFactor(value);
          jcrImpactFactorDTO.setYear(yearNew);
          jcrImpactFactorDTO.setJournal(journal);
        }
      }
    }
    if (jcrImpactFactorDTO.getJournal() == null) {
      return null;
    } else {
      return jcrImpactFactorDTO;
    }
  }

  private boolean isDouble(String number) {
    try {
      double test = Double.parseDouble(number);
      return true;
    } catch (NumberFormatException numberFormatException) {
      return false;
    }
  }

  private JCRImpactFactorDTO computeImpactFactorOld(String journal, int year) {
    if (journal.strip().equals("")) {
      return null;
    }
    String titleContent = preprocessJournalTitle(journal);
    RCode code = RCode.create();
    code.addString("input", titleContent);
    code.addInt("year", year);
    code.addRCode("if(!\"JCRImpactFactor\" %in% rownames(installed.packages())){");
    code.addRCode(
        "install.packages(\"JCRImpactFactor\",repos = \"http://cran.us.r-project.org\")}");
    code.addRCode(
        "resultTest <- JCRImpactFactor::find.IF.JCR(input,exact.match = FALSE,year=year)");
    code.addRCode("resScores=resultTest[,2]");
    code.addRCode("resNames=resultTest[,1]");
    RCaller caller = RCaller.create(code, RCallerOptions.create());
    caller.runAndReturnResult("resNames");
    String[] result = caller.getParser().getAsStringArray("resNames");
    caller = RCaller.create(code, RCallerOptions.create());
    caller.runAndReturnResult("resScores");
    String[] resultingScores = caller.getParser().getAsStringArray("resScores");
    double minScore = 1.0;
    if (resultingScores.length > 0) {
      int indexMax = -1;
      for (int i = 0; i < result.length; i++) {
        String resJournal = result[i];
        if (checkJournalTitleMatch(titleContent, resJournal)) {
          double distance = new JaroWinklerDistance().apply(journal.toLowerCase(),
              resJournal.toLowerCase());
          if (distance <= minScore) {
            minScore = distance;
            indexMax = i;
          }
        }
      }
      if (indexMax != -1) {
        JCRImpactFactorDTO impactFactorDTO = new JCRImpactFactorDTO();
        impactFactorDTO.setJournal(result[indexMax]);
        impactFactorDTO.setYear(year);
        if (isDouble(resultingScores[indexMax])) {
          double impactFactor = convertToDouble(resultingScores[indexMax]);
          impactFactorDTO.setImpactFactor(impactFactor);
          return (impactFactorDTO);
        } else {
          return null;
        }
      } else {
        return null;
      }
    }
    return null;
  }

  public JCRImpactFactorDTO getImpactFactor(String journal, int year) {
    JCRImpactFactorDTO res = computeImpactFactor(journal, year);
    return res;
  }

  private JCRImpactFactorDTO computeImpactFactorNearestYear(String journal, int year) {
    if (journal.strip().equals("")) {
      return null;
    }
    String titleContent = preprocessJournalTitle(journal);
    RCode code = RCode.create();
    code.addString("input", titleContent);
    code.addRCode("if(!\"JCRImpactFactor\" %in% rownames(installed.packages())){");
    code.addRCode(
        "install.packages(\"JCRImpactFactor\",repos = \"http://cran.us.r-project.org\")}");
    code.addRCode(
        "resultTest <- JCRImpactFactor::find.IF.JCR(input,exact.match = FALSE)");
    code.addRCode("resScores=unlist(resultTest[,-1])");
    code.addRCode("resNames=resultTest[,1]");
    code.addRCode("resYearNames=colnames(resultTest[-1])");
    RCaller caller = RCaller.create(code, RCallerOptions.create());
    caller.runAndReturnResult("resNames");
    String[] result = caller.getParser().getAsStringArray("resNames");
    caller = RCaller.create(code, RCallerOptions.create());
    caller.runAndReturnResult("resScores");
    String[] resultingScores = caller.getParser().getAsStringArray("resScores");
    caller = RCaller.create(code, RCallerOptions.create());
    caller.runAndReturnResult("resYearNames");
    String[] resYearNames = caller.getParser().getAsStringArray("resYearNames");
    double minScore = 1.0;
    if (resultingScores.length > 0) {
      int indexMax = -1;
      for (int i = 0; i < result.length; i++) {
        String resJournal = result[i];
        if (checkJournalTitleMatch(titleContent, resJournal)) {
          double distance = new JaroWinklerDistance().apply(journal.toLowerCase(),
              resJournal.toLowerCase());
          if (distance <= minScore) {
            minScore = distance;
            indexMax = i;
          }
        }
      }
      int indexNew = -1;
      int distanceYear = 1000;
      int i = 0;
      if (indexMax != -1) {
        for (String yearFound : resYearNames) {
          String yearFoundExtracted = yearFound.substring(2);
          if (isInteger(yearFoundExtracted)) {
            int yearFoundNumber = convertToInteger(yearFoundExtracted);
            if ((Math.abs(yearFoundNumber - year)) < distanceYear) {
              String valueFound = resultingScores[indexMax * resYearNames.length + i];
              if (!valueFound.equalsIgnoreCase("na")) {
                distanceYear = Math.abs(yearFoundNumber - year);
                indexNew = i;
              }
            }
          }
          i++;
        }
        if (indexNew != -1) {
          JCRImpactFactorDTO impactFactorDTO = new JCRImpactFactorDTO();
          impactFactorDTO.setJournal(result[indexMax]);
          String yearFound = resYearNames[indexNew];
          String yearFoundExtracted = yearFound.substring(2);
          if (isInteger(yearFoundExtracted)) {
            int yearFoundNumber = convertToInteger(yearFoundExtracted);
            impactFactorDTO.setYear(yearFoundNumber);
            String value = resultingScores[indexMax * resYearNames.length + indexNew];
            if (isDouble(value)) {
              double impactFactor = convertToDouble(value);
              impactFactorDTO.setImpactFactor(impactFactor);
              return impactFactorDTO;
            }
          }
        }
      }
    }
    return null;
  }

  private int convertToInteger(String yearBegin) {
    try {
      int value = Integer.valueOf(yearBegin);
      return value;
    } catch (NumberFormatException e) {
      return -1;
    }
  }

  private boolean isInteger(String yearBegin) {
    try {
      int value = Integer.valueOf(yearBegin);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }


  private double convertToDouble(String resultingScore) {
    try {
      double value = Double.parseDouble(resultingScore);
      return value;
    } catch (NumberFormatException e) {
      return -1.0;
    }
  }
}
