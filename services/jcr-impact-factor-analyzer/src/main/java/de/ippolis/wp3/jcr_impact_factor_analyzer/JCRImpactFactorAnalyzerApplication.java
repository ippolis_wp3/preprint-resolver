package de.ippolis.wp3.jcr_impact_factor_analyzer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * This is the application entry point. Change the class name as necessary.
 *
 * Usually there is no reason to add any other code in this file.
 **/
@SpringBootApplication
@ComponentScan({"de.ippolis.wp3"})
public class JCRImpactFactorAnalyzerApplication {

  public static void main(String[] args) {
    SpringApplication.run(JCRImpactFactorAnalyzerApplication.class, args);
  }
}
