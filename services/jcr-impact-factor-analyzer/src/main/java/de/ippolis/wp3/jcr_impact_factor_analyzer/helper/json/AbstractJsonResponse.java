package de.ippolis.wp3.jcr_impact_factor_analyzer.helper.json;

/**
 * Basic JSON response providing a response status.
 */
public abstract class AbstractJsonResponse implements JsonResponse {

  private JsonResponseStatus status;

  AbstractJsonResponse(JsonResponseStatus status) {
    this.status = status;
  }

  @Override
  public JsonResponseStatus getStatus() {
    return status;
  }

  @Override
  public void setStatus(JsonResponseStatus status) {
    this.status = status;
  }
}
