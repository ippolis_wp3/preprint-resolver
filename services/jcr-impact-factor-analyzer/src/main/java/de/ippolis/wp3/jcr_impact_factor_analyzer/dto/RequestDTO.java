package de.ippolis.wp3.jcr_impact_factor_analyzer.dto;

public class RequestDTO {
  private String journal;
  private int year;

  public RequestDTO(String journal, int year) {
    this.journal = journal;
    this.year = year;
  }

  public String getJournal() {
    return journal;
  }

  public int getYear() {
    return year;
  }
}
