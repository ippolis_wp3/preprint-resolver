package de.ippolis.wp3.jcr_impact_factor_analyzer.repository;

import de.ippolis.wp3.jcr_impact_factor_analyzer.dto.JCRImpactFactorEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JCRImpactFactorRepository extends
    JpaRepository<JCRImpactFactorEntity, Long> {

  public List<JCRImpactFactorEntity> findByJournalTitleIgnoreCase(String journalTitle);
}
