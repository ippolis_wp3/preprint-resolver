package de.ippolis.wp3.jcr_impact_factor_analyzer.helper.json;

/**
 * Tagging interface for JSON responses providing a response status.
 * <p>
 * TODO make common interface for all responses: add getData, getMessage, isFail, isSuccess etc.,
 *  then use factory to create success/error/fail responses
 */
public interface JsonResponse {

  JsonResponseStatus getStatus();

  void setStatus(JsonResponseStatus status);
}
