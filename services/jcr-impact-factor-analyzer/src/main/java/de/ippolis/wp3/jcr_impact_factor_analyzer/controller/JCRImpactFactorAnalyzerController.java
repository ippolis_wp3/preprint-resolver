package de.ippolis.wp3.jcr_impact_factor_analyzer.controller;


import de.ippolis.wp3.jcr_impact_factor_analyzer.dto.JCRImpactFactorDTO;
import de.ippolis.wp3.jcr_impact_factor_analyzer.dto.RequestDTO;
import de.ippolis.wp3.jcr_impact_factor_analyzer.helper.json.JsonResponse;
import de.ippolis.wp3.jcr_impact_factor_analyzer.helper.json.SuccessJsonResponse;
import de.ippolis.wp3.jcr_impact_factor_analyzer.service.JCRImpactFactorAnalyzer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Example REST controller.
 */
@RestController
public class JCRImpactFactorAnalyzerController {

  private final JCRImpactFactorAnalyzer JCRImpactFactorAnalyzer;
  Logger logger = LoggerFactory.getLogger(JCRImpactFactorAnalyzerController.class);

  @Autowired
  public JCRImpactFactorAnalyzerController(JCRImpactFactorAnalyzer JCRImpactFactorAnalyzer) {
    this.JCRImpactFactorAnalyzer = JCRImpactFactorAnalyzer;
  }

  /**
   * Once running, this service will print the current time at lb://microservice-template/time (or
   * http://localhost:8080/microservice-template/time)
   * <p>
   * The Locale is automatically extracted from the URL parameter locale.
   **/
  @PostMapping(value = "/getValue", consumes = "application/json", produces = "application/json")
  public JsonResponse getJCRImpactFactor(@RequestBody RequestDTO requestDTO) {
    String journal = requestDTO.getJournal();
    int year = requestDTO.getYear();
    JCRImpactFactorDTO res = this.JCRImpactFactorAnalyzer.getImpactFactor(journal, year);
    return new SuccessJsonResponse(res);
  }
}
