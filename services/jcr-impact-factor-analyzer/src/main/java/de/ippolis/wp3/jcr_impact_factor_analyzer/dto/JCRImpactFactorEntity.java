package de.ippolis.wp3.jcr_impact_factor_analyzer.dto;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity(name = "JCRTABLE")
public class JCRImpactFactorEntity {

  @Id
  @Column(name = "ID")
  private String id;
  @Column(name = "IEJOURNALTITLE")
  private String journalTitle;
  @Column(name = "IF2010")
  private String IF2010;
  @Column(name = "IF2011")
  private String IF2011;
  @Column(name = "IF2012")
  private String IF2012;
  @Column(name = "IF2013")
  private String IF2013;
  @Column(name = "IF2014")
  private String IF2014;
  @Column(name = "IF2015")
  private String IF2015;
  @Column(name = "IF2016")
  private String IF2016;
  @Column(name = "IF2017")
  private String IF2017;
  @Column(name = "IF2018")
  private String IF2018;
  @Column(name = "IF2019")
  private String IF2019;

  private JCRImpactFactorEntity() {
  }

  public JCRImpactFactorEntity(String id, String journalTitle, String IF2010, String IF2011,
      String IF2012, String IF2013, String IF2014, String IF2015, String IF2016,
      String IF2017, String IF2018, String IF2019) {
    this.id = id;
    this.journalTitle = journalTitle;
    this.IF2010 = IF2010;
    this.IF2011 = IF2011;
    this.IF2012 = IF2012;
    this.IF2013 = IF2013;
    this.IF2014 = IF2014;
    this.IF2015 = IF2015;
    this.IF2016 = IF2016;
    this.IF2017 = IF2017;
    this.IF2018 = IF2018;
    this.IF2019 = IF2019;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getJournalTitle() {
    return journalTitle;
  }

  public void setJournalTitle(String journalTitle) {
    this.journalTitle = journalTitle;
  }

  public String getIF2010() {
    return IF2010;
  }

  public void setIF2010(String IF2010) {
    this.IF2010 = IF2010;
  }

  public String getIF2011() {
    return IF2011;
  }

  public void setIF2011(String IF2011) {
    this.IF2011 = IF2011;
  }

  public String getIF2012() {
    return IF2012;
  }

  public void setIF2012(String IF2012) {
    this.IF2012 = IF2012;
  }

  public String getIF2013() {
    return IF2013;
  }

  public void setIF2013(String IF2013) {
    this.IF2013 = IF2013;
  }

  public String getIF2014() {
    return IF2014;
  }

  public void setIF2014(String IF2014) {
    this.IF2014 = IF2014;
  }

  public String getIF2015() {
    return IF2015;
  }

  public void setIF2015(String IF2015) {
    this.IF2015 = IF2015;
  }

  public String getIF2016() {
    return IF2016;
  }

  public void setIF2016(String IF2016) {
    this.IF2016 = IF2016;
  }

  public String getIF2017() {
    return IF2017;
  }

  public void setIF2017(String IF2017) {
    this.IF2017 = IF2017;
  }

  public String getIF2018() {
    return IF2018;
  }

  public void setIF2018(String IF2018) {
    this.IF2018 = IF2018;
  }

  public String getIF2019() {
    return IF2019;
  }

  public void setIF2019(String IF2019) {
    this.IF2019 = IF2019;
  }
}
