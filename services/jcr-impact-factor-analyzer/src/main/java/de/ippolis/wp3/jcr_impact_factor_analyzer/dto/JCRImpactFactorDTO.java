package de.ippolis.wp3.jcr_impact_factor_analyzer.dto;

public class JCRImpactFactorDTO {

  private String journal;
  private double impactFactor;
  private int year;

  public JCRImpactFactorDTO() {
  }

  public JCRImpactFactorDTO(String journal, double impactFactor, int year) {
    this.journal = journal;
    this.impactFactor = impactFactor;
    this.year = year;
  }

  public String getJournal() {
    return journal;
  }

  public void setJournal(String journal) {
    this.journal = journal;
  }

  public double getImpactFactor() {
    return impactFactor;
  }

  public void setImpactFactor(double impactFactor) {
    this.impactFactor = impactFactor;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }
}
