package de.ippolis.wp3.local_database_literature_service.dblp.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Provides REST functionality.
 */
@Service
public class RawRestServiceDBLP {

  private final RestTemplate rawRestTemplate;

  @Autowired
  public RawRestServiceDBLP(RestTemplate rawRestTemplate) {
    this.rawRestTemplate = rawRestTemplate;
  }

  /**
   * Performs a JSON GET request to the given URL trying to deserialize the result into the given
   * response type.
   */
  public <T> T performJsonGetRequest(String requestUri, Class<T> responseType) {
    return rawRestTemplate.getForObject(requestUri, responseType);
  }

  /**
   * Performs a JSON POST request to the given URL with the given parameters, converting the result
   * into a {@link JSONObject}.
   */
  public String performJsonGetRequest(String requestUri) {
    String responseString = this.performJsonGetRequest(requestUri, String.class);

    return responseString;
  }
}
