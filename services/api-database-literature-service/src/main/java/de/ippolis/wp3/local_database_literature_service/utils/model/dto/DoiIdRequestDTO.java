package de.ippolis.wp3.local_database_literature_service.utils.model.dto;

public class DoiIdRequestDTO {

  private String doi;
  private String id;

  public DoiIdRequestDTO() {
  }

  public DoiIdRequestDTO(String doi, String id) {
    this.doi = doi;
    this.id = id;
  }

  public String getDoi() {
    return doi;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
