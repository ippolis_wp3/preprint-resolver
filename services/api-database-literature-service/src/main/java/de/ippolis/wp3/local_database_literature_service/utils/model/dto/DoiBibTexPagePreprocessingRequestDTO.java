package de.ippolis.wp3.local_database_literature_service.utils.model.dto;

import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;

public class DoiBibTexPagePreprocessingRequestDTO {

  private String doi;
  private BibTexPagePreprocessing bibTexPagePreprocessing;
  private String id;

  public DoiBibTexPagePreprocessingRequestDTO() {
  }

  public DoiBibTexPagePreprocessingRequestDTO(String doi,
      BibTexPagePreprocessing bibTexPagePreprocessing, String id) {
    this.doi = doi;
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDoi() {
    return doi;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }
}
