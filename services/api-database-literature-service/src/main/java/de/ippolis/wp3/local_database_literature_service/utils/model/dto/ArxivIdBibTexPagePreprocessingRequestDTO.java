package de.ippolis.wp3.local_database_literature_service.utils.model.dto;

import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;

public class ArxivIdBibTexPagePreprocessingRequestDTO {

  private String arxivId;
  private BibTexPagePreprocessing bibTexPagePreprocessing;

  public ArxivIdBibTexPagePreprocessingRequestDTO() {
  }

  public ArxivIdBibTexPagePreprocessingRequestDTO(String arxivId,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.arxivId = arxivId;
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }

  public String getArxivId() {
    return arxivId;
  }

  public void setArxivId(String arxivId) {
    this.arxivId = arxivId;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }
}
