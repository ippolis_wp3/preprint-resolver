package de.ippolis.wp3.local_database_literature_service.openalex.service;

import java.io.IOException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Provides REST functionality
 */
@Service
public class RawRestServiceOpenAlex {

  private final RestTemplate rawRestTemplate;

  @Autowired
  public RawRestServiceOpenAlex(RestTemplate rawRestTemplate) {
    this.rawRestTemplate = rawRestTemplate;
  }

  /**
   * Performs a JSON POST request to the given URL with the given parameters, trying to deserialize
   * the result into the given response type
   */
  public <T> T performJsonGetRequest(String requestUri, Class<T> responseType) throws IOException {
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

    return rawRestTemplate
        .exchange(requestUri, HttpMethod.GET, requestEntity, responseType)
        .getBody();
  }

  /**
   * Performs a JSON POST request to the given URL with the given parameters, converting the result
   * into a {@link JSONObject}
   */
  public String performJsonGetRequest(String requestUri) throws IOException {
    String responseString = this.performJsonGetRequest(requestUri, String.class);

    return responseString;
  }
}
