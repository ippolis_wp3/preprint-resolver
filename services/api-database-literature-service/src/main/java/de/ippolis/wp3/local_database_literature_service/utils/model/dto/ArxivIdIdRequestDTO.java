package de.ippolis.wp3.local_database_literature_service.utils.model.dto;

public class ArxivIdIdRequestDTO {

  private String arxivId;
  private String id;

  public ArxivIdIdRequestDTO() {
  }

  public ArxivIdIdRequestDTO(String arxivId, String id) {
    this.arxivId = arxivId;
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getArxivId() {
    return arxivId;
  }

  public void setArxivId(String arxivId) {
    this.arxivId = arxivId;
  }
}
