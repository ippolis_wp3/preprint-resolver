package de.ippolis.wp3.local_database_literature_service.utils.model.dto;

import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;

public class TitleIdThresholdBibTexPagePreprocessingRequestDTO {

  private String title;
  private String id;
  private float threshold;
  private BibTexPagePreprocessing bibTexPagePreprocessing;

  public TitleIdThresholdBibTexPagePreprocessingRequestDTO() {
  }

  public TitleIdThresholdBibTexPagePreprocessingRequestDTO(String title, String id, float threshold,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.title = title;
    this.id = id;
    this.threshold = threshold;
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public float getThreshold() {
    return threshold;
  }

  public void setThreshold(float threshold) {
    this.threshold = threshold;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }
}
