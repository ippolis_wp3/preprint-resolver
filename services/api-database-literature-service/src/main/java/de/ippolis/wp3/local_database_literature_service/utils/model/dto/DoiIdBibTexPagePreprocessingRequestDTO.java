package de.ippolis.wp3.local_database_literature_service.utils.model.dto;

import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;

public class DoiIdBibTexPagePreprocessingRequestDTO {

  private String doi;
  private String id;
  private BibTexPagePreprocessing bibTexPagePreprocessing;

  public DoiIdBibTexPagePreprocessingRequestDTO() {
  }

  public DoiIdBibTexPagePreprocessingRequestDTO(String doi, String id,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.doi = doi;
    this.id = id;
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }

  public String getDoi() {
    return doi;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }
}
