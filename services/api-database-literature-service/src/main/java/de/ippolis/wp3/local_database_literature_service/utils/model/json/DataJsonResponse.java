package de.ippolis.wp3.local_database_literature_service.utils.model.json;

/**
 * JSON response containing data
 * <p>
 * TODO convert data to JSONObject
 */
public class DataJsonResponse extends AbstractJsonResponse {

  private Object data;

  public DataJsonResponse(JsonResponseStatus status, Object data) {
    super(status);
    this.data = data;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }
}
