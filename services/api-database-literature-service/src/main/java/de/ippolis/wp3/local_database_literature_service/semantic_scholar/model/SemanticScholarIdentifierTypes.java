package de.ippolis.wp3.local_database_literature_service.semantic_scholar.model;

public enum SemanticScholarIdentifierTypes {
  SEMANTIC_SCHOLAR_ID,
  DOI,
  CORPUSID,
  ARXIV,
  MAG,
  ACL,
  PMID,
  PMCID,
  URL
}
