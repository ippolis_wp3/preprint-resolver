package de.ippolis.wp3.local_database_literature_service.utils.caching;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.spi.CachingProvider;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.core.config.DefaultConfiguration;
import org.ehcache.impl.config.persistence.DefaultPersistenceConfiguration;
import org.ehcache.jsr107.Eh107Configuration;
import org.ehcache.jsr107.EhcacheCachingProvider;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CacheConfig {

  private org.ehcache.config.Configuration createMyConfig(final ClassLoader cl) {
    final File location =
        new File(
            System.getProperty(
                "java.io.tmpdir")); // Or some other location from some random properties file
    return new DefaultConfiguration(cl, new DefaultPersistenceConfiguration(location));
  }

  private javax.cache.configuration.Configuration<SimpleKey, String>
  createSimpleKeyStringConfiguration() {
    CacheConfiguration<SimpleKey, String> cachecConfig =
        CacheConfigurationBuilder.newCacheConfigurationBuilder(
                SimpleKey.class,
                String.class,
                ResourcePoolsBuilder.newResourcePoolsBuilder()
                    .heap(10, EntryUnit.ENTRIES)
                    .offheap(1, MemoryUnit.MB)
                    .disk(10, MemoryUnit.MB, true)
                    .build())
            .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofDays(1)))
            .withDefaultEventListenersThreadPool()
            .build();
    javax.cache.configuration.Configuration<SimpleKey, String> configuration =
        Eh107Configuration.fromEhcacheCacheConfiguration(cachecConfig);
    return configuration;
  }

  private javax.cache.configuration.Configuration<String, String>
  createStringStringCacheConfiguration() {
    CacheConfiguration<String, String> cachecConfig =
        CacheConfigurationBuilder.newCacheConfigurationBuilder(
                String.class,
                String.class,
                ResourcePoolsBuilder.newResourcePoolsBuilder()
                    .heap(10, EntryUnit.ENTRIES)
                    .offheap(1, MemoryUnit.MB)
                    .disk(10, MemoryUnit.MB, true)
                    .build())
            .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofDays(1)))
            .withDefaultEventListenersThreadPool()
            .build();
    javax.cache.configuration.Configuration<String, String> configuration =
        Eh107Configuration.fromEhcacheCacheConfiguration(cachecConfig);
    return configuration;
  }

  private Map<String, javax.cache.configuration.Configuration> createConfigurationMap() {
    Map<String, javax.cache.configuration.Configuration> configurationList = new LinkedHashMap<>();
    javax.cache.configuration.Configuration<String, String> cachecConfigSemanticScholarTitle =
        createStringStringCacheConfiguration();
    configurationList.put("semanticScholarTitle", cachecConfigSemanticScholarTitle);

    javax.cache.configuration.Configuration<String, String> cachecConfigDBLPTitle =
        createStringStringCacheConfiguration();
    configurationList.put("dblpTitle", cachecConfigDBLPTitle);
    javax.cache.configuration.Configuration<String, String> cachecConfigCrossCiteTitle =
        createStringStringCacheConfiguration();
    configurationList.put("crossCiteTitle", cachecConfigCrossCiteTitle);
    javax.cache.configuration.Configuration<String, String> cachecConfigOpenAlexTitle =
        createStringStringCacheConfiguration();
    configurationList.put("openAlexTitle", cachecConfigOpenAlexTitle);
    javax.cache.configuration.Configuration<SimpleKey, String> cachecConfigSemanticScholarId =
        createSimpleKeyStringConfiguration();
    configurationList.put("semanticScholarId", cachecConfigSemanticScholarId);

    javax.cache.configuration.Configuration<String, String> cachecConfigCrossCiteDOI =
        createStringStringCacheConfiguration();
    configurationList.put("crossCiteDOI", cachecConfigCrossCiteDOI);
    javax.cache.configuration.Configuration<String, String> cachecConfigOpenAlexDOI =
        createStringStringCacheConfiguration();
    configurationList.put("openAlexDOI", cachecConfigOpenAlexDOI);
    javax.cache.configuration.Configuration<String, String> cachecConfigDBLPArxivId =
        createStringStringCacheConfiguration();
    configurationList.put("dblpArxivID", cachecConfigDBLPArxivId);
    javax.cache.configuration.Configuration<String, String> cachecConfigDBLPVenue =
        createStringStringCacheConfiguration();
    configurationList.put("dblpVenue", cachecConfigDBLPVenue);
    return configurationList;
  }

  @Bean
  public CacheManager persistentCacheManager() throws IOException {

    CachingProvider cachingProvider =
        Caching.getCachingProvider("org.ehcache.jsr107.EhcacheCachingProvider");
    EhcacheCachingProvider ehcacheCachingProvider = (EhcacheCachingProvider) cachingProvider;

    CacheManager cacheManager =
        ehcacheCachingProvider.getCacheManager(
            ehcacheCachingProvider.getDefaultURI(),
            createMyConfig(cachingProvider.getDefaultClassLoader()));
    Map<String, javax.cache.configuration.Configuration> configurationMap =
        createConfigurationMap();
    for (String key : configurationMap.keySet()) {
      javax.cache.configuration.Configuration configuration = configurationMap.get(key);
      cacheManager.createCache(key, configuration);
    }
    return cacheManager;
  }
}
