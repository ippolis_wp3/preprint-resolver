package de.ippolis.wp3.literature_database_service.utils.model.dto;

import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import java.util.List;

public class TitleAuthorRequestDTO {
  private String title;
  private List<String> authors;
  private String id;
  private BibTexPagePreprocessing bibTexPagePreprocessing;
  private boolean useAPI;
  private boolean useLocalData;

  public TitleAuthorRequestDTO() {}

  public TitleAuthorRequestDTO(
      String title,
      List<String> authors,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      String id,
      boolean useAPI,
      boolean useLocalData) {
    this.title = title;
    this.authors = authors;
    this.id = id;
    this.useAPI = useAPI;
    this.useLocalData = useLocalData;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public boolean isUseAPI() {
    return useAPI;
  }

  public void setUseAPI(boolean useAPI) {
    this.useAPI = useAPI;
  }

  public boolean isUseLocalData() {
    return useLocalData;
  }

  public void setUseLocalData(boolean useLocalData) {
    this.useLocalData = useLocalData;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<String> getAuthors() {
    return authors;
  }

  public void setAuthors(List<String> authors) {
    this.authors = authors;
  }
}
