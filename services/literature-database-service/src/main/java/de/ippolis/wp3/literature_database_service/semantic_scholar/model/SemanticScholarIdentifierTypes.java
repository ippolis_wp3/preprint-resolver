package de.ippolis.wp3.literature_database_service.semantic_scholar.model;

public enum SemanticScholarIdentifierTypes {
  SEMANTIC_SCHOLAR_ID,
  DOI,
  CORPUSID,
  ARXIV,
  MAG,
  ACL,
  PMID,
  PMCID,
  URL
}
