package de.ippolis.wp3.literature_database_service.semantic_scholar.controller;

import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLog;
import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLogHelper;
import de.ippolis.wp3.literature_database_service.semantic_scholar.service.SemanticScholarLiteratureAnalyzer;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.model.dto.ArxivDoiTitleAuthorRequestDTO;
import de.ippolis.wp3.literature_database_service.utils.model.json.ErrorJsonResponse;
import de.ippolis.wp3.literature_database_service.utils.model.json.JsonResponse;
import de.ippolis.wp3.literature_database_service.utils.model.json.SuccessJsonResponse;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/** Example REST controller. */
@RestController
public class SemanticScholarController {
  private final SemanticScholarLiteratureAnalyzer semanticScholarLiteratureAnalyzer;
  private final FilterLogHelper filterLogHelper;
  Logger logger = LoggerFactory.getLogger(SemanticScholarController.class);

  @Autowired
  public SemanticScholarController(
      SemanticScholarLiteratureAnalyzer semanticScholarLiteratureAnalyzer,
      FilterLogHelper filterLogHelper) {
    this.semanticScholarLiteratureAnalyzer = semanticScholarLiteratureAnalyzer;
    this.filterLogHelper = filterLogHelper;
  }

  @PostMapping(
      value = "/semantic-scholar/getReferences",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse fetchSemanticScholarReferences(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      String id = requestDTO.getId();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      List<FilterLog> filterLogs = new LinkedList<>();
      List<Reference> result =
          this.semanticScholarLiteratureAnalyzer.resolveLiteratureEntry(
              id,
              doi,
              arxivId,
              title,
              authors,
              useLocalData,
              useAPI,
              bibTexPagePreprocessing,
              filterLogs);
      Map<String, Object> results = new LinkedHashMap<>();
      results.put("references", result);
      results.put("filterLogs", filterLogHelper.convertListOfFilterLogsToListOfMaps(filterLogs));
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/semantic-scholar/fetchNumberOfCitations",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse fetchNumberOfCitations(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String doi = requestDTO.getDoi();
      String id = requestDTO.getId();
      String arxivId = requestDTO.getArxivId();
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<FilterLog> filterLogs = new LinkedList<>();

      int result =
          semanticScholarLiteratureAnalyzer.getNumberOfCitations(
              doi, arxivId, title, authors, filterLogs);
      Map<String, Object> results = new LinkedHashMap<>();
      results.put("result", result);
      results.put("filterLogs", filterLogHelper.convertListOfFilterLogsToListOfMaps(filterLogs));
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/semantic-scholar/fetchNumberOfHighlyInfluentialCitations",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse fetchNumberOfHighlyInfluentialCitations(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String id = requestDTO.getId();
      String arxivId = requestDTO.getArxivId();
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      String doi = requestDTO.getDoi();
      List<FilterLog> filterLogs = new LinkedList<>();
      int result =
          semanticScholarLiteratureAnalyzer.getNumberOfHighlyInfluentialCitations(
              doi, arxivId, title, authors, filterLogs);
      Map<String, Object> results = new LinkedHashMap<>();
      results.put("result", result);
      results.put("filterLogs", filterLogHelper.convertListOfFilterLogsToListOfMaps(filterLogs));
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/semantic-scholar/resolveArxiv",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse resolveArxiv(@RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      String id = requestDTO.getId();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      List<String> authors = requestDTO.getAuthors();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<FilterLog> filterLogs = new LinkedList<>();
      List<Reference> references =
          semanticScholarLiteratureAnalyzer.resolveLiteratureEntryArxiv(
              id,
              doi,
              arxivId,
              title,
              authors,
              useLocalData,
              useAPI,
              bibTexPagePreprocessing,
              filterLogs);
      String result = "";
      if (references.size() > 0) {
        result = references.get(0).toLatexString(true);
      }
      Map<String, Object> results = new LinkedHashMap<>();
      results.put("result", result);
      results.put("filterLogs", filterLogHelper.convertListOfFilterLogsToListOfMaps(filterLogs));
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/semantic-scholar/resolveArxivFrontend",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse resolveArxivByArxivID(@RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {

    try {
      String arxivId = requestDTO.getArxivId();
      String doi = requestDTO.getDoi();
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      String id = requestDTO.getId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<FilterLog> filterLogs = new LinkedList<>();
      List<Reference> result =
          semanticScholarLiteratureAnalyzer.resolveLiteratureEntryArxiv(
              id,
              doi,
              arxivId,
              title,
              authors,
              useLocalData,
              useAPI,
              bibTexPagePreprocessing,
              filterLogs);
      List<Map<String, Object>> resultsMap = new LinkedList<>();
      for (Reference reference : result) {
        resultsMap.add(reference.toSimpleMap(true));
      }
      Map<String, Object> results = new LinkedHashMap<>();
      results.put("resultsMap", resultsMap);
      results.put("filterLogs", filterLogHelper.convertListOfFilterLogsToListOfMaps(filterLogs));
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;

    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }
}
