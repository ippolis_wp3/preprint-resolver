package de.ippolis.wp3.literature_database_service.semantic_scholar.service;

import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLog;
import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLogHelper;
import de.ippolis.wp3.literature_database_service.utils.helper.BibTexHelperService;
import de.ippolis.wp3.literature_database_service.utils.model.AbstractLiteratureAnalyzer;
import de.ippolis.wp3.literature_database_service.utils.model.ArxivHelper;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexReferenceComparisonHelper;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.service.H2RestService;
import de.ippolis.wp3.literature_database_service.utils.service.JCRRestService;
import de.ippolis.wp3.literature_database_service.utils.service.LocalRestService;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SemanticScholarLiteratureAnalyzer extends AbstractLiteratureAnalyzer {
  private final SemanticScholarAPIReferenceService semanticScholarAPIReferenceService;

  @Autowired
  public SemanticScholarLiteratureAnalyzer(
      ArxivHelper arxivHelper,
      BibTexHelperService bibTexHelperService,
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper,
      SemanticScholarAPIReferenceService semanticScholarAPIReferenceService,
      JCRRestService jcrRestService,
      H2RestService h2RestService,
      LocalRestService localRestService,
      FilterLogHelper filterLogHelper) {
    super(
        arxivHelper,
        bibTexHelperService,
        bibTexReferenceComparisonHelper,
        jcrRestService,
        h2RestService,
        localRestService,
        filterLogHelper);
    this.semanticScholarAPIReferenceService = semanticScholarAPIReferenceService;
  }

  @Override
  protected List<Reference> extractAPIResultsByDOI(
      String id,
      String doi,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> results;
    if (doi == null || doi.strip().equals("")) {
      results = new LinkedList<>();
    } else {
      results =
          semanticScholarAPIReferenceService.getReferencesByDOI(doi, bibTexPagePreprocessing, id);
    }
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "SemanticScholarAPIDOIExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractAPIResultsByTitle(
      String id,
      String title,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> results;
    if (title == null || title.strip().equals("")) {
      results = new LinkedList<>();
    } else {
      results =
          semanticScholarAPIReferenceService.getReferencesByTitle(
              title, bibTexPagePreprocessing, id);
    }
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "SemanticScholarAPITitleExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractAPIResultsByArxivId(
      String id,
      String arxivId,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> results;
    if (arxivId == null || arxivId.strip().equals("")) {
      results = new LinkedList<>();
    } else {
      results =
          semanticScholarAPIReferenceService.getReferencesByArxiv(arxivId, bibTexPagePreprocessing);
    }
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "SemanticScholarAPIArxivIdExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractLocalResultsByTitle(
      String id,
      String title,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> results = new LinkedList<>();
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "SemanticScholarLocalTitleExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractLocalResultsByDOI(
      String id,
      String doi,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> results = new LinkedList<>();
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "SemanticScholarLocalDOIExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractLocalResultsByArxivId(
      String id,
      String arxivId,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> results = new LinkedList<>();
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "SemanticScholarLocalArxivIdExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }
}
