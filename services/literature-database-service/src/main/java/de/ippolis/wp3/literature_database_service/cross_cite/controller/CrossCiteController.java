package de.ippolis.wp3.literature_database_service.cross_cite.controller;

import de.ippolis.wp3.literature_database_service.cross_cite.service.CrossCiteLiteratureAnalyzer;
import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLog;
import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLogHelper;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.model.dto.ArxivDoiTitleAuthorRequestDTO;
import de.ippolis.wp3.literature_database_service.utils.model.json.ErrorJsonResponse;
import de.ippolis.wp3.literature_database_service.utils.model.json.JsonResponse;
import de.ippolis.wp3.literature_database_service.utils.model.json.SuccessJsonResponse;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CrossCiteController {
  private final CrossCiteLiteratureAnalyzer crossCiteLiteratureAnalyzer;
  private final FilterLogHelper filterLogHelper;
  Logger logger = LoggerFactory.getLogger(CrossCiteController.class);

  @Autowired
  public CrossCiteController(
      CrossCiteLiteratureAnalyzer crossCiteLiteratureAnalyzer, FilterLogHelper filterLogHelper) {
    this.crossCiteLiteratureAnalyzer = crossCiteLiteratureAnalyzer;
    this.filterLogHelper = filterLogHelper;
  }

  @PostMapping(
      value = "/cross-cite/getReferences",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getReferencesCrossCite(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      String id = requestDTO.getId();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<FilterLog> filterLogs = new LinkedList<>();
      List<Reference> result =
          crossCiteLiteratureAnalyzer.resolveLiteratureEntry(
              id,
              doi,
              arxivId,
              title,
              authors,
              useLocalData,
              useAPI,
              bibTexPagePreprocessing,
              filterLogs);
      Map<String, Object> results = new LinkedHashMap<>();
      results.put("references", result);
      results.put("filterLogs", filterLogHelper.convertListOfFilterLogsToListOfMaps(filterLogs));
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/cross-cite/fetchNumberOfCitations",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse fetchNumberOfCitations(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      List<FilterLog> filterLogs = new LinkedList<>();
      int result =
          this.crossCiteLiteratureAnalyzer.getNumberOfCitations(
              doi, arxivId, title, authors, filterLogs);
      Map<String, Object> results = new LinkedHashMap<>();
      results.put("result", result);
      results.put("filterLogs", filterLogHelper.convertListOfFilterLogsToListOfMaps(filterLogs));
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/cross-cite/resolveArxivToString",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getInfoOpenAlexByDoi(@RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      String id = requestDTO.getId();
      List<String> authors = requestDTO.getAuthors();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      List<FilterLog> filterLogs = new LinkedList<>();
      List<Reference> references =
          crossCiteLiteratureAnalyzer.resolveLiteratureEntry(
              id,
              doi,
              arxivId,
              title,
              authors,
              useLocalData,
              useAPI,
              bibTexPagePreprocessing,
              filterLogs);
      String result = "";
      if (references.size() > 0) {
        result = references.get(0).toLatexString(true);
      }
      Map<String, Object> results = new LinkedHashMap<>();
      results.put("result", result);
      results.put("filterLogs", filterLogHelper.convertListOfFilterLogsToListOfMaps(filterLogs));
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/cross-cite/resolveArxivFrontend",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse solveArxivByArxivID(@RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String arxivId = requestDTO.getArxivId();
      String doi = requestDTO.getDoi();
      String title = requestDTO.getTitle();

      List<String> authors = requestDTO.getAuthors();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      String id = requestDTO.getId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      List<FilterLog> filterLogs = new LinkedList<>();
      List<Reference> references =
          crossCiteLiteratureAnalyzer.resolveLiteratureEntryArxiv(
              id,
              doi,
              arxivId,
              title,
              authors,
              useLocalData,
              useAPI,
              bibTexPagePreprocessing,
              filterLogs);
      List<Map<String, Object>> result = new LinkedList<>();
      for (Reference reference : references) {
        result.add(reference.toSimpleMap(true));
      }
      Map<String, Object> results = new LinkedHashMap<>();
      results.put("resultsMap", result);
      results.put("filterLogs", filterLogHelper.convertListOfFilterLogsToListOfMaps(filterLogs));
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;

    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }
}
