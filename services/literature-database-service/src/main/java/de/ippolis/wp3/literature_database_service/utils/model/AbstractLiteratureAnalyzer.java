package de.ippolis.wp3.literature_database_service.utils.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLog;
import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLogHelper;
import de.ippolis.wp3.literature_database_service.utils.helper.BibTexHelperService;
import de.ippolis.wp3.literature_database_service.utils.service.H2RestService;
import de.ippolis.wp3.literature_database_service.utils.service.JCRRestService;
import de.ippolis.wp3.literature_database_service.utils.service.LocalRestService;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class AbstractLiteratureAnalyzer {
  private final ArxivHelper arxivHelper;
  private final BibTexHelperService bibTexHelperService;
  private final BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper;
  private final LocalRestService localRestService;
  private final JCRRestService jcrRestService;
  private final H2RestService h2RestService;
  private final FilterLogHelper filterLogHelper;
  Logger logger = LoggerFactory.getLogger(AbstractLiteratureAnalyzer.class);

  @Autowired
  public AbstractLiteratureAnalyzer(
      ArxivHelper arxivHelper,
      BibTexHelperService bibTexHelperService,
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper,
      JCRRestService jcrRestService,
      H2RestService h2RestService,
      LocalRestService localRestService,
      FilterLogHelper filterLogHelper) {
    this.arxivHelper = arxivHelper;
    this.bibTexHelperService = bibTexHelperService;
    this.bibTexReferenceComparisonHelper = bibTexReferenceComparisonHelper;
    this.jcrRestService = jcrRestService;
    this.h2RestService = h2RestService;
    this.localRestService = localRestService;
    this.filterLogHelper = filterLogHelper;
  }

  public List<Reference> resolveLiteratureEntry(
      String id,
      String doi,
      String arxivId,
      String title,
      List<String> authors,
      boolean useLocalDatabase,
      boolean useAPI,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> arxivResultsLocalArxivId = new LinkedList<>();
    List<Reference> arxivResultsLocalDoi = new LinkedList<>();
    List<Reference> arxivResultsLocalTitle = new LinkedList<>();

    List<Reference> arxivResultsAPIArxivId = new LinkedList<>();
    List<Reference> arxivResultsAPIDoi = new LinkedList<>();
    List<Reference> arxivResultsAPITitle = new LinkedList<>();
    if (useLocalDatabase) {
      List<Reference> localResultsByArxivId =
          extractLocalResultsByArxivId(id, arxivId, bibTexPagePreprocessing, filterLogs);
      List<Reference> filteredListArxivIdLocal =
          arxivIdFilter(localResultsByArxivId, arxivId, filterLogs);
      filteredListArxivIdLocal = generalFilter(filteredListArxivIdLocal, filterLogs);
      arxivResultsLocalArxivId = identifyDirectArxivReferences(filteredListArxivIdLocal);
      List<Map<String, Object>> listInput =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListArxivIdLocal);
      filteredListArxivIdLocal.removeAll(arxivResultsLocalArxivId);
      List<Map<String, Object>> listOutput =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListArxivIdLocal);
      int lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
      FilterLog filterLog =
          new FilterLog(
              "RemoveArxivReferences",
              lastIndexInLog + 1,
              filteredListArxivIdLocal.size() + arxivResultsLocalArxivId.size(),
              filteredListArxivIdLocal.size(),
              listInput,
              listOutput);
      filterLogs.add(filterLog);
      if (filteredListArxivIdLocal.size() > 0) {
        return filteredListArxivIdLocal;
      }
      List<Reference> localResultsByDOI =
          extractLocalResultsByDOI(id, doi, bibTexPagePreprocessing, filterLogs);
      List<Reference> filteredListDOILocal = doiFilter(localResultsByDOI, doi, filterLogs);
      filteredListDOILocal = generalFilter(filteredListDOILocal, filterLogs);
      List<Map<String, Object>> listInputDOI =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListDOILocal);
      arxivResultsLocalDoi = identifyDirectArxivReferences(filteredListDOILocal);
      filteredListDOILocal.removeAll(arxivResultsLocalDoi);
      List<Map<String, Object>> listOutputDOI =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListDOILocal);
      lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
      FilterLog filterLogDOI =
          new FilterLog(
              "RemoveArxivReferences",
              lastIndexInLog + 1,
              filteredListDOILocal.size() + arxivResultsLocalDoi.size(),
              filteredListDOILocal.size(),
              listInputDOI,
              listOutputDOI);
      filterLogs.add(filterLogDOI);
      if (filteredListDOILocal.size() > 0) {
        return filteredListDOILocal;
      }
      List<Reference> localResultsByTitle =
          extractLocalResultsByTitle(id, title, bibTexPagePreprocessing, filterLogs);

      List<Reference> filteredListTitleLocal = titleFilter(localResultsByTitle, title, filterLogs);
      List<Reference> filteredListAuthorLocal =
          authorFilter(filteredListTitleLocal, authors, filterLogs);
      filteredListTitleLocal = generalFilter(filteredListAuthorLocal, filterLogs);
      arxivResultsLocalTitle = identifyDirectArxivReferences(filteredListTitleLocal);
      List<Map<String, Object>> listInputTitle =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListTitleLocal);
      filteredListTitleLocal.removeAll(arxivResultsLocalTitle);
      List<Map<String, Object>> listOutputTitle =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListTitleLocal);
      lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
      FilterLog filterLogTitle =
          new FilterLog(
              "RemoveArxivReferences",
              lastIndexInLog + 1,
              filteredListTitleLocal.size() + arxivResultsLocalTitle.size(),
              filteredListTitleLocal.size(),
              listInputTitle,
              listOutputTitle);
      filterLogs.add(filterLogTitle);
      if (filteredListTitleLocal.size() > 0) {
        return filteredListTitleLocal;
      }
    }

    if (useAPI) {
      List<Reference> localResultsByArxivId =
          extractAPIResultsByArxivId(id, arxivId, bibTexPagePreprocessing, filterLogs);
      List<Reference> filteredListArxivIdAPI =
          arxivIdFilter(localResultsByArxivId, arxivId, filterLogs);
      filteredListArxivIdAPI = generalFilter(filteredListArxivIdAPI, filterLogs);
      arxivResultsAPIArxivId = identifyDirectArxivReferences(filteredListArxivIdAPI);
      List<Map<String, Object>> listInput =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListArxivIdAPI);
      filteredListArxivIdAPI.removeAll(arxivResultsAPIArxivId);
      List<Map<String, Object>> listOutput =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListArxivIdAPI);
      int lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
      FilterLog filterLog =
          new FilterLog(
              "RemoveArxivReferences",
              lastIndexInLog + 1,
              filteredListArxivIdAPI.size() + arxivResultsAPIArxivId.size(),
              filteredListArxivIdAPI.size(),
              listInput,
              listOutput);
      filterLogs.add(filterLog);
      if (filteredListArxivIdAPI.size() > 0) {
        return filteredListArxivIdAPI;
      }
      List<Reference> localResultsByDOI =
          extractAPIResultsByDOI(id, doi, bibTexPagePreprocessing, filterLogs);
      List<Reference> filteredListDoiAPI = doiFilter(localResultsByDOI, doi, filterLogs);
      filteredListDoiAPI = generalFilter(filteredListDoiAPI, filterLogs);
      arxivResultsAPIDoi = identifyDirectArxivReferences(filteredListDoiAPI);
      List<Map<String, Object>> listInputDOI =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListDoiAPI);
      filteredListDoiAPI.removeAll(arxivResultsAPIDoi);
      List<Map<String, Object>> listOutputDOI =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListDoiAPI);
      lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
      FilterLog filterLogDOI =
          new FilterLog(
              "RemoveArxivReferences",
              lastIndexInLog + 1,
              filteredListDoiAPI.size() + arxivResultsAPIDoi.size(),
              filteredListDoiAPI.size(),
              listInputDOI,
              listOutputDOI);
      filterLogs.add(filterLogDOI);
      if (filteredListDoiAPI.size() > 0) {

        return filteredListDoiAPI;
      }
      List<Reference> localResultsByTitle =
          extractAPIResultsByTitle(id, title, bibTexPagePreprocessing, filterLogs);

      List<Reference> filteredListTitleAPI = titleFilter(localResultsByTitle, title, filterLogs);
      List<Reference> filteredListAuthorAPI =
          authorFilter(filteredListTitleAPI, authors, filterLogs);

      filteredListTitleAPI = generalFilter(filteredListAuthorAPI, filterLogs);
      arxivResultsAPITitle = identifyDirectArxivReferences(filteredListTitleAPI);
      List<Map<String, Object>> listInputTitle =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListTitleAPI);
      filteredListTitleAPI.removeAll(arxivResultsAPITitle);
      List<Map<String, Object>> listOutputTitle =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListTitleAPI);
      lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
      FilterLog filterLogTitle =
          new FilterLog(
              "RemoveArxivReferences",
              lastIndexInLog + 1,
              filteredListTitleAPI.size() + arxivResultsAPITitle.size(),
              filteredListTitleAPI.size(),
              listInputTitle,
              listOutputTitle);
      filterLogs.add(filterLogTitle);
      if (filteredListTitleAPI.size() > 0) {
        return filteredListTitleAPI;
      }
    }
    if (arxivResultsLocalArxivId.size() > 0) {
      return arxivResultsLocalArxivId;
    }
    if (arxivResultsLocalDoi.size() > 0) {
      return arxivResultsLocalDoi;
    }
    if (arxivResultsLocalTitle.size() > 0) {
      return arxivResultsLocalTitle;
    }
    if (arxivResultsAPIArxivId.size() > 0) {
      return arxivResultsAPIArxivId;
    }
    if (arxivResultsAPIDoi.size() > 0) {
      return arxivResultsAPIDoi;
    }
    if (arxivResultsAPITitle.size() > 0) {
      return arxivResultsAPITitle;
    }
    return new LinkedList<>();
  }

  public List<Reference> resolveLiteratureEntryArxiv(
      String id,
      String doi,
      String arxivId,
      String title,
      List<String> authors,
      boolean useLocalDatabase,
      boolean useAPI,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> references =
        resolveLiteratureEntryArxivFiltered(
            id,
            doi,
            arxivId,
            title,
            authors,
            useLocalDatabase,
            useAPI,
            bibTexPagePreprocessing,
            filterLogs);
    if (references.size() > 1) {
      List<Reference> filtered = identifyBestMatches(references, title, authors, filterLogs);
      for (Reference reference : filtered) {
        int counterCorrect = 0;
        int counterIncorrect = 0;
        String resolved = "";
        if (!reference.getJournal().strip().equals("")) {
          resolved = resolveAbbreviation(reference.getJournal());
        }
        JCRImpactFactor jcrImpactFactor =
            calculateJCRImpactFactor(reference.getJournal(), reference.getYear());
        if (jcrImpactFactor != null) {
          counterCorrect++;
          reference.setJcrImpactFactor(jcrImpactFactor);
        } else {
          if (!resolved.strip().equals("")) {
            jcrImpactFactor = calculateJCRImpactFactor(resolved, reference.getYear());
            if (jcrImpactFactor != null) {
              counterIncorrect++;
              reference.setJcrImpactFactor(jcrImpactFactor);
            }
          }
        }
        ScimagoJournalRankingDTO scimagoJournalRankingDTO =
            calculateScimagoJournalRanking(reference.getJournal(), reference.getYear());
        if (scimagoJournalRankingDTO != null) {
          reference.setScimagoJournalRankingDTO(scimagoJournalRankingDTO);
          counterCorrect++;
        } else {
          if (!resolved.strip().equals("")) {
            scimagoJournalRankingDTO =
                calculateScimagoJournalRanking(resolved, reference.getYear());
            if (scimagoJournalRankingDTO != null) {
              counterIncorrect++;
              reference.setScimagoJournalRankingDTO(scimagoJournalRankingDTO);
            }
          }
        }
        if (counterCorrect == 0) {
          if (counterIncorrect > 0) {
            if (!resolved.strip().equals("")) {
              reference.setJournal(resolved);
            }
          }
        }
      }
      return filtered;
    } else {
      for (Reference reference : references) {
        int counterCorrect = 0;
        int counterIncorrect = 0;
        JCRImpactFactor jcrImpactFactor =
            calculateJCRImpactFactor(reference.getJournal(), reference.getYear());
        String resolved = "";
        if (!reference.getJournal().strip().equals("")) {
          resolved = resolveAbbreviation(reference.getJournal());
        }
        if (jcrImpactFactor != null) {
          counterCorrect++;
          reference.setJcrImpactFactor(jcrImpactFactor);
        } else {
          if (!resolved.strip().equals("")) {
            jcrImpactFactor = calculateJCRImpactFactor(resolved, reference.getYear());
            if (jcrImpactFactor != null) {
              counterIncorrect++;
              reference.setJcrImpactFactor(jcrImpactFactor);
            }
          }
        }
        ScimagoJournalRankingDTO scimagoJournalRankingDTO =
            calculateScimagoJournalRanking(reference.getJournal(), reference.getYear());
        if (scimagoJournalRankingDTO != null) {
          counterCorrect++;
          reference.setScimagoJournalRankingDTO(scimagoJournalRankingDTO);
        } else {
          if (!resolved.strip().equals("")) {
            scimagoJournalRankingDTO =
                calculateScimagoJournalRanking(resolved, reference.getYear());
            if (scimagoJournalRankingDTO != null) {
              counterIncorrect++;
              reference.setScimagoJournalRankingDTO(scimagoJournalRankingDTO);
            }
          }
        }
        if (counterCorrect == 0) {
          if (counterIncorrect > 0) {
            if (!resolved.strip().equals("")) {
              reference.setJournal(resolved);
            }
          }
        }
      }
    }
    return references;
  }

  private ScimagoJournalRankingDTO calculateScimagoJournalRanking(String journalName, String year) {
    if (!journalName.strip().equals("")) {
      JSONObject requestParameters = new JSONObject();
      requestParameters.put("journalName", journalName);
      int yearExtracted = 0;
      try {
        yearExtracted = Integer.parseInt(year);
      } catch (NumberFormatException e) {
        yearExtracted = 0;
      }
      requestParameters.put("year", yearExtracted + 2 + "");
      try {
        final JSONObject response =
            this.h2RestService.performJsonPostRequest("/getSJR", requestParameters);
        if (response.getString("status").equals("SUCCESS")) {
          if (response.keySet().contains("data")) {
            if (response.get("data") != null) {
              if (!response.get("data").toString().equals("null")) {
                String referenceList = response.get("data").toString();

                ObjectMapper mapper = new ObjectMapper();
                SimpleModule module = new SimpleModule();
                module.addDeserializer(
                    ScimagoJournalRankingDTO.class, new ScimagoJournalRankingDTODeserializer());
                mapper.registerModule(module);
                ScimagoJournalRankingDTO references = null;
                try {
                  references = mapper.readValue(referenceList, ScimagoJournalRankingDTO.class);
                } catch (JsonProcessingException e) {
                  logger.debug(e.getMessage());
                }
                return references;
              }
            }
          }
        }
      } catch (Exception e) {
        logger.debug(e.getMessage());
        return null;
      }
    }
    return null;
  }

  private String resolveAbbreviation(String abbreviation) {
    if (!abbreviation.strip().equals("")) {
      JSONObject requestParameters = new JSONObject();
      requestParameters.put("abbreviation", abbreviation);
      try {
        final JSONObject response =
            this.localRestService.performJsonPostRequest(
                "/abbreviations/getJournalTitle", requestParameters);
        if (response.getString("status").equals("SUCCESS")) {
          if (response.keySet().contains("data")) {
            if (response.get("data") != null) {
              if (!response.get("data").toString().equals("null")) {
                Map<String, Object> referenceList = response.getJSONObject("data").toMap();
                if (referenceList.containsKey("journalName")) {
                  return referenceList.get("journalName").toString();
                }
              }
            }
          }
        }
      } catch (Exception e) {
        logger.debug(e.getMessage());
        return "";
      }
    }
    return "";
  }

  private JCRImpactFactor calculateJCRImpactFactor(String journalName, String year) {
    if (!journalName.strip().equals("")) {
      JSONObject requestParameters = new JSONObject();
      requestParameters.put("journal", journalName);
      int yearExtracted = 0;
      try {
        yearExtracted = Integer.parseInt(year);
      } catch (NumberFormatException e) {
        yearExtracted = 0;
      }
      requestParameters.put("year", yearExtracted + 2 + "");
      try {
        final JSONObject response =
            this.jcrRestService.performJsonPostRequest("/getValue", requestParameters);
        if (response.getString("status").equals("SUCCESS")) {
          if (response.keySet().contains("data")) {
            if (response.get("data") != null) {
              if (!response.get("data").toString().equals("null")) {
                String referenceList = response.get("data").toString();

                ObjectMapper mapper = new ObjectMapper();
                SimpleModule module = new SimpleModule();
                module.addDeserializer(JCRImpactFactor.class, new JCRImpactFactorDeserializer());
                mapper.registerModule(module);
                JCRImpactFactor references = null;
                try {
                  references = mapper.readValue(referenceList, JCRImpactFactor.class);
                } catch (JsonProcessingException e) {
                  logger.debug(e.getMessage());
                }
                return references;
              }
            }
          }
        }
      } catch (Exception e) {
        logger.debug(e.getMessage());
        return null;
      }
    }

    return null;
  }

  private double calculateMinLevenshteinRatio(List<Reference> references, String title) {
    double min = 2.0;
    for (Reference reference : references) {
      double distanceRatio =
          bibTexReferenceComparisonHelper.calculateLevenshteinDistanceRatio(
              title, reference.getTitleCorrect());
      if (distanceRatio < min) {
        min = distanceRatio;
      }
    }
    return min;
  }

  private List<Reference> identifyBestMatches(
      List<Reference> references, String title, List<String> authors, List<FilterLog> filterLogs) {
    double minLevenshtein = calculateMinLevenshteinRatio(references, title);
    List<Reference> filteredByLevenshtein =
        minLevenshteinDistanceFilter(references, title, minLevenshtein, filterLogs);
    if (filteredByLevenshtein.size() > 1) {
      double maxAuthorsMatchingRatio =
          calculateMaxAuthorsMatchingRatio(filteredByLevenshtein, authors);
      List<Reference> filteredByAuthors =
          maxAuthorsMatchingFilter(
              filteredByLevenshtein, authors, maxAuthorsMatchingRatio, filterLogs);
      return filteredByAuthors;
    } else {
      return filteredByLevenshtein;
    }
  }

  private List<Reference> maxAuthorsMatchingFilter(
      List<Reference> references,
      List<String> authors,
      double maxAuthorsMatchingRatio,
      List<FilterLog> filterLogs) {
    List<Reference> filteredList = new LinkedList<>();
    for (Reference reference : references) {
      double authorRatioCalculated =
          bibTexReferenceComparisonHelper.calculateAuthorRatio(reference.getSurnames(), authors);
      if (authorRatioCalculated == maxAuthorsMatchingRatio) {
        filteredList.add(reference);
      }
    }
    return filteredList;
  }

  private double calculateMaxAuthorsMatchingRatio(
      List<Reference> references, List<String> authors) {
    double max = -1.0;
    for (Reference reference : references) {
      double matchingRatio =
          bibTexReferenceComparisonHelper.calculateAuthorRatio(reference.getSurnames(), authors);
      if (matchingRatio > max) {
        max = matchingRatio;
      }
    }
    return max;
  }

  private List<Reference> minLevenshteinDistanceFilter(
      List<Reference> references, String title, double minLevenshtein, List<FilterLog> filterLogs) {
    List<Reference> filteredList = new LinkedList<>();
    for (Reference reference : references) {
      double levenshteinCalculated =
          bibTexReferenceComparisonHelper.calculateLevenshteinDistanceRatio(
              reference.getTitleCorrect(), title);
      if (levenshteinCalculated == minLevenshtein) {
        filteredList.add(reference);
      }
    }
    return filteredList;
  }

  private List<Reference> removeEntriesWithToManyNonAsciiSymbols(
      List<Reference> references, List<FilterLog> filterLogs) {
    List<Map<String, Object>> listInput =
        filterLogHelper.convertListOfReferencesToListOfMaps(references);
    List<Reference> filteredList = new LinkedList<>();
    for (Reference reference : references) {
      double ratio = extractRatioOfNonAsciiCharacters(reference.getJournal());
      if (ratio > 0.5) {
        ratio = extractRatioOfNonAsciiCharacters(reference.getTitleCorrect());
        if (ratio > 0.5) {
          ratio = extractRatioOfNonAsciiCharacters(reference.getAuthorsString(true));
          if (ratio > 0.5) {
            ratio = extractRatioOfNonAsciiCharacters(reference.getEditorsString(true));
            if (ratio > 0.5) {
              ratio = extractRatioOfNonAsciiCharacters(reference.getPublisher());
              if (ratio > 0.5) {
                ratio = extractRatioOfNonAsciiCharacters(reference.getVolume());
                if (ratio > 0.5) {
                  filteredList.add(reference);
                }
              }
            }
          }
        }
      }
    }
    List<Map<String, Object>> listOutput =
        filterLogHelper.convertListOfReferencesToListOfMaps(filteredList);
    int lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "ManyAsciiSymbolFilter",
            lastIndexInLog + 1,
            references.size(),
            filteredList.size(),
            listInput,
            listOutput);
    filterLogs.add(filterLog);
    return filteredList;
  }

  private double extractRatioOfNonAsciiCharacters(String text) {
    if (text.length() > 0) {
      return ((double) text.replaceAll("\\P{Print}", "").length()) / ((double) text.length());
    } else {
      return 1.0;
    }
  }

  private List<Reference> resolveLiteratureEntryArxivFiltered(
      String id,
      String doi,
      String arxivId,
      String title,
      List<String> authors,
      boolean useLocalDatabase,
      boolean useAPI,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    if (useLocalDatabase) {
      List<Reference> localResultsByArxivId =
          extractLocalResultsByArxivId(id, arxivId, bibTexPagePreprocessing, filterLogs);
      List<Reference> filteredListArxivIdLocal =
          arxivIdFilter(localResultsByArxivId, arxivId, filterLogs);
      filteredListArxivIdLocal = generalFilter(filteredListArxivIdLocal, filterLogs);
      filteredListArxivIdLocal = filtersForPreprintResolver(filteredListArxivIdLocal, filterLogs);
      List<Reference> arxivResultsLocalArxivId =
          identifyDirectArxivReferences(filteredListArxivIdLocal);
      List<Map<String, Object>> listInput =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListArxivIdLocal);
      filteredListArxivIdLocal.removeAll(arxivResultsLocalArxivId);
      List<Map<String, Object>> listOutput =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListArxivIdLocal);
      int lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
      FilterLog filterLog =
          new FilterLog(
              "RemoveArxivReferences",
              lastIndexInLog + 1,
              filteredListArxivIdLocal.size() + arxivResultsLocalArxivId.size(),
              filteredListArxivIdLocal.size(),
              listInput,
              listOutput);
      filterLogs.add(filterLog);
      if (filteredListArxivIdLocal.size() > 0) {
        return filteredListArxivIdLocal;
      }
      List<Reference> localResultsByDOI =
          extractLocalResultsByDOI(id, doi, bibTexPagePreprocessing, filterLogs);
      List<Reference> filteredListDOILocal = doiFilter(localResultsByDOI, doi, filterLogs);
      filteredListDOILocal = generalFilter(filteredListDOILocal, filterLogs);
      filteredListDOILocal = filtersForPreprintResolver(filteredListDOILocal, filterLogs);
      List<Reference> arxivResultsLocalDoi = identifyDirectArxivReferences(filteredListDOILocal);
      List<Map<String, Object>> listInputDOI =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListDOILocal);
      filteredListDOILocal.removeAll(arxivResultsLocalDoi);
      List<Map<String, Object>> listOutputDOI =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListDOILocal);
      int lastIndexInLogDOI = filterLogHelper.getLastIndexInLog(filterLogs);
      FilterLog filterLogDOI =
          new FilterLog(
              "RemoveArxivReferences",
              lastIndexInLogDOI + 1,
              filteredListDOILocal.size() + arxivResultsLocalDoi.size(),
              filteredListDOILocal.size(),
              listInputDOI,
              listOutputDOI);
      filterLogs.add(filterLogDOI);
      if (filteredListDOILocal.size() > 0) {
        return filteredListDOILocal;
      }
      List<Reference> localResultsByTitle =
          extractLocalResultsByTitle(id, title, bibTexPagePreprocessing, filterLogs);
      List<Reference> filteredListTitleLocal = titleFilter(localResultsByTitle, title, filterLogs);
      List<Reference> filteredListAuthorLocal =
          authorFilter(filteredListTitleLocal, authors, filterLogs);
      filteredListTitleLocal = generalFilter(filteredListAuthorLocal, filterLogs);
      filteredListTitleLocal = filtersForPreprintResolver(filteredListTitleLocal, filterLogs);
      List<Reference> arxivResultsLocalTitle =
          identifyDirectArxivReferences(filteredListTitleLocal);
      List<Map<String, Object>> listInputTitle =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListTitleLocal);
      filteredListTitleLocal.removeAll(arxivResultsLocalTitle);
      List<Map<String, Object>> listOutputTitle =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListTitleLocal);
      int lastIndexInLogTitle = filterLogHelper.getLastIndexInLog(filterLogs);
      FilterLog filterLogTitle =
          new FilterLog(
              "RemoveArxivReferences",
              lastIndexInLogTitle + 1,
              filteredListTitleLocal.size() + arxivResultsLocalTitle.size(),
              filteredListTitleLocal.size(),
              listInputTitle,
              listOutputTitle);
      filterLogs.add(filterLogTitle);
      if (filteredListTitleLocal.size() > 0) {
        return filteredListTitleLocal;
      }
    }
    if (useAPI) {
      List<Reference> localResultsByArxivId =
          extractAPIResultsByArxivId(id, arxivId, bibTexPagePreprocessing, filterLogs);
      List<Reference> filteredListArxivIdAPI =
          arxivIdFilter(localResultsByArxivId, arxivId, filterLogs);
      filteredListArxivIdAPI = generalFilter(filteredListArxivIdAPI, filterLogs);
      filteredListArxivIdAPI = filtersForPreprintResolver(filteredListArxivIdAPI, filterLogs);
      List<Reference> arxivResultsAPIArxivId =
          identifyDirectArxivReferences(filteredListArxivIdAPI);
      List<Map<String, Object>> listInput =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListArxivIdAPI);
      filteredListArxivIdAPI.removeAll(arxivResultsAPIArxivId);
      List<Map<String, Object>> listOutput =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListArxivIdAPI);
      int lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
      FilterLog filterLog =
          new FilterLog(
              "RemoveArxivReferences",
              lastIndexInLog + 1,
              filteredListArxivIdAPI.size() + arxivResultsAPIArxivId.size(),
              filteredListArxivIdAPI.size(),
              listInput,
              listOutput);
      filterLogs.add(filterLog);
      if (filteredListArxivIdAPI.size() > 0) {
        return filteredListArxivIdAPI;
      }
      List<Reference> localResultsByDOI =
          extractAPIResultsByDOI(id, doi, bibTexPagePreprocessing, filterLogs);
      List<Reference> filteredListDoiAPI = doiFilter(localResultsByDOI, doi, filterLogs);
      filteredListDoiAPI = generalFilter(filteredListDoiAPI, filterLogs);
      filteredListDoiAPI = filtersForPreprintResolver(filteredListDoiAPI, filterLogs);
      List<Reference> arxivResultsAPIDoi = identifyDirectArxivReferences(filteredListDoiAPI);
      List<Map<String, Object>> listInputDOI =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListDoiAPI);
      filteredListDoiAPI.removeAll(arxivResultsAPIDoi);
      List<Map<String, Object>> listOutputDOI =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListArxivIdAPI);
      int lastIndexInLogDOI = filterLogHelper.getLastIndexInLog(filterLogs);
      FilterLog filterLogDOI =
          new FilterLog(
              "RemoveArxivReferences",
              lastIndexInLogDOI + 1,
              filteredListDoiAPI.size() + arxivResultsAPIDoi.size(),
              filteredListDoiAPI.size(),
              listInputDOI,
              listOutputDOI);
      filterLogs.add(filterLogDOI);
      if (filteredListDoiAPI.size() > 0) {
        return filteredListDoiAPI;
      }
      List<Reference> localResultsByTitle =
          extractAPIResultsByTitle(id, title, bibTexPagePreprocessing, filterLogs);
      List<Reference> filteredListTitleAPI = titleFilter(localResultsByTitle, title, filterLogs);
      List<Reference> filteredListAuthorAPI =
          authorFilter(filteredListTitleAPI, authors, filterLogs);
      filteredListTitleAPI = generalFilter(filteredListAuthorAPI, filterLogs);
      filteredListTitleAPI = filtersForPreprintResolver(filteredListTitleAPI, filterLogs);
      List<Reference> arxivResultsAPITitle = identifyDirectArxivReferences(filteredListTitleAPI);
      List<Map<String, Object>> listInputTitle =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListTitleAPI);
      filteredListTitleAPI.removeAll(arxivResultsAPITitle);
      List<Map<String, Object>> listOutputTitle =
          filterLogHelper.convertListOfReferencesToListOfMaps(filteredListArxivIdAPI);
      int lastIndexInLogTitle = filterLogHelper.getLastIndexInLog(filterLogs);
      FilterLog filterLogTitle =
          new FilterLog(
              "RemoveArxivReferences",
              lastIndexInLogTitle + 1,
              filteredListArxivIdAPI.size() + arxivResultsAPITitle.size(),
              filteredListArxivIdAPI.size(),
              listInputTitle,
              listOutputTitle);
      filterLogs.add(filterLogTitle);
      if (filteredListTitleAPI.size() > 0) {
        return filteredListTitleAPI;
      }
    }
    return new LinkedList<>();
  }

  private List<Reference> filtersForPreprintResolver(
      List<Reference> references, List<FilterLog> filterLogs) {
    List<Reference> results = noJournalOrBooktitleFilter(references, filterLogs);
    results = noOrMiscPublicationTypeFilter(results, filterLogs);
    return results;
  }

  private List<Reference> noJournalOrBooktitleFilter(
      List<Reference> references, List<FilterLog> filterLogs) {
    List<Map<String, Object>> listInput =
        filterLogHelper.convertListOfReferencesToListOfMaps(references);
    List<Reference> results = new LinkedList<>();
    for (Reference reference : references) {
      if (!reference.getJournal().strip().equals("")
          || !reference.getBooktitle().strip().equals("")) {
        results.add(reference);
      }
    }
    List<Map<String, Object>> listOutput =
        filterLogHelper.convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "EntryWithoutJournalTitleOrBooktitleFilter",
            lastIndexInLog + 1,
            references.size(),
            results.size(),
            listInput,
            listOutput);
    filterLogs.add(filterLog);

    return results;
  }

  private List<Reference> noOrMiscPublicationTypeFilter(
      List<Reference> references, List<FilterLog> filterLogs) {
    List<Map<String, Object>> listInput =
        filterLogHelper.convertListOfReferencesToListOfMaps(references);

    List<Reference> results = new LinkedList<>();
    for (Reference reference : references) {
      if (!(reference.getPublicationType().strip().equals("")
          || reference.getPublicationType().equalsIgnoreCase("misc"))) {
        results.add(reference);
      }
    }
    List<Map<String, Object>> listOutput =
        filterLogHelper.convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "EntryWithoutOrWithMiscPublicationTypeFilter",
            lastIndexInLog + 1,
            references.size(),
            results.size(),
            listInput,
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  public int getNumberOfCitations(
      String doi, String arxivId, String title, List<String> authors, List<FilterLog> filterLogs) {
    List<Reference> references =
        this.resolveLiteratureEntry(
            "",
            doi,
            arxivId,
            title,
            authors,
            false,
            true,
            BibTexPagePreprocessing.NO_PREPROCESSING,
            filterLogs);
    if (references.size() > 0) {
      return references.get(0).getCitationCount();
    }
    return -1;
  }

  public int getNumberOfHighlyInfluentialCitations(
      String doi, String arxivId, String title, List<String> authors, List<FilterLog> filterLogs) {
    List<Reference> references =
        this.resolveLiteratureEntry(
            "",
            doi,
            arxivId,
            title,
            authors,
            false,
            true,
            BibTexPagePreprocessing.NO_PREPROCESSING,
            filterLogs);
    if (references.size() > 0) {
      return references.get(0).getHighlyInfluentialCitationCount();
    }
    return -1;
  }

  private List<Reference> generalFilter(List<Reference> references, List<FilterLog> filterLogs) {
    List<Reference> filteredList;
    filteredList = removeEntriesWithToManyNonAsciiSymbols(references, filterLogs);
    filteredList = removeRetractedEntries(filteredList, filterLogs);
    return filteredList;
  }

  private List<Reference> removeRetractedEntries(
      List<Reference> references, List<FilterLog> filterLogs) {
    List<Map<String, Object>> listBefore =
        filterLogHelper.convertListOfReferencesToListOfMaps(references);
    List<Reference> filteredList = new LinkedList<>();
    for (Reference reference : references) {
      if (reference.getRetracted() == null || reference.getRetracted() == false) {
        filteredList.add(reference);
      }
    }
    List<Map<String, Object>> listAfter =
        filterLogHelper.convertListOfReferencesToListOfMaps(filteredList);
    int lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "retractedEntryFilter",
            lastIndexInLog + 1,
            references.size(),
            filteredList.size(),
            listBefore,
            listAfter);
    filterLogs.add(filterLog);
    return filteredList;
  }

  protected abstract List<Reference> extractAPIResultsByDOI(
      String id,
      String doi,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs);

  protected abstract List<Reference> extractAPIResultsByTitle(
      String id,
      String title,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs);

  protected abstract List<Reference> extractAPIResultsByArxivId(
      String id,
      String arxivId,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs);

  protected abstract List<Reference> extractLocalResultsByTitle(
      String id,
      String title,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs);

  protected abstract List<Reference> extractLocalResultsByDOI(
      String id,
      String doi,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs);

  protected abstract List<Reference> extractLocalResultsByArxivId(
      String id,
      String arxivId,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs);

  private List<Reference> doiFilter(
      List<Reference> references, String doi, List<FilterLog> filterLogs) {
    if (doi == null || bibTexReferenceComparisonHelper.preprocessDOI(doi).equals("")) {
      return new LinkedList<>();
    }
    List<Map<String, Object>> listInput =
        filterLogHelper.convertListOfReferencesToListOfMaps(references);
    List<Reference> filteredList = new LinkedList<>();
    for (Reference reference : references) {
      if (bibTexReferenceComparisonHelper
          .preprocessDOI(reference.getDoi())
          .equals(bibTexReferenceComparisonHelper.preprocessDOI(doi))) {
        filteredList.add(reference);
      }
    }
    List<Map<String, Object>> listOutput =
        filterLogHelper.convertListOfReferencesToListOfMaps(filteredList);
    int lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "NonMatchingDOIFilter",
            lastIndexInLog + 1,
            references.size(),
            filteredList.size(),
            listInput,
            listOutput);
    filterLogs.add(filterLog);
    return filteredList;
  }

  private List<Reference> arxivIdFilter(
      List<Reference> references, String arxivId, List<FilterLog> filterLogs) {
    List<Map<String, Object>> inputReferences =
        filterLogHelper.convertListOfReferencesToListOfMaps(references);
    List<Reference> filteredList = new LinkedList<>();
    if (arxivId == null || arxivHelper.preprocessArxiv(arxivId).equals("")) {
      return new LinkedList<>();
    }
    for (Reference reference : references) {
      if (arxivHelper
          .preprocessArxiv(arxivId)
          .equals(arxivHelper.preprocessArxiv(reference.getLinkedArxivId()))) {
        filteredList.add(reference);
      } else {
        if (arxivHelper.preprocessArxiv(reference.getLinkedArxivId()).equals("")) {
          filteredList.add(reference);
        }
      }
    }
    int lastIndexInLog = filterLogHelper.getLastIndexInLog(filterLogs);
    List<Map<String, Object>> outputReferences =
        filterLogHelper.convertListOfReferencesToListOfMaps(filteredList);

    FilterLog filterLog =
        new FilterLog(
            "ArxivIdFilter",
            lastIndexInLog + 1,
            references.size(),
            filteredList.size(),
            inputReferences,
            outputReferences);
    filterLogs.add(filterLog);
    return filteredList;
  }

  private List<Reference> titleFilter(
      List<Reference> references, String title, List<FilterLog> filterLogs) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    return bibTexReferenceComparisonHelper.filterMatchesByTitle(references, title, filterLogs);
  }

  private List<Reference> authorFilter(
      List<Reference> references, List<String> authors, List<FilterLog> filterLogs) {
    return bibTexReferenceComparisonHelper.filterMatchesByAuthors(references, authors, filterLogs);
  }

  private List<Reference> identifyDirectArxivReferences(List<Reference> localResultsByArxivId) {
    List<Reference> filteredList =
        bibTexHelperService.extractArxivReferencesFromList(localResultsByArxivId, arxivHelper);
    return filteredList;
  }

  public FilterLogHelper getFilterLogHelper() {
    return filterLogHelper;
  }
}
