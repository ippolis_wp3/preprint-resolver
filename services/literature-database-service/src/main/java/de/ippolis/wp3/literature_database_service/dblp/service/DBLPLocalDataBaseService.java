package de.ippolis.wp3.literature_database_service.dblp.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.ippolis.wp3.literature_database_service.openalex.service.OpenAlexLocalDatabaseService;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.model.ReferenceDeserializer;
import de.ippolis.wp3.literature_database_service.utils.service.LocalRestService;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DBLPLocalDataBaseService {

  private final LocalRestService localRestService;
  private final Logger logger = LoggerFactory.getLogger(OpenAlexLocalDatabaseService.class);

  @Autowired
  public DBLPLocalDataBaseService(LocalRestService localRestService) {
    this.localRestService = localRestService;
  }

  public List<Reference> getByTitle(
      String title, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    JSONObject requestParameters = new JSONObject();
    requestParameters.put("id", id);
    requestParameters.put("title", title);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);
    try {
      final JSONObject response =
          this.localRestService.performJsonPostRequest(
              "/dblp/getLocalReferencesByTitle", requestParameters);
      if (response.getString("status").equals("SUCCESS")) {
        if (response.keySet().contains("data")) {
          String referenceList = response.get("data").toString();

          ObjectMapper mapper = new ObjectMapper();
          SimpleModule module = new SimpleModule();
          module.addDeserializer(Reference.class, new ReferenceDeserializer());
          mapper.registerModule(module);
          try {

            Reference[] references = mapper.readValue(referenceList, Reference[].class);
            return List.of(references);
          } catch (JsonMappingException e) {
            logger.debug(e.getMessage());
          } catch (JsonProcessingException e) {
            logger.debug(e.getMessage());
          }
        }
      }
      return new LinkedList<>();
    } catch (Exception e) {
      logger.debug(e.getMessage());
      return new LinkedList<>();
    }
  }

  public List<Reference> getByArxivId(
      String arxivId, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (arxivId == null || arxivId.strip().equals("")) {
      return new LinkedList<>();
    }
    JSONObject requestParameters = new JSONObject();
    requestParameters.put("id", id);
    requestParameters.put("arxivId", arxivId);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);
    try {
      final JSONObject response =
          this.localRestService.performJsonPostRequest(
              "/dblp/getLocalReferencesByArxivId", requestParameters);
      if (response.getString("status").equals("SUCCESS")) {
        if (response.keySet().contains("data")) {
          String referenceList = response.get("data").toString();

          ObjectMapper mapper = new ObjectMapper();
          SimpleModule module = new SimpleModule();
          module.addDeserializer(Reference.class, new ReferenceDeserializer());
          mapper.registerModule(module);
          try {

            Reference[] references = mapper.readValue(referenceList, Reference[].class);
            return List.of(references);
          } catch (JsonMappingException e) {
            logger.debug(e.getMessage());
          } catch (JsonProcessingException e) {
            logger.debug(e.getMessage());
          }
        }
      }
      return new LinkedList<>();
    } catch (Exception e) {
      logger.debug(e.getMessage());
      return new LinkedList<>();
    }
  }

  public List<Reference> getByDOI(
      String doi, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (doi == null || doi.strip().equals("")) {
      return new LinkedList<>();
    }
    JSONObject requestParameters = new JSONObject();
    requestParameters.put("id", id);
    requestParameters.put("doi", doi);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);
    try {
      final JSONObject response =
          this.localRestService.performJsonPostRequest(
              "/dblp/getLocalReferencesByDOI", requestParameters);
      if (response.getString("status").equals("SUCCESS")) {
        if (response.keySet().contains("data")) {
          String referenceList = response.get("data").toString();

          ObjectMapper mapper = new ObjectMapper();
          SimpleModule module = new SimpleModule();
          module.addDeserializer(Reference.class, new ReferenceDeserializer());
          mapper.registerModule(module);
          try {

            Reference[] references = mapper.readValue(referenceList, Reference[].class);
            return List.of(references);
          } catch (JsonMappingException e) {
            logger.debug(e.getMessage());
          } catch (JsonProcessingException e) {
            logger.debug(e.getMessage());
          }
        }
      }
      return new LinkedList<>();
    } catch (Exception e) {
      logger.debug(e.getMessage());
      return new LinkedList<>();
    }
  }
}
