package de.ippolis.wp3.literature_database_service.utils.model;

public class ScimagoJournalRankingDTO {

  private final String journalTitle;
  private int rank;
  private String type;
  private double sjr;
  private String sjrBestQuartile;
  private int hIndex;
  private int totalDocs;
  private int totalDocs3years;
  private int totalRefs;
  private int totalCites3years;
  private int citableDocs3Years;
  private double citesDocs2years;
  private double refsPerDoc;
  private String categories;
  private String scimagoYear;
  private String areas;

  public ScimagoJournalRankingDTO(String title) {
    this.journalTitle = title;
  }

  public String getAreas() {
    return areas;
  }

  public void setAreas(String areas) {
    this.areas = areas;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public String getJournalTitle() {
    return journalTitle;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public double getSjr() {
    return sjr;
  }

  public void setSjr(double sjr) {
    this.sjr = sjr;
  }

  public String getSjrBestQuartile() {
    return sjrBestQuartile;
  }

  public void setSjrBestQuartile(String sjrBestQuartile) {
    this.sjrBestQuartile = sjrBestQuartile;
  }

  public int gethIndex() {
    return hIndex;
  }

  public void sethIndex(int hIndex) {
    this.hIndex = hIndex;
  }

  public int getTotalDocs() {
    return totalDocs;
  }

  public void setTotalDocs(int totalDocs) {
    this.totalDocs = totalDocs;
  }

  public int getTotalDocs3years() {
    return totalDocs3years;
  }

  public void setTotalDocs3years(int totalDocs3years) {
    this.totalDocs3years = totalDocs3years;
  }

  public int getTotalRefs() {
    return totalRefs;
  }

  public void setTotalRefs(int totalRefs) {
    this.totalRefs = totalRefs;
  }

  public int getTotalCites3years() {
    return totalCites3years;
  }

  public void setTotalCites3years(int totalCites3years) {
    this.totalCites3years = totalCites3years;
  }

  public int getCitableDocs3Years() {
    return citableDocs3Years;
  }

  public void setCitableDocs3Years(int citableDocs3Years) {
    this.citableDocs3Years = citableDocs3Years;
  }

  public double getCitesDocs2years() {
    return citesDocs2years;
  }

  public void setCitesDocs2years(double citesDocs2years) {
    this.citesDocs2years = citesDocs2years;
  }

  public double getRefsPerDoc() {
    return refsPerDoc;
  }

  public void setRefsPerDoc(double refsPerDoc) {
    this.refsPerDoc = refsPerDoc;
  }

  public String getCategories() {
    return categories;
  }

  public void setCategories(String categories) {
    this.categories = categories;
  }

  public String getScimagoYear() {
    return scimagoYear;
  }

  public void setScimagoYear(String scimagoYear) {
    this.scimagoYear = scimagoYear;
  }
}
