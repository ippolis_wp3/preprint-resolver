package de.ippolis.wp3.literature_database_service.dblp.service;

import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLog;
import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLogHelper;
import de.ippolis.wp3.literature_database_service.utils.helper.BibTexHelperService;
import de.ippolis.wp3.literature_database_service.utils.model.AbstractLiteratureAnalyzer;
import de.ippolis.wp3.literature_database_service.utils.model.ArxivHelper;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexReferenceComparisonHelper;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.service.H2RestService;
import de.ippolis.wp3.literature_database_service.utils.service.JCRRestService;
import de.ippolis.wp3.literature_database_service.utils.service.LocalRestService;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DBLPLiteratureAnalyzer extends AbstractLiteratureAnalyzer {

  private final DBLPLocalDataBaseService dblpLocalDataBaseService;
  private final DBLPAPIReferenceService dblpapiReferenceService;

  @Autowired
  public DBLPLiteratureAnalyzer(
      ArxivHelper arxivHelper,
      BibTexHelperService bibTexHelperService,
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper,
      DBLPLocalDataBaseService dblpLocalDataBaseService,
      DBLPAPIReferenceService dblpapiReferenceService,
      JCRRestService jcrRestService,
      H2RestService h2RestService,
      LocalRestService localRestService,
      FilterLogHelper filterLogHelper) {
    super(
        arxivHelper,
        bibTexHelperService,
        bibTexReferenceComparisonHelper,
        jcrRestService,
        h2RestService,
        localRestService,
        filterLogHelper);
    this.dblpLocalDataBaseService = dblpLocalDataBaseService;
    this.dblpapiReferenceService = dblpapiReferenceService;
  }

  @Override
  protected List<Reference> extractAPIResultsByDOI(
      String id,
      String doi,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> results = new LinkedList<>();
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "DBLPAPIDOIExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractAPIResultsByTitle(
      String id,
      String title,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> results;
    if (title == null || title.strip().equals("")) {
      results = new LinkedList<>();
    }
    results = dblpapiReferenceService.getDBLPReferencesByTitle(title, bibTexPagePreprocessing, id);
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "DBLPAPITitleExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractAPIResultsByArxivId(
      String id,
      String arxivId,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> results = new LinkedList<>();
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "DBLPAPIArxivIdExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractLocalResultsByTitle(
      String id,
      String title,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> results;
    if (title == null || title.strip().equals("")) {
      results = new LinkedList<>();
    } else {
      results = dblpLocalDataBaseService.getByTitle(title, id, bibTexPagePreprocessing);
    }
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "DBLPLocalTitleExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractLocalResultsByDOI(
      String id,
      String doi,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> results;
    if (doi == null || doi.strip().equals("")) {
      results = new LinkedList<>();
    } else {
      results = dblpLocalDataBaseService.getByDOI(doi, id, bibTexPagePreprocessing);
    }
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "DBLPLocalDOIExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractLocalResultsByArxivId(
      String id,
      String arxivId,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    List<Reference> results;
    if (arxivId == null || arxivId.strip().equals("")) {
      results = new LinkedList<>();
    }
    results = dblpLocalDataBaseService.getByArxivId(arxivId, id, bibTexPagePreprocessing);
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "DBLPLocalArxivIdExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }
}
