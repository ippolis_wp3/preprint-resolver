package de.ippolis.wp3.literature_database_service.utils.service;

import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class H2RestService {
  private final RestTemplate restTemplate;

  private final boolean eurekaEnabled;

  private final String h2ServiceHostname;

  @Autowired
  public H2RestService(
      RestTemplate restTemplate,
      @Value("${eureka.client.enabled}") boolean eurekaEnabled,
      @Value("${ippolis_wp3.h2-database-service.hostname}") String h2ServiceHostname) {
    this.restTemplate = restTemplate;
    this.eurekaEnabled = eurekaEnabled;
    this.h2ServiceHostname = h2ServiceHostname;
  }

  /**
   * Performs a JSON POST request to the given URL with the given parameters, trying to deserialize
   * the result into the given response type.
   */
  public <T> T performJsonPostRequest(
      String requestUri, JSONObject requestParameters, Class<T> responseType) {

    return this.performJsonPostRequest(
        requestUri, requestParameters, responseType, new HttpHeaders());
  }

  public JSONObject performJsonGetRequest(String requestUri) {
    return this.performJsonGetRequest(requestUri, Map.of());
  }

  public JSONObject performJsonGetRequest(String requestUri, Map<String, ?> uriParameters) {
    return new JSONObject(this.performJsonGetRequest(requestUri, uriParameters, String.class));
  }

  public <T> T performJsonGetRequest(
      String requestUri, Map<String, ?> uriParameters, Class<T> responseType) {
    final HttpHeaders headers = new HttpHeaders();

    return this.performJsonGetRequest(requestUri, uriParameters, responseType, headers);
  }

  public <T> T performJsonGetRequest(
      String requestUri, Map<String, ?> uriParameters, Class<T> responseType, HttpHeaders headers) {

    HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

    if (headers.getAccept().isEmpty()) {
      headers.setAccept(List.of(MediaType.APPLICATION_JSON));
    }

    RestTemplate currentRestTemplate = this.restTemplate;

    if (!this.eurekaEnabled) {
      currentRestTemplate = new RestTemplate();
      requestUri = this.h2ServiceHostname + requestUri;
    } else {
      requestUri = "h2-database-service" + requestUri;
    }

    requestUri = "http://" + requestUri;

    return currentRestTemplate
        .exchange(requestUri, HttpMethod.GET, requestEntity, responseType, uriParameters)
        .getBody();
  }

  public <T> T performJsonPostRequest(
      String requestUri, JSONObject requestParameters, Class<T> responseType, HttpHeaders headers) {

    HttpEntity<String> request = this.prepareJsonRequest(requestParameters, headers);

    RestTemplate currentRestTemplate = this.restTemplate;

    if (!this.eurekaEnabled) {
      currentRestTemplate = new RestTemplate();
      requestUri = this.h2ServiceHostname + requestUri;
    } else {
      requestUri = "h2-database-service" + requestUri;
    }

    requestUri = "http://" + requestUri;

    return currentRestTemplate.postForObject(requestUri, request, responseType);
  }

  /** TODO merge with above, using custom header */
  public ResponseEntity<String> performExternalPostRequest(
      String requestUri, MultiValueMap<String, String> requestParameters, HttpHeaders headers) {

    HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(requestParameters, headers);

    return new RestTemplate().exchange(requestUri, HttpMethod.POST, entity, String.class);
  }

  /**
   * Performs a JSON POST request to the given URL with the given parameters, converting the result
   * into a {@link JSONObject}.
   */
  public JSONObject performJsonPostRequest(String requestUri, JSONObject requestParameters) {
    return this.performJsonPostRequest(requestUri, requestParameters, new HttpHeaders());
  }

  public JSONObject performJsonPostRequest(
      String requestUri, JSONObject requestParameters, HttpHeaders headers) {
    String responseString =
        this.performJsonPostRequest(requestUri, requestParameters, String.class, headers);

    return new JSONObject(responseString);
  }

  private HttpEntity<String> prepareJsonRequest(JSONObject requestParameters, HttpHeaders headers) {
    if (headers.getContentType() == null) {
      headers.setContentType(MediaType.APPLICATION_JSON);
    }

    return new HttpEntity<>(requestParameters.toString(), headers);
  }
}
