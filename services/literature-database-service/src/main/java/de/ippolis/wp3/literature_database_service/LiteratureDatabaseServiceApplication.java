package de.ippolis.wp3.literature_database_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * This is the application entry point. Change the class name as necessary.
 *
 * <p>Usually there is no reason to add any other code in this file.
 */
@SpringBootApplication
public class LiteratureDatabaseServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(LiteratureDatabaseServiceApplication.class, args);
  }

  @Bean("rawRestTemplate")
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }
}
