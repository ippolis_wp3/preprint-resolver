package de.ippolis.wp3.literature_database_service.utils.model;

public enum BibTexPagePreprocessing {
  DOUBLE_MINUS_WITH_SPACES,
  DOUBLE_MINUS_WITHOUT_SPACES,
  SINGLE_MINUS_WITH_SPACES,
  SINGLE_MINUS_WITHOUT_SPACES,
  NO_PREPROCESSING
}
