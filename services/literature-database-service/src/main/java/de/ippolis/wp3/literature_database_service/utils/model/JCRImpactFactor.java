package de.ippolis.wp3.literature_database_service.utils.model;

public class JCRImpactFactor {

  private String journal;
  private double impactFactor;
  private int year;

  public JCRImpactFactor() {}

  public JCRImpactFactor(String journal, double impactFactor, int year) {
    this.journal = journal;
    this.impactFactor = impactFactor;
    this.year = year;
  }

  public String getJournal() {
    return journal;
  }

  public void setJournal(String journal) {
    this.journal = journal;
  }

  public double getImpactFactor() {
    return impactFactor;
  }

  public void setImpactFactor(double impactFactor) {
    this.impactFactor = impactFactor;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }
}
