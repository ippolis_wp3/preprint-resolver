package de.ippolis.wp3.literature_database_service.utils.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;

public class JCRImpactFactorDeserializer extends StdDeserializer<JCRImpactFactor> {
  public JCRImpactFactorDeserializer() {
    this(null);
  }

  public JCRImpactFactorDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public JCRImpactFactor deserialize(
      JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
    JCRImpactFactor jcrImpactFactorDTO = new JCRImpactFactor();
    JsonNode node = jsonParser.getCodec().readTree(jsonParser);
    jcrImpactFactorDTO.setYear(node.get("year").asInt());
    jcrImpactFactorDTO.setImpactFactor(node.get("impactFactor").asDouble());
    jcrImpactFactorDTO.setJournal(node.get("journal").asText());
    return jcrImpactFactorDTO;
  }
}
