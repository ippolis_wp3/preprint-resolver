package de.ippolis.wp3.literature_database_service.filter_log.model;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FilterLog {
  private String filterName;
  private int index;
  private int inputN;
  private int outputN;
  private List<Map<String, Object>> inputReferences;
  private List<Map<String, Object>> outputReferences;

  public FilterLog(
      String filterName,
      int index,
      int inputN,
      int outputN,
      List<Map<String, Object>> inputReferences,
      List<Map<String, Object>> outputReferences) {
    this.filterName = filterName;
    this.index = index;
    this.inputN = inputN;
    this.outputN = outputN;
    this.inputReferences = inputReferences;
    this.outputReferences = outputReferences;
  }

  public String getFilterName() {
    return filterName;
  }

  public void setFilterName(String filterName) {
    this.filterName = filterName;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public int getInputN() {
    return inputN;
  }

  public void setInputN(int inputN) {
    this.inputN = inputN;
  }

  public int getOutputN() {
    return outputN;
  }

  public void setOutputN(int outputN) {
    this.outputN = outputN;
  }

  public void addOutputReference(Map<String, Object> outputReference) {
    if (outputReference != null) {
      if (outputReferences == null) {
        outputReferences = new LinkedList<>();
      }
      outputReferences.add(outputReference);
    }
  }

  public List<Map<String, Object>> getOutputReferences() {
    return outputReferences;
  }

  public void addInputReference(Map<String, Object> inputReference) {
    if (inputReference != null) {
      if (inputReferences == null) {
        inputReferences = new LinkedList<>();
      }
      inputReferences.add(inputReference);
    }
  }

  public List<Map<String, Object>> getInputReferences() {
    return inputReferences;
  }

  public Map<String, Object> toMap() {
    Map<String, Object> map = new LinkedHashMap<>();
    map.put("filterName", filterName);
    map.put("index", index);
    map.put("inputN", inputN);
    map.put("outputN", outputN);
    map.put("inputReferences", inputReferences);
    map.put("outputReferences", outputReferences);
    return map;
  }
}
