package de.ippolis.wp3.literature_database_service.filter_log.model;

import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class FilterLogHelper {
  public int getLastIndexInLog(List<FilterLog> filterLogs) {
    int i = 0;
    for (FilterLog filterLog : filterLogs) {
      if (filterLog.getIndex() > i) {
        i = filterLog.getIndex();
      }
    }
    return i;
  }

  public List<Map<String, Object>> convertListOfReferencesToListOfMaps(List<Reference> list) {
    List<Map<String, Object>> resultsList = new LinkedList<>();
    for (Reference reference : list) {
      resultsList.add(reference.toSimpleMap(true));
    }
    return resultsList;
  }

  public List<Map<String, Object>> convertListOfFilterLogsToListOfMaps(List<FilterLog> filterLogs) {
    List<Map<String, Object>> resultsList = new LinkedList<>();
    for (FilterLog filterLog : filterLogs) {
      resultsList.add(filterLog.toMap());
    }
    return resultsList;
  }
}
