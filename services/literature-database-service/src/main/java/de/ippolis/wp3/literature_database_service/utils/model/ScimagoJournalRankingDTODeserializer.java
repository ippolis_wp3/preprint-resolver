package de.ippolis.wp3.literature_database_service.utils.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;

public class ScimagoJournalRankingDTODeserializer
    extends StdDeserializer<ScimagoJournalRankingDTO> {
  public ScimagoJournalRankingDTODeserializer() {
    this(null);
  }

  public ScimagoJournalRankingDTODeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public ScimagoJournalRankingDTO deserialize(
      JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {

    JsonNode node = jsonParser.getCodec().readTree(jsonParser);
    if (node.get("journalTitle") == null) {
      return null;
    }
    ScimagoJournalRankingDTO scimagoJournalRankingDTO =
        new ScimagoJournalRankingDTO(node.get("journalTitle").asText());
    if (node.get("rank") != null) {
      scimagoJournalRankingDTO.setRank(node.get("rank").asInt());
    }
    if (node.get("type") != null) {
      scimagoJournalRankingDTO.setType(node.get("type").asText());
    }
    if (node.get("sjr") != null) {
      scimagoJournalRankingDTO.setSjr(node.get("sjr").asDouble());
    }
    if (node.get("sjrBestQuartile") != null) {
      scimagoJournalRankingDTO.setSjrBestQuartile(node.get("sjrBestQuartile").asText());
    }
    if (node.get("hIndex") != null) {
      scimagoJournalRankingDTO.sethIndex(node.get("hIndex").asInt());
    }
    if (node.get("totalDocs2020") != null) {
      scimagoJournalRankingDTO.setTotalDocs(node.get("totalDocs2020").asInt());
    }
    if (node.get("totalDocs3years") != null) {
      scimagoJournalRankingDTO.setTotalDocs3years(node.get("totalDocs3years").asInt());
    }
    if (node.get("totalRefs") != null) {
      scimagoJournalRankingDTO.setTotalRefs(node.get("totalRefs").asInt());
    }
    if (node.get("totalCites3years") != null) {
      scimagoJournalRankingDTO.setTotalCites3years(node.get("totalCites3years").asInt());
    }
    if (node.get("citableDocs3Years") != null) {
      scimagoJournalRankingDTO.setCitableDocs3Years(node.get("citableDocs3Years").asInt());
    }
    if (node.get("citesDocs2years") != null) {
      scimagoJournalRankingDTO.setCitesDocs2years(node.get("citesDocs2years").asInt());
    }
    if (node.get("refsPerDoc") != null) {
      scimagoJournalRankingDTO.setRefsPerDoc(node.get("refsPerDoc").asDouble());
    }
    if (node.get("categories") != null) {
      scimagoJournalRankingDTO.setCategories(node.get("categories").asText());
    }
    if (node.get("scimagoYear") != null) {
      scimagoJournalRankingDTO.setScimagoYear(node.get("scimagoYear").asText());
    }
    if (node.get("areas") != null) {
      scimagoJournalRankingDTO.setAreas(node.get("areas").asText());
    }
    return scimagoJournalRankingDTO;
  }
}
