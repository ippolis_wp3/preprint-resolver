package de.ippolis.wp3.literature_database_service.utils.helper;

import de.ippolis.wp3.literature_database_service.utils.service.LocalRestService;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AbbreviationHelperService {
  private final LocalRestService localRestService;
  private final Logger logger = LoggerFactory.getLogger(AbbreviationHelperService.class);

  @Autowired
  public AbbreviationHelperService(LocalRestService localRestService) {
    this.localRestService = localRestService;
  }

  public Map<String, Object> getAbbreviation(String journalName) {
    if (journalName == null || journalName.strip().equals("")) {
      return new LinkedHashMap<>();
    }
    JSONObject requestParameters = new JSONObject();
    requestParameters.put("journalName", journalName);
    try {
      final JSONObject response =
          this.localRestService.performJsonPostRequest(
              "/abbreviations/getAbbreviation", requestParameters);
      if (response.getString("status").equals("SUCCESS")) {
        if (response.keySet().contains("data")) {
          JSONObject referenceList = response.getJSONObject("data");
          return referenceList.toMap();
        }
      }
    } catch (Exception e) {
      logger.debug(e.getMessage());
    }
    return new LinkedHashMap<>();
  }
}
