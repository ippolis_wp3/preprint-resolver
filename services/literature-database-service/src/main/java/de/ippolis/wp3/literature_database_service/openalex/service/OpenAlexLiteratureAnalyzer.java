package de.ippolis.wp3.literature_database_service.openalex.service;

import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLog;
import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLogHelper;
import de.ippolis.wp3.literature_database_service.utils.helper.BibTexHelperService;
import de.ippolis.wp3.literature_database_service.utils.model.AbstractLiteratureAnalyzer;
import de.ippolis.wp3.literature_database_service.utils.model.ArxivHelper;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexReferenceComparisonHelper;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.service.H2RestService;
import de.ippolis.wp3.literature_database_service.utils.service.JCRRestService;
import de.ippolis.wp3.literature_database_service.utils.service.LocalRestService;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OpenAlexLiteratureAnalyzer extends AbstractLiteratureAnalyzer {

  private final OpenAlexLocalDatabaseService openAlexLocalDatabaseService;
  private final OpenAlexAPIReferenceService openAlexAPIReferenceService;

  @Autowired
  public OpenAlexLiteratureAnalyzer(
      ArxivHelper arxivHelper,
      BibTexHelperService bibTexHelperService,
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper,
      OpenAlexLocalDatabaseService openAlexLocalDatabaseService,
      OpenAlexAPIReferenceService openAlexAPIReferenceService,
      JCRRestService jcrRestService,
      H2RestService h2RestService,
      LocalRestService localRestService,
      FilterLogHelper filterLogHelper) {
    super(
        arxivHelper,
        bibTexHelperService,
        bibTexReferenceComparisonHelper,
        jcrRestService,
        h2RestService,
        localRestService,
        filterLogHelper);
    this.openAlexLocalDatabaseService = openAlexLocalDatabaseService;
    this.openAlexAPIReferenceService = openAlexAPIReferenceService;
  }

  @Override
  protected List<Reference> extractLocalResultsByDOI(
      String id,
      String doi,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    if (doi == null || doi.strip().equals("")) {
      return new LinkedList<>();
    }
    List<Reference> results =
        openAlexLocalDatabaseService.getByDOI(doi, id, bibTexPagePreprocessing);
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "OpenAlexLocalDOIExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractLocalResultsByTitle(
      String id,
      String title,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    List<Reference> results =
        openAlexLocalDatabaseService.getByTitle(title, id, bibTexPagePreprocessing);
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "OpenAlexLocalTitleExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractLocalResultsByArxivId(
      String id,
      String arxivId,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    if (arxivId == null || arxivId.strip().equals("")) {
      return new LinkedList<>();
    }
    List<Reference> results =
        openAlexLocalDatabaseService.getByArxivId(arxivId, id, bibTexPagePreprocessing);
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "OpenAlexLocalArxivIdExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractAPIResultsByTitle(
      String id,
      String title,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    List<Reference> results =
        openAlexAPIReferenceService.getOpenAlexResultsByTitle(title, id, bibTexPagePreprocessing);
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "OpenAlexAPITitleExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractAPIResultsByDOI(
      String id,
      String doi,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    if (doi == null || doi.strip().equals("")) {
      return new LinkedList<>();
    }
    List<Reference> results =
        openAlexAPIReferenceService.getOpenAlexResultByDOI(doi, id, bibTexPagePreprocessing);
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "OpenAlexAPIDOIExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }

  @Override
  protected List<Reference> extractAPIResultsByArxivId(
      String id,
      String arxivId,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      List<FilterLog> filterLogs) {
    /*TODO*/

    LinkedList<Reference> results = new LinkedList<>();
    List<Map<String, Object>> listOutput =
        getFilterLogHelper().convertListOfReferencesToListOfMaps(results);
    int lastIndexInLog = getFilterLogHelper().getLastIndexInLog(filterLogs);
    FilterLog filterLog =
        new FilterLog(
            "OpenAlexAPIArxivIdExtraction",
            lastIndexInLog + 1,
            0,
            results.size(),
            new LinkedList<>(),
            listOutput);
    filterLogs.add(filterLog);
    return results;
  }
}
