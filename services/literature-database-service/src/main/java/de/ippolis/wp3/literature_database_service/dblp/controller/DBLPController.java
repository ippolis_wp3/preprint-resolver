package de.ippolis.wp3.literature_database_service.dblp.controller;

import de.ippolis.wp3.literature_database_service.dblp.service.DBLPLiteratureAnalyzer;
import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLog;
import de.ippolis.wp3.literature_database_service.filter_log.model.FilterLogHelper;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.model.dto.ArxivDoiTitleAuthorRequestDTO;
import de.ippolis.wp3.literature_database_service.utils.model.json.ErrorJsonResponse;
import de.ippolis.wp3.literature_database_service.utils.model.json.JsonResponse;
import de.ippolis.wp3.literature_database_service.utils.model.json.SuccessJsonResponse;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DBLPController {
  private final DBLPLiteratureAnalyzer dblpLiteratureAnalyzer;
  private final FilterLogHelper filterLogHelper;
  private final Logger logger = LoggerFactory.getLogger(DBLPController.class);

  @Autowired
  public DBLPController(
      DBLPLiteratureAnalyzer dblpLiteratureAnalyzer, FilterLogHelper filterLogHelper) {
    this.dblpLiteratureAnalyzer = dblpLiteratureAnalyzer;
    this.filterLogHelper = filterLogHelper;
  }

  @PostMapping(
      value = "/dblp/getResultByDoiOrTitleAndAuthors",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getResultByDoiOrTitleAndAuthors(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    String title = requestDTO.getTitle();
    List<String> authors = requestDTO.getAuthors();
    String doi = requestDTO.getDoi();
    String id = requestDTO.getId();
    String arxivId = requestDTO.getArxivId();
    BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
    boolean useLocalData = requestDTO.isUseLocalData();
    boolean useAPIData = requestDTO.isUseAPI();
    List<FilterLog> filterLogs = new LinkedList<>();
    List<Reference> result =
        dblpLiteratureAnalyzer.resolveLiteratureEntry(
            id,
            doi,
            arxivId,
            title,
            authors,
            useLocalData,
            useAPIData,
            bibTexPagePreprocessing,
            filterLogs);
    Map<String, Object> results = new LinkedHashMap<>();
    results.put("references", result);
    results.put("filterLogs", filterLogHelper.convertListOfFilterLogsToListOfMaps(filterLogs));
    logger.info("Response data: {}", results);
    return new SuccessJsonResponse(results);
  }

  @PostMapping(
      value = "/dblp/resolveArxivToLatexString",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse resolveArxivToLatexString(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    String title = requestDTO.getTitle();
    List<String> authors = requestDTO.getAuthors();
    String id = requestDTO.getId();
    String doi = requestDTO.getDoi();

    boolean useLocalData = requestDTO.isUseLocalData();
    boolean useAPIData = requestDTO.isUseAPI();
    String arxivId = requestDTO.getArxivId();
    BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
    List<FilterLog> filterLogs = new LinkedList<>();
    List<Reference> result =
        dblpLiteratureAnalyzer.resolveLiteratureEntryArxiv(
            id,
            doi,
            arxivId,
            title,
            authors,
            useLocalData,
            useAPIData,
            bibTexPagePreprocessing,
            filterLogs);
    String latexString = "";
    if (result.size() > 0) {
      latexString = result.get(0).toLatexString(true);
    }
    Map<String, Object> results = new LinkedHashMap<>();
    results.put("result", latexString);
    results.put("filterLogs", filterLogHelper.convertListOfFilterLogsToListOfMaps(filterLogs));
    logger.info("Response data: {}", results);
    return new SuccessJsonResponse(results);
  }

  @PostMapping(
      value = "/dblp/resolveArxivToMap",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse solveArxivByArxivID(@RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      String doi = requestDTO.getDoi();
      String id = requestDTO.getArxivId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      String arxivId = requestDTO.getArxivId();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<FilterLog> filterLogs = new LinkedList<>();
      List<Reference> references =
          dblpLiteratureAnalyzer.resolveLiteratureEntryArxiv(
              id,
              doi,
              arxivId,
              title,
              authors,
              useLocalData,
              useAPI,
              bibTexPagePreprocessing,
              filterLogs);
      List<Map<String, Object>> result = new LinkedList<>();
      for (Reference reference : references) {
        result.add(reference.toSimpleMap(true));
      }
      Map<String, Object> results = new LinkedHashMap<>();
      results.put("resultsMap", result);
      results.put("filterLogs", filterLogHelper.convertListOfFilterLogsToListOfMaps(filterLogs));

      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }
}
